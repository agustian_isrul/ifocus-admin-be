const express = require("express"),
  app = express();
const router = express.Router();
const pool = require("../../connection/db");
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const logger = require("../../routes/services/utils/logservices");
const moment = require("moment");
const checkAuth = require("../../middleware/check-auth");
const { insertGroup } = require("./utils/groupUtils");
router.use(checkAuth);
/*POST END*/

router.put("/updatestat/:id", async (req, res, next) => {
  try {
    const {
      id,
      modulename,
      created_byid,
      status,
      idapplication,
      modulecode,
      applabel,
    } = req.body;

    const resp = await pool("mst_moduleby_tenant")
      .where({ id: req.params.id })
      .update({ status: status });
    var jsonbody = resp;

    res.status(200).json({ status: 200, data: jsonbody });
    // }
  } catch (err) {
    next(err);
    res.status(500).json({ status: 500, data: "No User found" });
  }
});

router.get("/", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const authHeader = req.headers.authorization;
    var dcodeInfo = null;
    if (authHeader) {
      TokenArray = authHeader.split(" ");
      dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
    } else {
      res.status(500).json({ status: 500, data: "Not authorized" });
    }

    res.status(200).json({ status: 200, data: dcodeInfo });
  } catch (err) {
    next(err);
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});

router.get("/:id", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const authHeader = req.headers.authorization;
    var dcodeInfo = req.userData;
    var tipeModule = 1;
    console.log(">>>>>>>>>>>> " + dcodeInfo.idtenant);
    console.log(">>>>>>>>>>>> " + dcodeInfo.leveltenant);

    let resp;

    if (dcodeInfo.leveltenant !== "0") {
      resp = await pool
        .select("mstmod.id", "mmod.*")
        .from("mst_module as mstmod ")

        .innerJoin(
          pool
            .select(
              "mmod.id",
              "mmod.modulename",
              "mmod.created_byid",
              "mmod.status",
              "mmod.idapplication",
              "mmod.modulecode",
              "mapp.applabel"
            )
            .from(
              pool
                .select("*")
                .from("mst_moduleby_tenant")
                .where({ idtenant: dcodeInfo.idtenant })
                .as("mmod")
            )
            .innerJoin(
              "mst_application as mapp",
              " mmod.idapplication",
              " mapp.id"
            )
            .as("mmod"),
          " mstmod.modulename",
          "mmod.modulename"
        )
        .where({ "mmod.idapplication": req.params.id })
        .orderBy("mmod.id");
    } else {
      resp = await pool
        .select(
          "mmod.id",
          "mmod.modulename",
          "mmod.created_byid",
          "mmod.status",
          "mmod.idapplication",
          "mmod.modulecode",
          "mapp.applabel"
        )
        .from("mst_moduleby_tenant as mmod")
        .innerJoin("mst_application as mapp", "mmod.idapplication", "mapp.id")
        .where({
          "mmod.idapplication": dcodeInfo.idtenant,
          "mmod.idtenant": req.params.id,
        })
        .orderBy("mmod.id");
    }

    var jsonbody = resp;

    res.status(200).json({ status: 200, data: jsonbody });
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});

router.get("/menusmodule/:id", async (req, res, next) => {
  try {
    let systemdate = new Date();
    // const authHeader = req.headers.authorization;
    var dcodeInfo = req.userData;
    console.log(req.params.id);

    // TokenArray = authHeader.split(" ");
    // dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
    let module = {};
    let menus = [];

    const resp = await pool
      .select("id", "modulename")
      .from("mst_module")
      .where({ id: req.params.id });

    module = resp[0];

    await pool
      .select("mmen.idmodule", "mmen.id", "mmen.title", "mmod.modulename")
      .from("mst_moduleby_tenant as mmod")
      .innerJoin("mst_menu as mmen", "mmod.id", "mmen.idmodule")
      .where({ "mmod.id": req.params.id })
      .then(async (result) => {
        menus = result;
        module["menus"] = menus;
        console.log(module);

        res.status(200).json({ status: 200, data: module });
      })
      .catch((err) => {
        console.log(err);
        next(err);
        res.status(500).json({ status: 500, data: "Error insert log Catch" });
      });
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});

router.get("/modulesgroup/:id", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const authHeader = req.headers.authorization;
    var dcodeInfo = null;
    if (authHeader) {
      TokenArray = authHeader.split(" ");
      dcodeInfo = await jwttools.decryptdata(TokenArray[1]);

      const resp = await pool
        .select(
          "mtg.groupname",
          "mtg.idtenant",
          "mtg.created_byid",
          "mtg.created_at",
          "mtg.updated_at",
          "mtg.id",
          "mtg.issubgroup",
          "mtg.idapplication",
          "mtg.idowner",
          "mapp.appname",
          "mapp.applabel"
        )
        .from("mst_tenantgroup as mtg")
        .innerJoin("mst_application as mapp", "mapp.id", "mtg.idapplication")
        .where({
          "tg.idapplication": req.params.id,
          "mtg.idtenant": dcodeInfo.idtenant,
        });

      var jsonbody = resp;
      res.status(200).json({ status: 200, data: jsonbody });
    } else {
      res.status(500).json({ status: 500, data: "Not authorized" });
    }

    res.status(200).json({ status: 200, data: dcodeInfo });
  } catch (err) {
    next(err);
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});

router.get("/menusmodule/:id/:groupid", async (req, res, next) => {
  try {
    var dcodeInfo = req.userData;
    let module = {};
    let menus = [];

    const resp = await pool
      .select("id", "modulename")
      .from("mst_moduleby_tenant")
      .where({ id: req.params.id });
    module = resp[0];

    let menusDataWithAcl = await pool
      .select(
        "mmen.idmodule",
        "mmen.id",
        "uga.fread",
        "uga.fupdate",
        "uga.fdelete",
        "uga.fcreate",
        "uga.fapproval",
        "mmen.title",
        "mmod.modulename"
      )
      .from("mst_moduleby_tenant as mmod")
      .innerJoin("mst_menu as  mmen", "mmod.id", "mmen.idmodule")
      .rightJoin("mst_usergroupacl as uga", "mmen.id", "uga.idmenu")
      .where({ "mmod.id": req.params.id, "uga.idgroup": req.params.groupid });

    let menusById = await pool
      .select("*")
      .from("mst_menu")
      .where({ idmodule: req.params.id });

    let mergedMenus = [];
    await menusById.map(async (mid) => {
      let mergedMenu = {
        fcreate: 0,
        fdelete: 0,
        fread: 0,
        fupdate: 0,
        id: "",
        idmodule: "",
        modulename: "",
        title: "",
      };

      mergedMenu.id = mid.id;
      mergedMenu.modulename = mid.name;
      mergedMenu.title = mid.title;
      mergedMenus.push(mergedMenu);
    });

    menus = await mergedMenus.map((mergMen) => ({
      ...mergMen,
      ...menusDataWithAcl.find((acmMen) => acmMen.id === mergMen.id),
    }));
    module.menus = menus;
    let jsonbody = module;

    res.status(200).json({ status: 200, data: jsonbody });
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});

module.exports = router;

// router.get("/:id", async (req, res, next) => {
//   try {
//     let systemdate = new Date();
//     const authHeader = req.headers.authorization;
//     var dcodeInfo = null;
//     if (authHeader) {
//       TokenArray = authHeader.split(" ");
//       dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
//       var tipeModule = 1;
//       // const resp = await pool.query("SELECT mmod.id, mmod.modulename, mmod.created_byid, mmod.status, mmod.idapplication, mmod.modulecode, mapp.applabel FROM public.mst_module mmod INNER JOIN mst_application mapp ON mmod.idapplication = mapp.id WHERE mmod.idapplication = $1;",[req.params.id]);
//       console.log(">>>>>>>>>>>> " + dcodeInfo.idtenant);
//       var query =
//         "SELECT mmod.id, mmod.modulename, mmod.created_byid, mmod.status, mmod.idapplication, mmod.modulecode, mapp.applabel FROM public.mst_moduleby_tenant mmod INNER JOIN mst_application mapp ON mmod.idapplication = mapp.id WHERE mmod.idapplication = $1 and mmod.idtenant=$2 order by mmod.id;";
//       if (dcodeInfo.leveltenant !== "0") {
//         query =
//           "SELECT mmod.id, mmod.modulename, mmod.created_byid, mmod.status, mmod.idapplication, mmod.modulecode, mapp.applabel FROM public.mst_moduleby_tenant mmod INNER JOIN mst_application mapp ON mmod.idapplication = mapp.id WHERE mmod.idapplication = $1 and mmod.idtenant=$2 and mmod.moduletype>=1 order by mmod.id;";
//       }

//       const resp = await pool.query(query, [req.params.id, dcodeInfo.idtenant]);

//       var jsonbody = resp.rows;

//       res.status(200).json({ status: 200, data: jsonbody });
//     } else {
//       res.status(500).json({ status: 500, data: "Not authorized" });
//     }
//   } catch (err) {
//     res.status(500).json({ status: 500, data: "Error insert log Catch" });
//   }
// });

// router.get("/menusmodule/:id/:groupid", async (req, res, next) => {
//   try {
//     let systemdate = new Date();
//     const authHeader = req.headers.authorization;
//     var dcodeInfo = null;
//     //console.log(authHeader);
//     if (authHeader) {
//       TokenArray = authHeader.split(" ");
//       dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
//       let module = {};
//       let menus = [];
//       const resp = await pool.query(
//         "SELECT  id,modulename from public.mst_moduleby_tenant WHERE id = $1;",
//         [req.params.id]
//       );
//       module = resp.rows[0];
//       let menusDataWithAcl = await pool.query(
//         "SELECT mmen.idmodule,mmen.id,uga.fread,uga.fupdate,uga.fdelete,uga.fcreate,mmen.title,mmod.modulename FROM public.mst_moduleby_tenant mmod INNER JOIN mst_menu mmen ON mmod.id = mmen.idmodule  inner join mst_usergroupacl uga on mmen.id = uga.idmenu WHERE mmod.id = $1 and uga.idgroup = $2",
//         [req.params.id, req.params.groupid]
//       );
//       let menusById = await pool.query(
//         "Select * from mst_menu where idmodule = $1",
//         [req.params.id]
//       );
//       let mergedMenus = [];
//       await menusById.rows.map(async (mid) => {
//         let mergedMenu = {
//           fcreate: 0,
//           fdelete: 0,
//           fread: 0,
//           fupdate: 0,
//           id: "",
//           idmodule: "",
//           modulename: "",
//           title: "",
//         };

//         mergedMenu.id = mid.id;
//         mergedMenu.modulename = mid.name;
//         mergedMenu.title = mid.title;
//         mergedMenus.push(mergedMenu);
//       });

//       menus = await mergedMenus.map((mergMen) => ({
//         ...mergMen,
//         ...menusDataWithAcl.rows.find((acmMen) => acmMen.id === mergMen.id),
//       }));
//       module.menus = menus;
//       let jsonbody = module;

//       res.status(200).json({ status: 200, data: jsonbody });
//     } else {
//       res.status(500).json({ status: 500, data: "Not authorized" });
//     }
//   } catch (err) {
//     console.log(err);
//     res.status(500).json({ status: 500, data: "Error insert log Catch" });
//   }
// });
