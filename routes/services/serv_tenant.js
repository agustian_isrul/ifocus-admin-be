const express = require("express"),
  app = express();
const router = express.Router();
const pool = require("../../connection/db");
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const logger = require("../../routes/services/utils/logservices");
const moment = require("moment");
const checkAuth = require("../../middleware/check-auth");
const { insertGroup } = require("./utils/groupUtils");
router.use(checkAuth);
/*POST END*/

router.get("/getAllTenants", async (req, res, next) => {
  try {
    let systemdate = new Date();
    // const authHeader = req.headers.authorization;
    var dcodeInfo = req.userData;

    // console.log(">>> USER INFO ", dcodeInfo);

    const resp = await pool
      .select(
          "id", 
          "tnname", 
          "tnstatus", 
          "tntype", 
          "cdidowner", 
          "tnflag", 
          "tnparentid", 
          "cdtenant", 
          "created_at", 
          "updated_at")
      .from("cdid_tenant")
      .where({ "tnparentid": dcodeInfo.idtenant });

      res.status(200).json({ status: 200, data: resp })
    //   .where({ id: req.params.id });

    // module = resp[0];

    // await pool
    //   .select("mmen.idmodule", "mmen.id", "mmen.title", "mmod.modulename")
    //   .from("mst_moduleby_tenant as mmod")
    //   .innerJoin("mst_menu as mmen", "mmod.id", "mmen.idmodule")
    //   .where({ "mmod.id": req.params.id })
    //   .then(async (result) => {
    //     menus = result;
    //     module["menus"] = menus;
    //     console.log(module);

    //     res.status(200).json({ status: 200, data: module });
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //     next(err);
    //     res.status(500).json({ status: 500, data: "Error insert log Catch" });
    //   });
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});

router.post("/getAllTenantsIn", async (req, res, next) => {
  try {
    let systemdate = new Date();
    // const authHeader = req.headers.authorization;
    var dcodeInfo = req.userData;

    // console.log(">>> USER INFO ", dcodeInfo);
    const { payloads } = req.body;

    console.log("PAYLOADS ", payloads);
    const resp = await pool
      .select(
          "id", 
          "tnname", 
          "tnstatus", 
          "tntype", 
          "cdidowner", 
          "tnflag", 
          "tnparentid", 
          "cdtenant", 
          "created_at", 
          "updated_at")
      .from("cdid_tenant")
      .whereIn("tnname", payloads );

      res.status(200).json({ status: 200, data: resp })
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});


module.exports = router;

