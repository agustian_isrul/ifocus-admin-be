const express = require("express"),
  app = express();
const router = express.Router();
const pool = require("../../connection/db");
const jwttools = require("./utils/encryptdecryptjwt");
const bcrypt = require("bcrypt");
const logger = require("./utils/logservices");
const moment = require("moment");
const validatePass = require("./utils/user_management/userValidation");
const sysParam = require("./utils/user_management/getSysParam");
const checkAuth = require("../../middleware/check-auth");
// let umParam;

// const getParam = async () => {
//   umParam = await sysParam.getNEParam();
// };

/*POST END*/
const eventLog = require("../../middleware/eventLog");
router.use(checkAuth);
router.use(eventLog);

router.get("/smtpsettings", async (req, res, next) => {
  try {
    var dcodeInfo = req.userData;
    /*
    const query =
      "SELECT id, param_name, param_value, category, description, idtenant, idapp, typeparam FROM public.system_param where category =$1 and typeparam= 1 and idapp=$2 order by id;";
    // console.log(query,"NOTIFIKASI_EMAIL", dcodeInfo.apps[0].idapp);
    const result = await pool.query(query, [
      "NOTIFIKASI_EMAIL",
      dcodeInfo.apps[0].idapp,
    ]);
    */

    const result = await pool
      .select(
        "id",
        "param_name",
        "param_value",
        "category",
        "description",
        "idtenant",
        "idapp",
        "typeparam"
      )
      .from("system_param")
      .where({
        category: "NOTIFIKASI_EMAIL",
        typeparam: 1,
        idapp: dcodeInfo.apps[0].idapp,
      })
      .orderBy("id");

    res.status(200).json({ status: 200, data: result });
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.post("/smtpupdate", async (req, res, next) => {
  var dcodeInfo = req.userData;

  var apps = dcodeInfo.apps[0];

  // res.status(200).json({ status: 200, data: apps });
  try {
    const { id, value } = req.body;

    await pool("system_param").update({ param_value: value }).where({ id: id });
    res.status(200).json({ status: 200, data: "Update Success" });
  } catch (err) {
    next(err);
    res.status(500).json({ status: 500, data: "Error insert Tenant" });
  }
});

module.exports = router;
