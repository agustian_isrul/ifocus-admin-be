const express = require("express"),
  app = express();
const router = express.Router();
const pool = require("../../connection/db");
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const logger = require("../../routes/services/utils/logservices");
const moment = require("moment");
const axios = require("axios");
const userValidation = require("./utils/user_management/userValidation");
const sysParam = require("./utils/user_management/getSysParam");
const passwordUtil = require("./utils/password/passwordUtil");
const passEnc = require("./utils/password/encryptionPass");
let umParam;
var conf = require("../../config.json");
const getParam = async () => {
  umParam = await sysParam.getUMParam();
};

const checkAuth = require("../../middleware/check-auth");
const eventLog = require("../../middleware/eventLog");
router.use(checkAuth);

router.use(eventLog);
/*POST END*/

router.get("/", async (req, res, next) => {
  try {
    // const resp = await pool.query(
    //   "Select * from public.cdid_owner order by id",
    //   []
    // );

    const resp = await pool.select("*").from("cdid_owner").orderBy("id");
    res.status(200).json({ status: 200, data: resp });
  } catch (err) {
    next(err);

    res.status(500).json({ status: 500, data: "No data tenants found" });
  }
});

router.get("/retriveusers", async (req, res, next) => {
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];

  try {
    var leveluser = parseInt(dcodeInfo.leveltenant);
    console.log(">>>>>>>>>> LEVEL USER PAGE USER " + leveluser);
    var query = "";
    let resp;
    switch (leveluser) {
      case 0:
        // query =
        //   "SELECT tu.id, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at,tu.idtenant, tu.idsubtenant, tu.leveltenant, tu.active, ou.orgid, mo.orgname, ctug.idgroupuseracl, COALESCE(mu.groupname, 'Administrator') groupname FROM public.cdid_tenant_user tu INNER JOIN public.cdid_orguserassign ou ON tu.id = ou.userid INNER JOIN public.mst_organizations mo ON ou.orgid=mo.id LEFT JOIN (SELECT * FROM public.cdid_tenant_user_groupacl) ctug ON tu.id = ctug.iduser LEFT JOIN mst_usergroup mu ON ctug.idgroupuseracl = mu.id WHERE tu.idtenant=$1 and tu.leveltenant = $2 order by tu.id";
        // const resp = await pool.query(
        //   query,
        //   leveluser === 0
        //     ? [dcodeInfo.idtenant, leveluser + 1]
        //     : [leveluser, apps.idapp]
        // );

        resp = await pool
          .select(
            "tu.id",
            "tu.fullname",
            "tu.userid",
            "tu.pwd",
            "tu.creator_stat",
            "tu.creator_byid",
            "tu.created_at",
            "tu.updated_at",
            "tu.idtenant",
            "tu.idsubtenant",
            "tu.leveltenant",
            "tu.active",
            "ou.orgid",
            "mo.orgname",
            "ctug.idgroupuseracl",
            "mu.groupname as groupname "
          )
          .from("cdid_tenant_user as tu")
          .innerJoin("cdid_orguserassign as ou", "tu.id ", "ou.userid")
          .innerJoin("mst_organizations as mo", "ou.orgid", "mo.id")
          .leftJoin(
            pool.select("*").from("cdid_tenant_user_groupacl").as("ctug"),
            "tu.id",
            "ctug.iduser"
          )
          .leftJoin(
            pool.select("*").from("mst_usergroup").as("mu"),
            "ctug.idgroupuseracl ",
            "mu.id"
          )
          .where({
            "tu.idtenant": leveluser === 0 ? dcodeInfo.idtenant : leveluser,
            "tu.leveltenant": leveluser === 0 ? leveluser + 1 : apps.idapp,
          });
        break;
      case 1:
        console.log(">>>>>>>>>> CASE PAGE USER " + leveluser);
        console.log(dcodeInfo.idtenant, leveluser, apps.idapp);

        // query =
        //   "SELECT DISTINCT ON (tu.id) tu.id,tu.leveltenant, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at, tu.idtenant, tu.idsubtenant,tu.leveltenant, tu.active, ct.tnname tenantname,'' subtenantname,tu2.fullname creatorname FROM public.cdid_tenant_user tu INNER JOIN public.cdid_tenant ct ON tu.idtenant = ct.id INNER JOIN public.cdid_tenant_user tu2 ON tu.creator_byid = tu2.id INNER JOIN public.cdid_tenant_user_groupacl tug ON tu.id = tug.iduser WHERE   tu.leveltenant >= $1 and tug.idapplication = $2 and ct.id= " +
        //   dcodeInfo.idtenant;
        resp = await pool
          .select(
            "tu.id",
            "tu.leveltenant",
            "tu.fullname",
            "tu.userid",
            "tu.pwd",
            "tu.creator_stat",
            "tu.creator_byid",
            "tu.created_at",
            "tu.updated_at",
            "tu.idtenant",
            "tu.idsubtenant",
            "tu.leveltenant",
            "tu.active",
            "ct.tnname as tenantname",
            "tu2.fullname as creatorname"
          )
          .from("cdid_tenant_user as tu")
          .innerJoin("cdid_tenant_user as tu2", "tu.id ", "ct.id")
          .innerJoin("mst_organizations as mo", "tu.creator_byid", "tu2.id")
          .innerJoin("cdid_tenant_user_groupacl as tug", "tu.id", "tug.iduser")
          .where({
            "tug.idapplication": leveluser === 0 ? leveluser + 1 : apps.idapp,
            "ct.id": dcodeInfo.idtenant,
          })
          .andWhere(
            " tu.leveltenant",
            ">=",
            leveluser === 0 ? dcodeInfo.idtenant : leveluser
          );
        break;

      default:
        // query =
        //   "SELECT tu.id, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at, tu.idtenant, tu.idsubtenant, tu.leveltenant, tu.active, ct.tnname tenantname,'' subtenantname,tu2.fullname creatorname, tug.idgroupuseracl idgroupacl, mug.groupname groupaclname FROM public.cdid_tenant_user tu INNER JOIN public.cdid_tenant_user_groupacl tug ON tu.id = tug.iduser INNER JOIN public.cdid_tenant ct ON tu.idtenant = ct.id INNER JOIN mst_usergroup mug ON tug.idgroupuseracl = mug.id INNER JOIN public.cdid_tenant_user tu2 ON tu.creator_byid = tu2.id WHERE tu.idtenant=$1 and tu.leveltenant > $2 and tug.idapplication=$3";

        resp = await pool
          .select(
            "tu.id",
            "tu.fullname",
            "tu.userid",
            "tu.pwd",
            "tu.creator_stat",
            "tu.creator_byid",
            "tu.created_at",
            "tu.updated_at",
            "tu.idtenant",
            "tu.idsubtenant",
            "tu.leveltenant",
            "tu.active",
            "ct.tnname as tenantname",
            "tu2.fullname  as creatorname",
            " tug.idgroupuseracl as idgroupacl",
            "mug.groupname as groupaclname"
          )
          .from("cdid_tenant_user as tu")
          .innerJoin("cdid_tenant_user_groupacl as tug", "tu.id ", "tug.iduser")
          .innerJoin("cdid_tenant as ct", "tu.idteantn", "ct.id")
          .innerJoin("mst_usergroup as mug", "tug.idgroupuseracl", "mug.id")
          .innerJoin("cdid_tenant_user as tu2", "tu.creator_byid", "tu2.id")
          .where({
            "tu.idtenant": leveluser === 0 ? leveluser + 1 : apps.idapp,
            "tug.idapplication": dcodeInfo.idtenant,
          })
          .andWhere(
            " tu.leveltenant",
            ">",
            leveluser === 0 ? dcodeInfo.idtenant : leveluser
          );
        break;
    }

    // const resp = await pool.query(
    //   query,
    //   leveluser === 0
    //     ? [dcodeInfo.idtenant, leveluser + 1]
    //     : [leveluser, apps.idapp]
    // );

    if (resp.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: resp });
      }, 500);
    } else {
      // res.status(500).json({ status: 500, data: "Error retrive Users" });
      setTimeout(function () {
        res.status(200).json({ status: 200, data: resp });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    next(err);

    res.status(500).json({ status: 500, data: "Error retrive Users" });
  }
});

router.get("/retriveusersadmin", async (req, res, next) => {
  // const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  // console.log(">> retriveusersadmin get req Data ",req.userData);
  var apps = dcodeInfo.apps[0];
  // res.status(200).json({ status: 200, data: apps.id_application });
  try {
    var leveluser = parseInt(dcodeInfo.leveltenant);
    console.log(">>>>>>>>>> LEVEL USER PAGE USER " + leveluser);
    var query = "";
    console.log(">>>>>>>>>> CASE PAGE USER " + leveluser);
    // console.log(dcodeInfo.idtenant, leveluser, apps.idapp);

    // console.log(dcodeInfo);
    // query =
    //   "SELECT DISTINCT ON (tu.id) tu.id,tu.leveltenant, tu.fullname, tu.userid,tu.total_attempt, tu.pwd, tu.creator_stat, tu.creator_byid, to_char(tu.created_at, 'YYYY-MM-DD HH:mm:ss') created_at,tu.updated_at, tu.idtenant, tu.idsubtenant,tu.leveltenant, tu.active, ct.tnname tenantname,'' subtenantname,tu2.fullname creatorname,tug.idgroupuseracl, mug.groupname FROM public.cdid_tenant_user tu INNER JOIN public.cdid_tenant ct ON tu.idtenant = ct.id INNER JOIN public.cdid_tenant_user tu2 ON tu.creator_byid = tu2.id INNER JOIN public.cdid_tenant_user_groupacl tug ON tu.id = tug.iduser INNER JOIN mst_usergroup mug ON tug.idgroupuseracl = mug.id WHERE tu.leveltenant >= $1 and tug.idapplication = $2 and ct.id= " +
    //   dcodeInfo.idtenant;

    let resp = await pool
      .select(
        "tu.id",
        "tu.leveltenant",
        "tu.fullname",
        "tu.userid",
        "tu.total_attempt",
        "tu.pwd",
        "tu.creator_stat",
        "tu.creator_byid",
        "tu.created_at as created_at",
        "tu.updated_at",
        "tu.idtenant",
        "tu.idsubtenant",
        pool.raw("IF(tu.leveltenant > 1, 'SUPPLIER','RESOURCING') as levelname"),
        "tu.active",
        "tu.idapproval",
        "ct.tnname  as tenantname",
        "tu2.fullname as  creatorname",
        "tug.idgroupuseracl",
        "mug.groupname",
        "bio.bioemailactive"
      )
      .from("cdid_tenant_user as tu")
      // .innerJoin("cdid_tenant as ct", "tu.idtenant", "ct.id")
      .leftJoin("cdid_tenant as ct", "tu.idtenant", "ct.id")
      .innerJoin("cdid_tenant_user as tu2", "tu.creator_byid", "tu2.id")
      .innerJoin("cdid_tenant_user_groupacl as tug", "tu.id", "tug.iduser")
      .innerJoin("mst_usergroup as mug", "tug.idgroupuseracl", "mug.id")
      .innerJoin(
        pool
          .select("*")
          .from("mst_biodata_corell")
          .where({ bioidcorel: 3 })
          .orderBy("id", "desc")
          .as("bio"),
        "tu.id",
        "bio.biocorelobjid"
      ).modify(function(queryBuilder) {
        if(dcodeInfo.idtenant < 2) {
          queryBuilder.where({
            "tug.idapplication": apps.idapp
          })
          .andWhere("tu.leveltenant", ">=", leveluser);
        } else {
          queryBuilder.where({
            "tug.idapplication": apps.idapp,
            "ct.id": dcodeInfo.idtenant
          })
          .andWhere("tu.leveltenant", ">=", leveluser);
        }
      })
      
      // ,
      // "ct.id": dcodeInfo.idtenant,
    // console.log(resp);
    if (resp.length > 0) {
      res.status(200).json({ status: 200, data: resp });
    } else {
      // res.status(500).json({ status: 500, data: "Error retrive Users" });
      setTimeout(function () {
        res.status(200).json({ status: 202, data: resp });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    next(err);

    res.status(500).json({ status: 500, data: "Error retrive Users" });
  }
});

router.get("/retriveusersbyid/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];
  let userId = req.params.id;
  try {
    var leveluser = parseInt(dcodeInfo.leveltenant);
    var query = "";
    let preparedStatement = [];
    let queryOrg = false;
    let preparedStatementOrg = [];
    let queryGroup = false;
    let preparedStatementGroup = [];

    let resp;
    console.log("AMBIL EDIT >>>>> ", leveluser);
    switch (leveluser) {
      case 0:
        // query =
        //   "SELECT tu.id, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at,tu.idtenant, tu.idsubtenant, tu.leveltenant, tu.active FROM public.cdid_tenant_user tu WHERE tu.idtenant=$1 and tu.leveltenant = $2 and tu.id = $3";
        // preparedStatement = [dcodeInfo.idtenant, leveluser + 1, userId];
        resp = await pool
          .select(
            "tu.id",
            "tu.fullname",
            "tu.userid",
            "tu.pwd",
            "tu.creator_stat",
            "tu.creator_byid",
            "tu.created_at",
            "tu.updated_at",
            "tu.idtenant",
            "tu.idsubtenant",
            "tu.leveltenant",
            "tu.active",
            "mc.bioemailactive"
          )
          .from("cdid_tenant_user as tu")
          .innerJoin("mst_biodata_corell as mc", "tu.id", "mc.biocorelobjid")
          .where({
            "tu.idtenant": dcodeInfo.idtenant,
            "tu.leveltenant": leveluser + 1,
            "tu.id": userId,
            "mc.bioidcorel": 3,
          });

        // queryOrg =
        //   "SELECT  oua.orgid  id, mo.orgname AS name FROM  public.cdid_orguserassign oua INNER JOIN public.mst_organizations mo ON oua.orgid = mo.id WHERE userid = $1 ";
        // preparedStatementOrg = [userId];
        queryOrg = await pool
          .select("oua.orgid  as  id", "mo.orgname AS name")
          .from("cdid_orguserassign as oua")
          .innerJoin("mst_organizations as mo", "oua.orgid", "mo.id")
          .where({
            userid: userId,
          });
        break;
      case 1:
        // query =
        //   "SELECT DISTINCT ON (tu.id) tu.id, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at, tu.idtenant, tu.idsubtenant,tu.leveltenant, tu.active, ct.tnname tenantname,'' subtenantname,tu2.fullname creatorname FROM public.cdid_tenant_user tu INNER JOIN public.cdid_tenant ct ON tu.idtenant = ct.id INNER JOIN public.cdid_tenant_user tu2 ON tu.creator_byid = tu2.id INNER JOIN public.cdid_tenant_user_groupacl tug ON tu.id = tug.iduser WHERE tu.idtenant=$1 and tu.leveltenant = $2 and tug.idapplication = $3 and tu.id =$4";
        // preparedStatement = [
        //   dcodeInfo.idtenant,
        //   userId == dcodeInfo.id ? leveluser + 1 : leveluser,
        //   apps.idapp,
        //   userId,
        // ];
        resp = await pool
          .select(
            "tu.id",
            "tu.fullname",
            "tu.userid",
            "tu.pwd",
            "tu.creator_stat",
            "tu.creator_byid",
            "tu.created_at",
            "tu.updated_at",
            "tu.idtenant",
            "tu.idsubtenant",
            "tu.leveltenant",
            "tu.active",
            "tu.usercode",
            "ct.tnname as  tenantname",
            "ct.cdtenant as  tenantcode",
            "tu2.fullname as  creatorname",
            "mc.bioemailactive",
            "mc.description",
            "mc.empid",
            "mc.empname",
            "mc.cltid",
            "mc.cltname",
            "mc.emailcc_1",
            "mc.emailcc_2",
          )
          .from("cdid_tenant_user as tu")
          // .innerJoin("cdid_tenant as ct ", "tu.idtenant", "ct.id")
          .leftJoin("cdid_tenant as ct ", "tu.idtenant", "ct.id")
          .innerJoin("cdid_tenant_user as tu2 ", "tu.creator_byid", "tu2.id")
          .innerJoin("cdid_tenant_user_groupacl as tug ", "tu.id", "tug.iduser")
          .innerJoin("mst_biodata_corell as mc", "tu.id", "mc.biocorelobjid")
          .where({
            // "tu.idtenant": dcodeInfo.idtenant,
            // "tu.leveltenant":
            //   userId == dcodeInfo.id ? leveluser + 1 : leveluser,
            // "tug.idapplication": apps.idapp,
            "tu.id": userId,
            "mc.bioidcorel": 3,
          });


          // console.log(">>> DATA USERNYA : ", resp);
        // queryGroup =
        //   "SELECT mug.id ,mug.groupname as name FROM public.cdid_tenant_user tu INNER JOIN public.cdid_tenant_user_groupacl tug ON tu.id = tug.iduser INNER JOIN public.cdid_tenant ct ON tu.idtenant = ct.id INNER JOIN mst_usergroup mug ON tug.idgroupuseracl = mug.id WHERE tu.idtenant = $1 and tu.leveltenant = $2 and tug.idapplication = $3 and tu.id = $4";
        // preparedStatementGroup = [
        //   dcodeInfo.idtenant,
        //   userId == dcodeInfo.id ? leveluser + 1 : leveluser,
        //   apps.idapp,
        //   userId,
        // ];

        queryGroup = await pool
          .select("mug.id", "mug.groupname as name")
          .from("cdid_tenant_user as tu")
          .innerJoin("cdid_tenant_user_groupacl as tug ", "tu.id", "tug.iduser")
          .innerJoin("cdid_tenant as ct ", "tu.idtenant", "ct.id")
          .innerJoin("mst_usergroup as mug ", "tug.idgroupuseracl", "mug.id")
          .where({
            // "tu.idtenant": dcodeInfo.idtenant,
            "tu.leveltenant":
              userId == dcodeInfo.id ? leveluser + 1 : leveluser,
            "tug.idapplication": apps.idapp,
            "tu.id": userId,
          });
        break;
      default:
        console.log("unhandled  level user");
        // query =
        //   "SELECT tu.id, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at, tu.idtenant, tu.idsubtenant, tu.leveltenant, tu.active, ct.tnname tenantname,'' subtenantname,tu2.fullname creatorname, tug.idgroupuseracl idgroupacl, mug.groupname groupaclname FROM public.cdid_tenant_user tu INNER JOIN public.cdid_tenant_user_groupacl tug ON tu.id = tug.iduser INNER JOIN public.cdid_tenant ct ON tu.idtenant = ct.id INNER JOIN mst_usergroup mug ON tug.idgroupuseracl = mug.id INNER JOIN public.cdid_tenant_user tu2 ON tu.creator_byid = tu2.id WHERE tu.idtenant=$1 and tu.leveltenant > 0 and tug.idapplication=$2 and tu.id = $3 tu.leveltenant > $4 ";
        break;
    }

    // const resp = await pool.query(query, preparedStatement);
    // console.log("Group by Edit ", queryGroup);
    if (queryOrg) {
      if (queryOrg.length > 0) {
        resp[0].org = queryOrg;
      }
    }
    if (queryGroup) {
      if (queryGroup.length > 0) {
        resp[0].group = queryGroup;
      }
    }

    console.log(resp);
    if (resp.length > 0) {
      res.status(200).json({ status: 200, data: resp[0] });
    } else {
      res.status(500).json({ status: 500, data: "Error retrive Users" });
    }
  } catch (err) {
    console.log(err);
    next(err);

    res.status(500).json({ status: 500, data: "Error retrive Users" });
  }
});

router.post("/insertbysuper", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var toknid = req.tokenID;
  try {
    var apps = dcodeInfo.apps[0];
    const { fullname, userid, password, isactive, org, group, emailname } =
      req.body;
    var levetid = parseInt(dcodeInfo.leveltenant);
    if (levetid == 0) {
      levetid = levetid + 1;
    }
    console.log(
      ">>>>>>>>>>>>>>>>>>>>>>>>>> APP INSERT ID " + JSON.stringify(apps)
    );
    let encryptedPass = await passEnc.encryptPass(password);
    const resp = await pool("cdid_tenant_user")
      .returning([
        "id",
        "fullname",
        "userid",
        "pwd",
        "creator_stat",
        "creator_byid",
        "idtenant",
        "leveltenant",
        "active",
      ])
      .insert({
        fullname: fullname,
        userid: userid,
        pwd: encryptedPass,
        creator_stat: levetid > 0 ? 1 : levetid,
        creator_byid: dcodeInfo.id,
        idtenant: dcodeInfo.idtenant,
        leveltenant: levetid,
        active: isactive,
      });
    if (resp.length > 0) {
      var userObj = resp[0];
      // console.log(">>>>>>>>>>>>>>>>>>>>>>>. ID APP " + apps.id_application);
      const cdidbio = await pool("mst_biodata_corell")
        .returning([
          "id",
          "bioname",
          "bioemailactive",
          "bioidcorel",
          "biocorelobjid",
        ])
        .insert({
          bioname: userObj.fullname,
          bioemailactive: userid,
          bioidcorel: 3,
          biocorelobjid: userObj.id,
        });
      if (cdidbio.length > 0) {
        let groupDatas = [];
        let orgAssign = [];
        let query;
        let query2;
        let resp;
        switch (parseInt(dcodeInfo.leveltenant)) {
          case 0:
            // await org.map((grp) => {
            //   console.log(grp);
            //   let groups = [];
            //   groups.push(userObj.id);
            //   groups.push(grp.id);
            //   groups.push(0);
            //   groupDatas.push(groups);
            // });
            // query =
            //   "INSERT INTO public.cdid_orguserassign(userid, orgid, grpid)  VALUES %L returning *";
            await org.map((grp) => {
              console.log(grp);
              let groups = { userid: userObj.id, orgid: grp.id, grpid: 0 };
              groupDatas.push(groups);
            });
            resp = await pool("cdid_orguserassign")
              .returning(["id", "userid"])
              .insert(groupDatas);
            break;
          case 1:
            // query2 =
            //   "INSERT INTO public.cdid_orguserassign(userid, orgid, grpid)  VALUES %L returning *";
            // await dcodeInfo.apps.map(async (app) => {
            //   let groups = [];
            //   groups.push(userObj.id);
            //   groups.push(app.orgid);
            //   groups.push(0);
            //   orgAssign.push(groups);
            // });

            await dcodeInfo.apps.map(async (app) => {
              // console.log(grp);
              let groups = { userid: userObj.id, orgid: app.orgid, grpid: 0 };
              orgAssign.push(groups);
            });
            resp = await pool("cdid_orguserassign").insert(orgAssign);

            // await group.map((grp) => {
            //   console.log(grp);
            //   let groups = [];
            //   groups.push(userObj.id);
            //   groups.push(grp.id);
            //   groups.push(dcodeInfo.idtenant);
            //   groups.push(apps.idapp);
            //   groups.push(dcodeInfo.id);
            //   groups.push(levetid);
            //   groupDatas.push(groups);
            // });
            // query =
            //   "INSERT INTO public.cdid_tenant_user_groupacl(iduser, idgroupuseracl, idtenant, idapplication, created_byid, grouptype) VALUES %L returning *";
            await group.map((grp) => {
              //console.log(grp);
              let groups = {
                iduser: userObj.id,
                idgroupuseracl: grp.id,
                idtenant: dcodeInfo.idtenant,
                idapplication: apps.idapp,
                created_byid: dcodeInfo.id,
                grouptype: levetid,
              };
              groupDatas.push(groups);
            });
            resp = await pool("cdid_tenant_user_groupacl")
              .returning(["iduser", idgroupuseracl])
              .insert(groupDatas);
            break;
          default:
            break;
        }
        console.log("askjd");
        console.log(resp);
        if (resp.length > 0) {
          const insertEventLog = await pool("trx_eventlog").insert({
            created_by: dcodeInfo.id,
            value: "Create Event",
            idapp: 0,
            idmenu: 0,
            description: "Create Users in krakatoa",
            cdaction: 5,
            refevent: toknid,
          });
          res.status(200).json({ status: 200, data: userObj });
        } else {
          res.status(500).json({
            status: 500,
            data: "Error insert cdid_orguserassign or cdid_tenant_user_groupacl ",
          });
        }
      }
    } else {
      res.status(500).json({ status: 500, data: "Error insert User" });
    }
  } catch (err) {
    console.log(err);
    next(err);

    res.status(500).json({ status: 500, data: "Error insert User" });
  }
});

router.post("/insertbyadmin", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var toknid = req.tokenID;
  try {
    var apps = dcodeInfo.apps[0];
    // const { fullname, userid, password, isactive, org, group, emailname } =
    //   req.body;
    const { fullname, userid, password, isactive, org, group, emailname, usercode,description,startdate,enddate,emplobj,suplobj, orgMlt,userlevel,tenantchoosed,emailname1,emailname2 } =
      req.body;
    // var levetid = parseInt(dcodeInfo.leveltenant);userlevel
    var levetid = userlevel;
    let validatePass = await userValidation.validatePassword(password);
    console.log("Pass is valid : ", validatePass);

    let validateUserId = await userValidation.validateUserId(userid);
    if (!validateUserId) {
      res
        .status(422)
        .json({ status: 422, data: "User id does not meet the criteria" });
      return;
    }

    let encryptedPass = await passEnc.encryptPass(password);
    // res.status(200).json({ status: 200, data: {hasil : apps.withapproval} });
    if (apps.withapproval > 0) {
      let resp = await pool("cdid_tenant_user")
        .returning([
          "id",
          "fullname",
          "userid",
          "pwd",
          "creator_stat",
          "creator_byid",
          "idtenant",
          "leveltenant",
          "active",
        ])
        .insert({
          fullname: fullname,
          userid: userid,
          pwd: encryptedPass,
          creator_stat: levetid,
          creator_byid: dcodeInfo.id,
          idtenant: dcodeInfo.idtenant,
          leveltenant: levetid,
          active: 3,
        });
      if (resp.length > 0) {
        var userObj = resp[0];
        console.log(">>>>>>>>>>>>>>>>>>>>>>>. ID APP " + resp[0]);
        console.log(resp[0]);
        console.log(">>>>>>>>>>>>>>>>>>>>>>>. ID APP " + resp[0]);
        // await passwordUtil.insertPasswordHistory(resp[0].id, password);
        await passwordUtil.insertPasswordHistory(resp[0], password);
        const cdidbio = await pool("mst_biodata_corell")
          .returning([
            "id",
            "bioname",
            "bioemailactive",
            "bioidcorel",
            "biocorelobjid",
          ])
          .insert({
            bioname: userObj.fullname,
            bioemailactive: emailname,
            bioidcorel: 3,
            biocorelobjid: userObj.id,
          });
        if (cdidbio.length > 0) {
          let groupDatas = [];
          let orgAssign = [];

          await dcodeInfo.apps.map(async (app) => {
            let groups = {
              userid: userObj.id,
              orgid: app.idorg,
              grpid: 0,
            };
            orgAssign.push(groups);
          });
          await group.map((grp) => {
            //console.log(grp);
            let groups = {
              iduser: userObj.id,
              idgroupuseracl: grp.id,
              idtenant: dcodeInfo.idtenant,
              idapplication: apps.idapp,
              created_byid: dcodeInfo.id,
              grouptype: levetid,
            };
            groupDatas.push(groups);
          });
          resp = await pool("cdid_orguserassign")
            .returning(["id", "userid"])
            .insert(orgAssign);
          resp = await pool("cdid_tenant_user_groupacl")
            .returning(["id", "iduser"])
            .insert(groupDatas);
          if (resp.length > 0) {
            const insertEventLog = await pool("trx_eventlog").insert({
              created_by: dcodeInfo.id,
              value: "Create Event",
              idapp: apps.idapp,
              idmenu: 0,
              description: "Create User in " + apps.applabel,
              cdaction: 6,
              refevent: toknid,
            });
            res.status(200).json({ status: 200, data: userObj });
          } else {
            res.status(500).json({
              status: 500,
              data: "Error insert cdid_orguserassign or cdid_tenant_user_groupacl ",
            });
          }
        }
      } else {
        res.status(500).json({ status: 500, data: "Error insert User" });
      }
    } else {
      console.log("Payload from FE ", req.body);
      // res.status(200).json({ status: 200, data: {} });
      //>>>>>>>>>>>>>>>>>>>>>
      let resp = await pool("cdid_tenant_user")
        .returning([
          "id",
          "fullname",
          "userid",
          "pwd",
          "creator_stat",
          "creator_byid",
          "idtenant",
          "leveltenant",
          "active",
        ])
        .insert({
          fullname: fullname,
          userid: userid,
          pwd: encryptedPass,
          creator_stat: levetid,
          creator_byid: dcodeInfo.id,
          idtenant: tenantchoosed.id,
          leveltenant: levetid,
          active: isactive,
          usercode: usercode
        });
      if (resp.length > 0) {
        var userObj = resp[0];
        console.log(">>>>>>>>>>>>>>>>>>>>>>>. ID User " + resp[0]);
        // await passwordUtil.insertPasswordHistory(resp[0].id, password);
        var cltid= null;
        var cltname= null;
        var empid = emplobj?.code;
        var empname = emplobj?.label;
        let stdate = moment(new Date(startdate)).format('YYYY-MM-DD');
        let eddate = moment(new Date(enddate)).format('YYYY-MM-DD');

//########################### USER MAPPING #################################
// {"fullname":"Ryan Muktiadhi",
    // "usercode":"555555555",
    // "userid":"RHMT001",
    // "password":"manage",
    // "group":[{"name":"Creator","id":1}],"isactive":1,
    // "emailname":"rahmat@argon.com",
    // "description":"testing",
    // "startdate":"2022-01-18T06:57:19.697Z",
    // "enddate":"2032-01-18T06:57:19.697Z",
    // "emplobj":{"code":"555555555","label":"Rahmat kartolo","email":"rahmat@argon.com","email2":null},
    // "suplobj":[],
    // "orgMlt":[{"name":"Creator","id":1}],
    // "userlevel":1,
    // "temppass":"manage",
    // "tenantchoosed":{"id":2,"tnname":"ANUGRAH ARGON MEDICA, PT","tnstatus":1,"tntype":1,"cdidowner":1,"tnflag":1,"tnparentid":1,"cdtenant":"AAMP","created_at":"2021-12-26T16:25:11.000Z","updated_at":"2021-12-26T16:25:11.000Z"},
    // "emailname1":"alfarysquad@gmail.com",
    // "emailname2":"alfarysquad@gmail.com"}

    if(levetid < 2) {
      let umappingpl = {payload: {
        idusrkkt:userObj, 
        usrkktname:userid,  
        dbinstance:null, 
        vendorid:null,
        vendorname:null,
        orgid:null,
        idtenant:tenantchoosed.id,
        targetemail1:emailname,
        targetemail2:emailname1,
        idtenantuser:userObj,
        created_by:dcodeInfo.id,
        companyname:tenantchoosed.tnname,
        companycode:tenantchoosed.cdtenant
    }, payloads:[]} 
      axios
      .post(
        conf.spbadminbe + "/spb/usermapping/insertusermap",umappingpl,
        {
          headers: { Authorization: authHeader },
        }
      )
      .then(async (resp) => {
        console.log("BALIKAN insert map >>>>>>>>>>>>>>> ",resp.data);
        // let datas = resp.data
        // res.status(200).json({ status: 200, data: datas });
      })
      .catch((err) => {
        console.log("Error API ", err);
        return;
      });
    } else if(levetid > 1) {
      console.log(">>>>>>>>>>>>>>> MASUKIN USER MAPPING ",suplobj) ;
      let umappingpl = {payload: {
        idusrkkt:userObj, 
        usrkktname:userid,  
        dbinstance:null, 
        vendorid:null,
        vendorname:null,
        orgid:null,
        idtenant:null,
        targetemail1:null,
        targetemail2:null,
        idtenantuser:userObj,
        created_by:dcodeInfo.id,
        companyname:null,
        companycode:null
    }, payloads:suplobj} 
      axios
      .post(
        conf.spbadminbe + "/spb/usermapping/insertusermap",umappingpl,
        {
          headers: { Authorization: authHeader },
        }
      )
      .then(async (resp) => {
        console.log("BALIKAN insert map >>>>>>>>>>>>>>> ",resp.data);
        // let datas = resp.data
        // res.status(200).json({ status: 200, data: datas });
      })
      .catch((err) => {
        console.log("Error API ", err);
        return;
      });






    }

       

//###############################################################################

        const cdidbio = await pool("mst_biodata_corell")
          .returning([
            "id",
            "bioname",
            "bioemailactive",
            "bioidcorel",
            "biocorelobjid",
            "description"
          ])
          .insert({
            bioname: fullname,
            bioemailactive: emailname,
            bioidcorel: 3,
            biocorelobjid: userObj,
            description:description,
            startdate:stdate,
            enddate:eddate,
            emailcc_1:emailname1,
            emailcc_2:emailname2,
            empid:empid,
            empname : empname
          });
          // console.log(">>>>>>>>>>>>>>>>>>>>>>>. BIO CORREL " + cdidbio);
        if (cdidbio.length > 0) {
          let groupDatas = [];
          let orgAssign = [];

          await dcodeInfo.apps.map(async (app) => {
            let groups = {
              userid: userObj,
              orgid: app.idorg,
              grpid: 0,
            };
            console.log(">>>>>>>>>>>>>>>>>>>>>>>. Setelah BIO COREL " + groups);
            orgAssign.push(groups);
          });

          await group.map((grp) => {
            console.log(">>>>> GROUP ITEM >>>>",grp);
            let groups = {
              iduser: userObj,
              idgroupuseracl: grp.id,
              idtenant: dcodeInfo.idtenant,
              idapplication: apps.idapp,
              created_byid: dcodeInfo.id,
              grouptype: levetid,
            };
            groupDatas.push(groups);
          });


          resp = await pool("cdid_orguserassign")
            .returning(["id", "userid"])
            .insert(orgAssign);

          console.log(">>>> ID USER DI GROUP >>>> ", groupDatas);


          resp = await pool("cdid_tenant_user_groupacl")
            .returning(["id", "iduser"])
            .insert(groupDatas);

          if (resp.length > 0) {






            const insertEventLog = await pool("trx_eventlog").insert({
              created_by: dcodeInfo.id,
              value: "Create Event",
              idapp: apps.idapp,
              idmenu: 0,
              description: "Create User in " + apps.applabel,
              cdaction: 6,
              refevent: toknid,
            });

            res.status(200).json({ status: 200, data: userObj });
          } else {
            res.status(500).json({
              status: 500,
              data: "Error insert cdid_orguserassign or cdid_tenant_user_groupacl ",
            });
          }
        }
      } else {
        res.status(500).json({ status: 500, data: "Error insert User" });
      }
    }
  } catch (err) {
    console.log(err);
    next(err);

    res.status(500).json({ status: 500, data: "Error insert User" });
  }
});

router.post("/updatebysupperuser", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var toknid = req.tokenID;
  var apps = dcodeInfo.apps[0];
  var levetid = parseInt(dcodeInfo.leveltenant);
  if (levetid == 0) {
    levetid = levetid + 1;
  }
  try {
    const { fullname, userid, isactive, org, group, emailname } = req.body;
    let now = new Date();
    now.setHours(now.getHours() + 7);
    const resp = await pool("cdid_tenant_user")
      .returning(["id", "fullname", "updated_at"])
      .where("id", userid)
      .update({
        fullname: fullname,
        updated_at: pool.fn.now(),
        active: isactive,
      });
    if (resp.length > 0) {
      var userObj = resp[0];
      const cdidbio = await pool("mst_biodata_corell")
        .returning(["id", "bioname", "updated_at"])
        .where("biocorelobjid", userid)
        .update({
          bioname: userObj.fullname,
          bioemailactive: emailname,
          updated_at: pool.fn.now(),
        });
      var leveluser = parseInt(dcodeInfo.leveltenant);
      let assignData = [];
      let groupDatas = [];

      await pool("cdid_orguserassign")
        .where({ userid: userid })
        .del()
        .then(async () => {
          console.log("level user eidt");
          console.log(leveluser);
          if (leveluser == 0) {
            await org.map((app) => {
              // orgid: app.orgid,
              let groups = {
                userid: userObj.id,
                orgid: app.id,
                grpid: 0,
              };
              assignData.push(groups);
            });

            console.log("assignData");
            console.log(assignData);
            await pool("cdid_orguserassign").insert(assignData);
          } else if (leveluser == 1) {
            await pool("cdid_tenant_user_groupacl")
              .where({ iduser: userid })
              .del();

            await group.map((grp) => {
              let groups = {
                iduser: userObj.id,
                idgroupuseracl: grp.id,
                idtenant: dcodeInfo.idtenant,
                idapplication: apps.idapp,
                created_byid: dcodeInfo.id,
                grouptype: levetid,
              };
              groupDatas.push(groups);
            });

            await dcodeInfo.apps.map((app) => {
              let groups = {
                userid: userObj.id,
                orgid: app.orgid,
                grpid: 0,
              };
              assignData.push(groups);
            });

            await pool("cdid_tenant_user_groupacl").insert(groupDatas);
            await pool("cdid_orguserassign").insert(assignData);
          }
          await pool("trx_eventlog").insert({
            created_by: dcodeInfo.id,
            value: "Update Event",
            idapp: 0,
            idmenu: 0,
            description: "Update user in krakatoa",
            cdaction: 6,
            refevent: toknid,
          });
          res.status(200).json({ status: 200, data: userObj });
        })
        .catch((err) => {
          console.log(err);
          res.status(500).json({
            status: 500,
            data: "Error update cdid_orguserassign or cdid_tenant_user_groupacl ",
          });
        });
    } else {
      res.status(500).json({ status: 500, data: "Error update User" });
    }
  } catch (err) {
    console.log(err);
    next(err);

    res.status(500).json({ status: 500, data: "Error update User" });
  }
});

router.post("/updatebyadmin", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var toknid = req.tokenID;
  var apps = dcodeInfo.apps[0];
  const { id, fullname, userid, password, isactive, org, group, emailname, usercode,description,startdate,enddate,emplobj,suplobj, orgMlt,userlevel,temppass, tenantchoosed,emailname1,emailname2 } =
  req.body;
  // var levetid = parseInt(dcodeInfo.leveltenant) + 1;
  // var levetid = parseInt(dcodeInfo.leveltenant);
  var levetid = userlevel;
  if (levetid == 0) {
    levetid = levetid + 1;
  }
  // const { id, fullname, userid, isactive, org, group, emailname } = req.body;
  
  try {
    // let encryptedPass = await passEnc.encryptPass(password);
    if (apps.withapproval > 0) {
      const insertApprovalTmp = await pool("trx_approvaltmp")
        .returning([
          "id",
          "idapp",
          "idtenant",
          "cdmenu",
          "jsondata",
          "status",
          "typedata",
        ])
        .insert({
          idapp: apps.idapp,
          idtenant: dcodeInfo.idtenant,
          cdmenu: "USRMN",
          jsondata: req.body,
          status: 0,
          typedata: 5,
        });
      if (insertApprovalTmp.length > 0) {
        var userObj = insertApprovalTmp[0];
        let idreturn = userObj.id;
        const resp = await pool("cdid_tenant_user")
          .returning(["id", "fullname", "updated_at"])
          .where("id", userid)
          .update({
            fullname: fullname,
            updated_at: pool.fn.now(),
            active: isactive == 1 ? 7 : 5,
            idapproval: idreturn,
          });
        if (resp.length > 0) {
          var userObj = resp[0];
          res.status(200).json({ status: 200, data: userObj });
        }
      }
    } else {
      let now = new Date();
      now.setHours(now.getHours() + 7);
      const resp = await pool("cdid_tenant_user")
        .returning(["id", "fullname", "updated_at"])
        .where("id", id)
        .update({
          fullname: fullname,
          pwd: password,
          updated_at: pool.fn.now(),
          userid:userid,
          idtenant: tenantchoosed.id,
          leveltenant: levetid,
          active: isactive,
          usercode: usercode
        });
        console.log(">>>>>>>>>>>>>>>>>>>>>>>. UPDATE CDID_TENANT " + resp);
      if (resp > 0) {
        var userObj = resp;
        
        // bioname: fullname,
        //     bioemailactive: emailname,
        //     bioidcorel: 3,
        //     biocorelobjid: userObj,
        //     description:description,
        //     startdate:stdate,
        //     enddate:eddate,
        //     emailcc_1:emailname1,
        //     emailcc_2:emailname2,
        //     empid:empid,
        //     empname : empname
        var cltid= null;
        var cltname= null;
        var empid = emplobj?.code;
        var empname = emplobj?.label;
        let stdate = moment(new Date(startdate)).format('YYYY-MM-DD');
        let eddate = moment(new Date(enddate)).format('YYYY-MM-DD');
        const cdidbio = await pool("mst_biodata_corell")
          .returning(["id", "bioname", "updated_at"])
          .where({"biocorelobjid": id, "bioidcorel":3})
          .update({
            bioname: fullname,
            bioemailactive: emailname,
            description:description,
            startdate:stdate,
            enddate:eddate,
            emailcc_1:emailname1,
            emailcc_2:emailname2,
            empid:empid,
            empname : empname,
            updated_at: pool.fn.now(),
          });
        var leveluser = parseInt(dcodeInfo.leveltenant);
        let assignData = [];
        let groupDatas = [];
        await pool("cdid_orguserassign")
          .where({ userid: id })
          .del()
          .then(async () => {
            if (leveluser == 0) {
              await pool("cdid_orguserassign").where({ userid: userid }).del();
              // orgid: app.orgid,
              await org.map((app) => {
                let groups = {
                  userid: userObj.id,
                  orgid: app.id,
                  grpid: 0,
                };
                assignData.push(groups);
              });
              await pool("cdid_orguserassign").insert(assignData);
            } else if (leveluser == 1) {
              await pool("cdid_orguserassign").where({ userid: id }).del();
              await pool("cdid_tenant_user_groupacl")
                .where({ iduser: id })
                .del();

              await group.map((grp) => {
                let groups = {
                  iduser: id,
                  idgroupuseracl: grp.id,
                  idtenant: tenantchoosed.id,
                  idapplication: apps.idapp,
                  created_byid: dcodeInfo.id,
                  grouptype: levetid,
                };
                groupDatas.push(groups);
              });

              await dcodeInfo.apps.map((app) => {
                let groups = {
                  userid: id,
                  orgid: app.idorg,
                  grpid: 0,
                };
                assignData.push(groups);
              });
              await pool("cdid_tenant_user_groupacl").insert(groupDatas);
              await pool("cdid_orguserassign").insert(assignData);
            }
            await pool("trx_eventlog").insert({
              created_by: dcodeInfo.id,
              value: "Update Event",
              idapp: 0,
              idmenu: 0,
              description: "Update user in krakatoa",
              cdaction: 6,
              refevent: toknid,
            });
            res.status(200).json({ status: 200, data: userObj });
          })
          .catch((err) => {
            console.log(err);
            res.status(500).json({
              status: 500,
              data: "Error update cdid_orguserassign or cdid_tenant_user_groupacl ",
            });
          });
      } else {
        res.status(500).json({ status: 500, data: "Error update User" });
      }
    }
  } catch (err) {
    console.log(err);
    next(err);

    res.status(500).json({ status: 500, data: "Error update User" });
  }
});

router.post("/updatebyadminactive", async (req, res, next) => {
  var dcodeInfo = req.userData;
  var toknid = req.tokenID;
  var apps = dcodeInfo.apps[0];
  // var levetid = parseInt(dcodeInfo.leveltenant) + 1;
  var levetid = parseInt(dcodeInfo.leveltenant);
  if (levetid == 0) {
    levetid = levetid + 1;
  }
  try {
    const { id, oldactive, isactive, idapproval } = req.body;
    let now = new Date();
    now.setHours(now.getHours() + 7);
    console.log(">>>>>>>>>>>>>>>>>>>>>>>.JSON SSSSSSSSSSSSSSSSSSSSS");
    console.log(req.body);
    switch (oldactive) {
      case 3:
        // code block
        const resp = await pool("cdid_tenant_user")
          .returning(["id", "fullname", "updated_at"])
          .where("id", id)
          .update({
            active: isactive,
          });
        if (resp.length > 0) {
          var userObj = resp[0];
          //console.log(">>>>>>>>>>>>>>>>>>>>>>>. ID APP " + apps.id_application);
          res.status(200).json({ status: 200, data: userObj });
        } else {
          res.status(500).json({ status: 500, data: "Error update User" });
        }

        break;
      case 5:
      case 7:
        console.log(">>>>>>>>>>>>>>>>>>>>>>>.JSON SSSSSSSSSSSSSSSSSSSSS");
        let respAppTmp = await pool
          .select("jsondata")
          .from("trx_approvaltmp")
          .where({ id: idapproval, status: 0 });
        if (respAppTmp.length > 0) {
          let jsdata = respAppTmp[0];
          let jdata = JSON.parse(jsdata.jsondata);
          // console.log(jdata);
          const respCtu = await pool("cdid_tenant_user")
            .returning(["id", "fullname", "updated_at"])
            .where("id", jdata.userid)
            .update({
              fullname: jdata.fullname,
              updated_at: pool.fn.now(),
              active: jdata.isactive,
            });
          if (respCtu.length > 0) {
            var userObj = respCtu[0];
            const cdidbioRsp = await pool("mst_biodata_corell")
              .returning(["id", "bioname", "updated_at"])
              .where("biocorelobjid", jdata.userid)
              .update({
                bioname: userObj.fullname,
                bioemailactive: jdata.emailname,
                updated_at: pool.fn.now(),
              });
            var leveluser = parseInt(dcodeInfo.leveltenant);
            let assignData = [];
            let groupDatas = [];
            await pool("cdid_orguserassign")
              .where({ userid: jdata.userid })
              .del()
              .then(async () => {
                await pool("cdid_orguserassign")
                  .where({ userid: jdata.userid })
                  .del();
                await pool("cdid_tenant_user_groupacl")
                  .where({ iduser: jdata.userid })
                  .del();
                await jdata.group.map((grp) => {
                  let groups = {
                    iduser: userObj.id,
                    idgroupuseracl: grp.id,
                    idtenant: dcodeInfo.idtenant,
                    idapplication: apps.idapp,
                    created_byid: dcodeInfo.id,
                    grouptype: levetid,
                  };
                  groupDatas.push(groups);
                });

                await dcodeInfo.apps.map((app) => {
                  let groups = {
                    userid: userObj.id,
                    orgid: app.idorg,
                    grpid: 0,
                  };
                  assignData.push(groups);
                });
                await pool("cdid_tenant_user_groupacl").insert(groupDatas);
                await pool("cdid_orguserassign").insert(assignData);
                await pool("trx_eventlog").insert({
                  created_by: dcodeInfo.id,
                  value: "Update Event",
                  idapp: 0,
                  idmenu: 0,
                  description: "Update user in Lp,o",
                  cdaction: 6,
                  refevent: toknid,
                });
                res.status(200).json({ status: 200, data: userObj });
              })
              .catch((err) => {
                console.log(err);
                res.status(500).json({
                  status: 500,
                  data: "Error update cdid_orguserassign or cdid_tenant_user_groupacl ",
                });
              });
          } else {
            res.status(500).json({ status: 500, data: "Error update User" });
          }
        }
        break;
      case 9:
        console.log(">>>>>>>>>>>>>>>>>>>>>>>.JSON SSSSSSSSSSSSSSSSSSSSS");
        let respDel;
        let respAppDelTmp = await pool
          .select("jsondata")
          .from("trx_approvaltmp")
          .where({ id: idapproval, status: 0 });
        if (respAppDelTmp.length > 0) {
          let jsdata = respAppDelTmp[0];
          let userId = JSON.parse(jsdata.jsondata);
          console.log(jsdata);
          respDel = await pool("cdid_tenant_user").where({ id: userId }).del();
          respDel = await pool("mst_biodata_corell")
            .where({ biocorelobjid: userId })
            .del();
          var leveluser = parseInt(dcodeInfo.leveltenant);
          if (leveluser == 0) {
            respDel = await pool("cdid_orguserassign")
              .where({ userid: userId })
              .del();
          } else {
            respDel = await pool("cdid_orguserassign")
              .where({ userid: userId })
              .del();
            respDel = await pool("cdid_tenant_user_groupacl")
              .where({ iduser: userId })
              .del();
          }
          await pool("trx_eventlog").insert({
            created_by: dcodeInfo.id,
            value: "Delete Event",
            idapp: 0,
            idmenu: 0,
            description: "Delete User in " + apps.applabel,
            cdaction: 8,
            refevent: toknid,
          });
          res.status(200).json({ status: 200, data: "Success" });
        }

        break;
      default:
      // code block
    }
  } catch (err) {
    console.log(err);
    next(err);

    res.status(500).json({ status: 500, data: "Error update User" });
  }
});

router.get("/deletebysuper/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var toknid = req.tokenID;
  var apps = dcodeInfo.apps[0];
  let userId = req.params.id;
  try {
    let resp;
    resp = await pool("cdid_tenant_user").where({ id: userId }).del();
    resp = await pool("mst_biodata_corell")
      .where({ biocorelobjid: userId })
      .del();
    var leveluser = parseInt(dcodeInfo.leveltenant);
    if (leveluser == 0) {
      resp = await pool("cdid_orguserassign").where({ userid: userId }).del();
    } else {
      resp = await pool("cdid_orguserassign").where({ userid: userId }).del();
      resp = await pool("cdid_tenant_user_groupacl")
        .where({ iduser: userId })
        .del();
    }

    await pool("trx_eventlog").insert({
      created_by: dcodeInfo.id,
      value: "Delete Event",
      idapp: 0,
      idmenu: 0,
      description: "Delete User in kraktoa",
      cdaction: 7,
      refevent: toknid,
    });
    res.status(200).json({ status: 200, data: "Success" });
  } catch (err) {
    console.log(err);
    next(err);

    res.status(500).json({ status: 500, data: "Error retrive Users" });
  }
});

router.get("/deletebyadmin/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var toknid = req.tokenID;
  var apps = dcodeInfo.apps[0];
  let userId = req.params.id;
  try {
    let resp;
    if (apps.withapproval > 0) {
      const insertApprovalTmp = await pool("trx_approvaltmp")
        .returning([
          "id",
          "idapp",
          "idtenant",
          "cdmenu",
          "jsondata",
          "status",
          "typedata",
        ])
        .insert({
          idapp: apps.idapp,
          idtenant: dcodeInfo.idtenant,
          cdmenu: "USRMN",
          jsondata: userId,
          status: 0,
          typedata: 9,
        });
      if (insertApprovalTmp.length > 0) {
        var userObj = insertApprovalTmp[0];
        let idreturn = userObj.id;
        const resp = await pool("cdid_tenant_user")
          .returning(["id", "fullname", "updated_at"])
          .where("id", userId)
          .update({
            active: 9,
            idapproval: idreturn,
          });
        if (resp.length > 0) {
          var userObj = resp[0];
          res.status(200).json({ status: 200, data: userObj });
        }
      }
    } else {
      resp = await pool("cdid_tenant_user").where({ id: userId }).del();
      axios
          .get(
            conf.spbadminbe + "/spb/usermapping/deleteusermap/"+userId,
            {
              headers: { Authorization: authHeader },
            }
          )
          .then(async (resp) => {
            console.log("BALIKAN Delete map >>>>>>>>>>>>>>> ",resp.data);
            // let datas = resp.data
            // res.status(200).json({ status: 200, data: datas });
          })
          .catch((err) => {
            console.log("Error API ", err);
            return;
          });
      resp = await pool("mst_biodata_corell")
        .where({ biocorelobjid: userId })
        .del();
      var leveluser = parseInt(dcodeInfo.leveltenant);
      if (leveluser == 0) {
        resp = await pool("cdid_orguserassign").where({ userid: userId }).del();
      } else {
        resp = await pool("cdid_orguserassign").where({ userid: userId }).del();
        resp = await pool("cdid_tenant_user_groupacl")
          .where({ iduser: userId })
          .del();
      }

      await pool("trx_eventlog").insert({
        created_by: dcodeInfo.id,
        value: "Delete Event",
        idapp: 0,
        idmenu: 0,
        description: "Delete User in " + apps.applabel,
        cdaction: 8,
        refevent: toknid,
      });
      res.status(200).json({ status: 200, data: "Success" });
    }
  } catch (err) {
    console.log(err);
    next(err);

    res.status(500).json({ status: 500, data: "Error retrive Users" });
  }
});

router.get("/retriveemployeebycompany/:company", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];
  var token = req.tokenID;
  let company = req.params.company;
  console.log(">>>>>>> TOKEN ", authHeader);
  try {
    // var leveluser = parseInt(dcodeInfo.leveltenant);
    axios
      .get(
        conf.spbadminbe + "/spb/employees/getemployeebyCompany/"+company,
        {
          headers: { Authorization: authHeader },
        }
      )
      .then(async (resp) => {
        // console.log("BALIKAN >>>>>>>>>>>>>>> ",resp.data);
        let datas = resp.data
        res.status(200).json({ status: 200, data: datas });
      })
      .catch((err) => {
        console.log("Error API ", err);
        return;
      });
  } catch (err) {
    console.log(err);
    next(err);

    res.status(500).json({ status: 500, data: "Error retrive Users" });
  }
});


router.get("/retrivecompaniesbyuser/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];
  var token = req.tokenID;
  let id = req.params.id;
  console.log(">>>>>>> TOKEN ", authHeader);
  try {
    // var leveluser = parseInt(dcodeInfo.leveltenant);
    axios
      .get(
        conf.spbadminbe + "/spb/usermapping/getusermappingsbyiduser/"+id,
        {
          headers: { Authorization: authHeader },
        }
      )
      .then(async (resp) => {
        console.log("BALIKAN >>>>>>>>>>>>>>> ",resp.data);
        let datas = resp.data
        res.status(200).json({ status: 200, data: datas });
      })
      .catch((err) => {
        console.log("Error API ", err);
        return;
      });
  } catch (err) {
    console.log(err);
    next(err);

    res.status(500).json({ status: 500, data: "Error retrive Users" });
  }
});









module.exports = router;
// router.get("/retriveusersadmin", async (req, res, next) => {
//   // const authHeader = req.headers.authorization;
//   var dcodeInfo = req.userData;
//   console.log(req.userData);
//   var apps = dcodeInfo.apps[0];
//   // res.status(200).json({ status: 200, data: apps.id_application });
//   try {
//     var leveluser = parseInt(dcodeInfo.leveltenant);
//     console.log(">>>>>>>>>> LEVEL USER PAGE USER " + leveluser);
//     var query = "";
//     console.log(">>>>>>>>>> CASE PAGE USER " + leveluser);
//     console.log(dcodeInfo.idtenant, leveluser, apps.idapp);

//     // console.log(dcodeInfo);
//     // query =
//     //   "SELECT DISTINCT ON (tu.id) tu.id,tu.leveltenant, tu.fullname, tu.userid,tu.total_attempt, tu.pwd, tu.creator_stat, tu.creator_byid, to_char(tu.created_at, 'YYYY-MM-DD HH:mm:ss') created_at,tu.updated_at, tu.idtenant, tu.idsubtenant,tu.leveltenant, tu.active, ct.tnname tenantname,'' subtenantname,tu2.fullname creatorname,tug.idgroupuseracl, mug.groupname FROM public.cdid_tenant_user tu INNER JOIN public.cdid_tenant ct ON tu.idtenant = ct.id INNER JOIN public.cdid_tenant_user tu2 ON tu.creator_byid = tu2.id INNER JOIN public.cdid_tenant_user_groupacl tug ON tu.id = tug.iduser INNER JOIN mst_usergroup mug ON tug.idgroupuseracl = mug.id WHERE tu.leveltenant >= $1 and tug.idapplication = $2 and ct.id= " +
//     //   dcodeInfo.idtenant;

//     let resp = await pool
//       .select(
//         "tu.id",
//         "tu.leveltenant",
//         "tu.fullname",
//         "tu.userid",
//         "tu.total_attempt",
//         "tu.pwd",
//         "tu.creator_stat",
//         "tu.creator_byid",
//         "tu.created_at as created_at",
//         "tu.updated_at",
//         "tu.idtenant",
//         "tu.idsubtenant",
//         "tu.leveltenant",
//         "tu.active",
//         "ct.tnname  as tenantname",
//         "tu2.fullname as  creatorname",
//         "tug.idgroupuseracl",
//         "mug.groupname"
//       )
//       .from("cdid_tenant_user as tu")
//       .innerJoin("cdid_tenant as ct", "tu.idtenant", "ct.id")
//       .innerJoin("cdid_tenant_user as tu2", "tu.creator_byid", "tu2.id")
//       .innerJoin("cdid_tenant_user_groupacl as tug", "tu.id", "tug.iduser")
//       .innerJoin("mst_usergroup as mug", "tug.idgroupuseracl", "mug.id")
//       .where({
//         "tug.idapplication": apps.idapp,
//         "ct.id": dcodeInfo.idtenant,
//       })
//       .andWhere("tu.leveltenant", ">=", leveluser);

//     console.log(resp);
//     if (resp.length > 0) {
//       res.status(200).json({ status: 200, data: resp });
//     } else {
//       // res.status(500).json({ status: 500, data: "Error retrive Users" });
//       setTimeout(function () {
//         res.status(200).json({ status: 202, data: resp });
//       }, 500);
//     }
//   } catch (err) {
//     console.log(err);
//     next(err);

//     res.status(500).json({ status: 500, data: "Error retrive Users" });
//   }
// });



// if (apps.withapproval > 0) {
//   let resp = await pool("cdid_tenant_user")
//     .returning([
//       "id",
//       "fullname",
//       "userid",
//       "pwd",
//       "creator_stat",
//       "creator_byid",
//       "idtenant",
//       "leveltenant",
//       "active",
//     ])
//     .insert({
//       fullname: fullname,
//       userid: userid,
//       pwd: encryptedPass,
//       creator_stat: levetid,
//       creator_byid: dcodeInfo.id,
//       idtenant: dcodeInfo.idtenant,
//       leveltenant: levetid,
//       active: 3,
//     });
//   if (resp.length > 0) {
//     var userObj = resp[0];
//     console.log(">>>>>>>>>>>>>>>>>>>>>>>. ID APP " + resp[0]);
//     console.log(resp[0]);
//     console.log(">>>>>>>>>>>>>>>>>>>>>>>. ID APP " + resp[0]);
//     await passwordUtil.insertPasswordHistory(resp[0].id, password);
//     const cdidbio = await pool("mst_biodata_corell")
//       .returning([
//         "id",
//         "bioname",
//         "bioemailactive",
//         "bioidcorel",
//         "biocorelobjid",
//       ])
//       .insert({
//         bioname: userObj.fullname,
//         bioemailactive: emailname,
//         bioidcorel: 3,
//         biocorelobjid: userObj.id,
//       });
//     if (cdidbio.length > 0) {
//       let groupDatas = [];
//       let orgAssign = [];

//       await dcodeInfo.apps.map(async (app) => {
//         let groups = {
//           userid: userObj.id,
//           orgid: app.idorg,
//           grpid: 0,
//         };
//         orgAssign.push(groups);
//       });
//       await group.map((grp) => {
//         //console.log(grp);
//         let groups = {
//           iduser: userObj.id,
//           idgroupuseracl: grp.id,
//           idtenant: dcodeInfo.idtenant,
//           idapplication: apps.idapp,
//           created_byid: dcodeInfo.id,
//           grouptype: levetid,
//         };
//         groupDatas.push(groups);
//       });
//       resp = await pool("cdid_orguserassign")
//         .returning(["id", "userid"])
//         .insert(orgAssign);
//       resp = await pool("cdid_tenant_user_groupacl")
//         .returning(["id", "iduser"])
//         .insert(groupDatas);
//       if (resp.length > 0) {
//         const insertEventLog = await pool("trx_eventlog").insert({
//           created_by: dcodeInfo.id,
//           value: "Create Event",
//           idapp: apps.idapp,
//           idmenu: 0,
//           description: "Create User in " + apps.applabel,
//           cdaction: 6,
//           refevent: toknid,
//         });
//         res.status(200).json({ status: 200, data: userObj });
//       } else {
//         res.status(500).json({
//           status: 500,
//           data: "Error insert cdid_orguserassign or cdid_tenant_user_groupacl ",
//         });
//       }
//     }
//   } else {
//     res.status(500).json({ status: 500, data: "Error insert User" });
//   }
// } else {
//   let resp = await pool("cdid_tenant_user")
//     .returning([
//       "id",
//       "fullname",
//       "userid",
//       "pwd",
//       "creator_stat",
//       "creator_byid",
//       "idtenant",
//       "leveltenant",
//       "active",
//     ])
//     .insert({
//       fullname: fullname,
//       userid: userid,
//       pwd: encryptedPass,
//       creator_stat: levetid,
//       creator_byid: dcodeInfo.id,
//       idtenant: dcodeInfo.idtenant,
//       leveltenant: levetid,
//       active: isactive,
//     });
//   if (resp.length > 0) {
//     var userObj = resp[0];
//     console.log(">>>>>>>>>>>>>>>>>>>>>>>. ID User " + resp[0]);
//     // await passwordUtil.insertPasswordHistory(resp[0].id, password);

//     const cdidbio = await pool("mst_biodata_corell")
//       .returning([
//         "id",
//         "bioname",
//         "bioemailactive",
//         "bioidcorel",
//         "biocorelobjid",
//       ])
//       .insert({
//         bioname: fullname,
//         bioemailactive: emailname,
//         bioidcorel: 3,
//         biocorelobjid: userObj,
//       });
//     if (cdidbio.length > 0) {
//       let groupDatas = [];
//       let orgAssign = [];

//       await dcodeInfo.apps.map(async (app) => {
//         let groups = {
//           userid: userObj,
//           orgid: app.idorg,
//           grpid: 0,
//         };
//         orgAssign.push(groups);
//       });

//       await group.map((grp) => {
//         console.log(">>>>> GROUP ITEM >>>>",grp);
//         let groups = {
//           iduser: userObj,
//           idgroupuseracl: grp.id,
//           idtenant: dcodeInfo.idtenant,
//           idapplication: apps.idapp,
//           created_byid: dcodeInfo.id,
//           grouptype: levetid,
//         };
//         groupDatas.push(groups);
//       });
//       resp = await pool("cdid_orguserassign")
//         .returning(["id", "userid"])
//         .insert(orgAssign);

//       console.log(">>>> ID USER DI GROUP >>>> ", groupDatas);


//       resp = await pool("cdid_tenant_user_groupacl")
//         .returning(["id", "iduser"])
//         .insert(groupDatas);

//       if (resp.length > 0) {
//         const insertEventLog = await pool("trx_eventlog").insert({
//           created_by: dcodeInfo.id,
//           value: "Create Event",
//           idapp: apps.idapp,
//           idmenu: 0,
//           description: "Create User in " + apps.applabel,
//           cdaction: 6,
//           refevent: toknid,
//         });
//         res.status(200).json({ status: 200, data: userObj });
//       } else {
//         res.status(500).json({
//           status: 500,
//           data: "Error insert cdid_orguserassign or cdid_tenant_user_groupacl ",
//         });
//       }
//     }
//   } else {
//     res.status(500).json({ status: 500, data: "Error insert User" });
//   }
// }



// router.post("/insertbyadmin", async (req, res, next) => {
//   const authHeader = req.headers.authorization;
//   var dcodeInfo = req.userData;
//   var toknid = req.tokenID;
//   try {
//     var apps = dcodeInfo.apps[0];
//     // const { fullname, userid, password, isactive, org, group, emailname } =
//     //   req.body;
//     const { fullname, userid, password, isactive, org, group, emailname, usercode,description,startdate,enddate,emplobj,suplobj, orgMlt,userlevel,tenantchoosed,emailname1,emailname2 } =
//       req.body;
//     // var levetid = parseInt(dcodeInfo.leveltenant);userlevel
//     var levetid = userlevel;
//     let validatePass = await userValidation.validatePassword(password);
//     console.log("Pass is valid : ", validatePass);
    
//     // if (!validatePass) {
//     //   res
//     //     .status(422)
//     //     .json({ status: 422, data: "Password does not meet the criteria" });
//     //   return;
//     // }
//     let validateUserId = await userValidation.validateUserId(userid);
//     if (!validateUserId) {
//       res
//         .status(422)
//         .json({ status: 422, data: "User id does not meet the criteria" });
//       return;
//     }

//     let encryptedPass = await passEnc.encryptPass(password);
//     // res.status(200).json({ status: 200, data: {hasil : apps.withapproval} });
//     if (apps.withapproval > 0) {
//       let resp = await pool("cdid_tenant_user")
//         .returning([
//           "id",
//           "fullname",
//           "userid",
//           "pwd",
//           "creator_stat",
//           "creator_byid",
//           "idtenant",
//           "leveltenant",
//           "active",
//         ])
//         .insert({
//           fullname: fullname,
//           userid: userid,
//           pwd: encryptedPass,
//           creator_stat: levetid,
//           creator_byid: dcodeInfo.id,
//           idtenant: dcodeInfo.idtenant,
//           leveltenant: levetid,
//           active: 3,
//         });
//       if (resp.length > 0) {
//         var userObj = resp[0];
//         console.log(">>>>>>>>>>>>>>>>>>>>>>>. ID APP " + resp[0]);
//         console.log(resp[0]);
//         console.log(">>>>>>>>>>>>>>>>>>>>>>>. ID APP " + resp[0]);
//         await passwordUtil.insertPasswordHistory(resp[0].id, password);
//         const cdidbio = await pool("mst_biodata_corell")
//           .returning([
//             "id",
//             "bioname",
//             "bioemailactive",
//             "bioidcorel",
//             "biocorelobjid",
//           ])
//           .insert({
//             bioname: userObj.fullname,
//             bioemailactive: emailname,
//             bioidcorel: 3,
//             biocorelobjid: userObj.id,
//           });
//         if (cdidbio.length > 0) {
//           let groupDatas = [];
//           let orgAssign = [];

//           await dcodeInfo.apps.map(async (app) => {
//             let groups = {
//               userid: userObj.id,
//               orgid: app.idorg,
//               grpid: 0,
//             };
//             orgAssign.push(groups);
//           });
//           await group.map((grp) => {
//             //console.log(grp);
//             let groups = {
//               iduser: userObj.id,
//               idgroupuseracl: grp.id,
//               idtenant: dcodeInfo.idtenant,
//               idapplication: apps.idapp,
//               created_byid: dcodeInfo.id,
//               grouptype: levetid,
//             };
//             groupDatas.push(groups);
//           });
//           resp = await pool("cdid_orguserassign")
//             .returning(["id", "userid"])
//             .insert(orgAssign);
//           resp = await pool("cdid_tenant_user_groupacl")
//             .returning(["id", "iduser"])
//             .insert(groupDatas);
//           if (resp.length > 0) {
//             const insertEventLog = await pool("trx_eventlog").insert({
//               created_by: dcodeInfo.id,
//               value: "Create Event",
//               idapp: apps.idapp,
//               idmenu: 0,
//               description: "Create User in " + apps.applabel,
//               cdaction: 6,
//               refevent: toknid,
//             });
//             res.status(200).json({ status: 200, data: userObj });
//           } else {
//             res.status(500).json({
//               status: 500,
//               data: "Error insert cdid_orguserassign or cdid_tenant_user_groupacl ",
//             });
//           }
//         }
//       } else {
//         res.status(500).json({ status: 500, data: "Error insert User" });
//       }
//     } else {
//       console.log("Payload from FE ", req.body);
//       // res.status(200).json({ status: 200, data: {} });
//       //>>>>>>>>>>>>>>>>>>>>>
//       let resp = await pool("cdid_tenant_user")
//         .returning([
//           "id",
//           "fullname",
//           "userid",
//           "pwd",
//           "creator_stat",
//           "creator_byid",
//           "idtenant",
//           "leveltenant",
//           "active",
//         ])
//         .insert({
//           fullname: fullname,
//           userid: userid,
//           pwd: encryptedPass,
//           creator_stat: levetid,
//           creator_byid: dcodeInfo.id,
//           idtenant: tenantchoosed.id,
//           leveltenant: levetid,
//           active: isactive,
//           usercode: usercode
//         });
//       if (resp.length > 0) {
//         var userObj = resp[0];
//         console.log(">>>>>>>>>>>>>>>>>>>>>>>. ID User " + resp[0]);
//         // await passwordUtil.insertPasswordHistory(resp[0].id, password);
//         var cltid= null;
//         var cltname= null;
//         var empid = emplobj?.code;
//         var empname = emplobj?.label;
//         let stdate = moment(new Date(startdate)).format('YYYY-MM-DD');
//         let eddate = moment(new Date(enddate)).format('YYYY-MM-DD');
//         const cdidbio = await pool("mst_biodata_corell")
//           .returning([
//             "id",
//             "bioname",
//             "bioemailactive",
//             "bioidcorel",
//             "biocorelobjid",
//             "description"
//           ])
//           .insert({
//             bioname: fullname,
//             bioemailactive: emailname,
//             bioidcorel: 3,
//             biocorelobjid: userObj,
//             description:description,
//             startdate:stdate,
//             enddate:eddate,
//             emailcc_1:emailname1,
//             emailcc_2:emailname2,
//             empid:empid,
//             empname : empname
//           });
//           // console.log(">>>>>>>>>>>>>>>>>>>>>>>. BIO CORREL " + cdidbio);
//         if (cdidbio.length > 0) {
//           let groupDatas = [];
//           let orgAssign = [];

//           await dcodeInfo.apps.map(async (app) => {
//             let groups = {
//               userid: userObj,
//               orgid: app.idorg,
//               grpid: 0,
//             };
//             console.log(">>>>>>>>>>>>>>>>>>>>>>>. Setelah BIO COREL " + groups);
//             orgAssign.push(groups);
//           });

//           await group.map((grp) => {
//             console.log(">>>>> GROUP ITEM >>>>",grp);
//             let groups = {
//               iduser: userObj,
//               idgroupuseracl: grp.id,
//               idtenant: dcodeInfo.idtenant,
//               idapplication: apps.idapp,
//               created_byid: dcodeInfo.id,
//               grouptype: levetid,
//             };
//             groupDatas.push(groups);
//           });


//           resp = await pool("cdid_orguserassign")
//             .returning(["id", "userid"])
//             .insert(orgAssign);

//           console.log(">>>> ID USER DI GROUP >>>> ", groupDatas);


//           resp = await pool("cdid_tenant_user_groupacl")
//             .returning(["id", "iduser"])
//             .insert(groupDatas);

//           if (resp.length > 0) {
//             const insertEventLog = await pool("trx_eventlog").insert({
//               created_by: dcodeInfo.id,
//               value: "Create Event",
//               idapp: apps.idapp,
//               idmenu: 0,
//               description: "Create User in " + apps.applabel,
//               cdaction: 6,
//               refevent: toknid,
//             });
//             res.status(200).json({ status: 200, data: userObj });
//           } else {
//             res.status(500).json({
//               status: 500,
//               data: "Error insert cdid_orguserassign or cdid_tenant_user_groupacl ",
//             });
//           }
//         }
//       } else {
//         res.status(500).json({ status: 500, data: "Error insert User" });
//       }
//     }
//   } catch (err) {
//     console.log(err);
//     next(err);

//     res.status(500).json({ status: 500, data: "Error insert User" });
//   }
// });