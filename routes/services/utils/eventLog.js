const pool = require("../../../connection/db");

const insertLog = async (user, appId, description, action, tokenID, type) => {
  // const value = await pool.query(
  //   "SELECT * FROM public.mst_bussines_param where valueint = $1 and category = $2 ",
  //   [action, "LOG_EVENT"]
  // );

  const value = await pool
    .where({ valueint: action, category: "LOG_EVENT" })
    .select("*")
    .from("mst_bussines_param");

  const eventType = type == 1 ? " Success" : type == 2 ? " Failed" : " Unknown";
  // const insertLog = await pool.query(
  //   "INSERT INTO public.trx_eventlog (created_by, value,idapp,idmenu,description,cdaction,refevent) VALUES ( $1,$2,$3,$4,$5,$6,$7) returning *",
  //   [
  //     user,
  //     value.rows[0].label
  //       ? value.rows[0].label + eventType
  //       : "Unknown_Event" + eventType,
  //     appId,
  //     0,
  //     description,
  //     action,
  //     tokenID,
  //   ]
  // );

  const insertLog = await pool("trx_eventlog")
    .returning([
      "id",
      "created_by",
      "valuejson",
      "value",
      "idapp",
      "idmenu",
      "description",
      "cdaction",
      "refevent",
      "created_at",
      "updated_at",
    ])
    .insert({
      created_by: user,
      value: value[0].label
        ? value.label + eventType
        : "Unknown_Event" + eventType,
      idapp: appId,
      idmenu: 0,
      description: description,
      cdaction: action,
      refevent: tokenID,
    });

  console.log(insertLog);
  return 1;
};

module.exports = {
  insertLog,
};
