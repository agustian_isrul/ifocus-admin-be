const bcrypt = require("bcrypt");
const saltRounds = 10;
let encrtpyted;
const encryptPass = async (pass) => {
  encrtpyted = await bcrypt.hash(pass, 10);

  return encrtpyted;
};

const comparePassword = async (pass, enctype) => {
  let res = await bcrypt.compare(pass, enctype);

  return res;
};

module.exports = {
  encryptPass,
  comparePassword,
};
