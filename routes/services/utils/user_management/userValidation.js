const pool = require("../../../../connection/db");
const passwordValidator = require("password-validator");

const sysParam = require("./getSysParam");
let umParam;

const getParam = async () => {
  umParam = await sysParam.getUMParam();
};

const validateUserId = async (userId) => {
  await getParam();
  if (umParam.USERID_IS_NUMBER) {
    if (isNaN(userId)) {
      return false;
    } else {
      return true;
    }
  }
  return true;
};

const validatePassword = async (password, oldpassword) => {
  await getParam();
  let prop = {
    minLength: umParam.PASSWORD_MIN_LENGTH,
    maxLength: umParam.PASSWORD_MAX_LENGTH,
    upperCase: umParam.PASSWORD_UPPERCASE,
    lowerCase: umParam.PASSWORD_LOWERCASE,
    character: umParam.PASSWORD_CHARACTER,
    lastPassword: umParam.PASSWORD_HISTORY,
  };
  try {
    var passwordSchema = new passwordValidator();
    passwordSchema.has().not().spaces();
    if (prop.minLength) {
      passwordSchema.is().min(prop.minLength);
    }
    if (prop.maxLength) {
      passwordSchema.is().max(prop.maxLength);
    }
    if (prop.upperCase == 1) {
      passwordSchema.has().uppercase();
    }
    if (prop.lowerCase == 1) {
      passwordSchema.has().lowercase();
    }
    if (prop.character) {
      passwordSchema.has(new RegExp(prop.character));
    }
    // if (prop.lastPassword) {
    //   if (oldpassword) passwordSchema.is().not().oneOf(oldpassword);
    // }
    console.log(passwordSchema.validate(password));
    return passwordSchema.validate(password);
  } catch (err) {
    console.log(err);
    return;
  }
};

module.exports = {
  validatePassword,
  validateUserId,
};
