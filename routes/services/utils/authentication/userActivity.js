const pool = require("../../../../connection/db");
const sysParam = require("../user_management/getSysParam");

let loginParam, sessionParam;
const getParam = async () => {
  loginParam = await sysParam.getUMParam();
  //sessionParam = await sysParam.getSessionParam();
};

const getUserById = async (userId) => {
  let userData = await pool
    .where("userid", userId)
    .select("*")
    .from("cdid_tenant_user");
  console.log("userActivity.js line 15 :",userId);
  return userData;
};

const isChangePasswordTime = async (userId) => {
  await getParam();
  const userData = await getUserById(userId);
  if (userData.length > 0) {
    let lastChangePasswordData =
      userData[0].last_change_password || userData[0].created_at;
    const lastChangePasswordUser = new Date(lastChangePasswordData);
    const today = new Date();
    let diffDays = parseInt(
      (today - lastChangePasswordUser) / (1000 * 60 * 60 * 24)
    );
    if (diffDays >= loginParam.CHANGE_PASSWORD_RANGE) {
      return true;
    }
    return false;
  }
  return false;
};

const isInActiveUserTime = async (userId) => {
  await getParam();
  const userData = await getUserById(userId);
  if (userData.length > 0) {
    let lastChangePasswordData =
      userData[0].last_change_password || userData[0].created_date;
    const lastLoginUser = new Date(lastChangePasswordData);
    const today = new Date();
    let diffDays = parseInt((today - lastLoginUser) / (1000 * 60 * 60 * 24));
    if (diffDays >= loginParam.INACTIVE_USER_RANGE) {
      await pool("cdid_tenant_user")
        .update({ active: 0 })
        .where("userid", userId);
      return true;
    }
    return false;
  }
  return false;
};

module.exports = {
  isChangePasswordTime,
  isInActiveUserTime,
};
