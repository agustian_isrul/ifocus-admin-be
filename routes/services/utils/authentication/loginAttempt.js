const pool = require("../../../../connection/db");
const sysParam = require("../user_management/getSysParam");

let loginParam, sessionParam;
const getParam = async () => {
  loginParam = await sysParam.getUMParam();
  //sessionParam = await sysParam.getSessionParam();
};

const getUserById = async (userId) => {
  let userData = await pool
    .where("userid", userId)
    .select("*")
    .from("cdid_tenant_user");
  console.log("User id ",userId);
  return userData;
};

const checktAttempt = async (userId) => {
  let getCurrentAttempt = await getUserById(userId);
  if (getCurrentAttempt.length > 0) {
    return getCurrentAttempt[0].total_attempt;
  } else return 0;
};

const updateAttempt = async (userId, attempt) => {
  try {
    console.log("updating attemp", userId, attempt);
    let getCurrentAttempt = await getUserById(userId);

    if (getCurrentAttempt.length > 0) {
      await pool("cdid_tenant_user")
        .returning(["*"])
        .where("userid", userId)
        .update({
          total_attempt:
            attempt === undefined
              ? getCurrentAttempt[0].total_attempt + 1
              : attempt,
        });
    } else {
      return;
    }
  } catch (err) {
    console.log(err);
    return;
  }
};

const updateLockTime = async (userId) => {
  // await pool.query(
  //   "Update  public.cdid_tenant_user set locked_at = to_timestamp($1 / 1000.0) where userid = $2",
  //   [Date.now(), userID]
  // );
  var now = new Date();
  now.setHours(now.getHours() + 7);
  await pool("cdid_tenant_user")
    .returning(["*"])
    .where("userid", userId)
    .update({
      locked_at: pool.fn.now(),
      active: 0,
    });
};

const isUnlockTime = async (userId) => {
  await getParam();
  let userData = await getUserById(userId);
  let lockedTime = userData[0].locked_at;
  var today = new Date();
  var lockedDate = new Date(lockedTime);
  var diffMs = lockedDate - today;
  var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes

  return diffMins >= loginParam.LOCK_DURATION ? true : false;
};

const updateLoginTime = async (userId) => {
  // await pool.query(
  //   "Update  public.cdid_tenant_user set last_login = to_timestamp($1 / 1000.0) where userid = $2",
  //   [Date.now(), userId]
  // );
  var now = new Date();
  now.setHours(now.getHours() + 7);
  console.log(userId);
  console.log(now);
  await pool("cdid_tenant_user").where("userid", userId).update({
    last_login: pool.fn.now(),
  });
};

module.exports = {
  updateAttempt,
  checktAttempt,
  updateLockTime,
  isUnlockTime,
  updateLoginTime,
};
