const uuid = require("uuid");
const crypto = require("crypto");

const getRandomId = async () => {
  let randomId = await uuid.v4();
  return randomId;
};

const getRandomWithCrypto = async (byteChar) => {
  // console.log("************************************ "+byteChar);
  const id = await crypto.randomBytes(byteChar).toString("hex");
  return id;
};

// getRandomId();
// getRandomWithCrypto();

module.exports = {
  getRandomId,
  getRandomWithCrypto
};