const pool = require("../../../connection/db");

const insertGroup = async (dataobject, apps, idTenant, grouptype) => {
  console.log("INi idTenant", idTenant);
  const { group, modules, menus } = dataobject;
  try {
    var now = new Date();
    now.setHours(now.getHours() + 7);
    const groupResult = await pool("mst_usergroup")
      .returning(["id", "groupname"])
      .insert({
        groupname: group.groupName,
        idtenant: idTenant,
        //created_byid: apps.id,
        //created_at: now.toUTCString(),
        active: group.active,
        idapplication: apps.idapp,
        grouptype: group.groupType,
      });

    if (groupResult.length > 0 && modules.length > 0) {
      let arrData = [];
      await modules.map(async (module) => {
        let obj = {
          // idgroup: groupResult[0].id,
          idgroup: groupResult[0],
          idtenant: idTenant,
          idmodule: module.id,
          fview: module.view,
          //created_byid: apps.id,
          //created_at: now.toUTCString(),
        };
        arrData.push(obj);
      });

      const modulesResult = await pool
        .insert(arrData)
        .returning(["id", "idgroup"])
        .into("mst_usergroupacl");

      if (modulesResult.length > 0 && menus.length > 0) {
        let menuArr = [];
        var now = new Date();
        now.setHours(now.getHours() + 7);
        await menus.map(async (menu) => {
          let obj = {
            // idgroup: groupResult[0].id,
            idgroup: groupResult[0],
            idtenant: idTenant ? idTenant : 0,
            idmodule: menu.moduleId ? menu.moduleId : 0,
            idmenu: menu.menuId ? menu.menuId : 0,
            fcreate: menu.acl.create ? menu.acl.create : 0,
            fread: menu.acl.read ? menu.acl.read : 0,
            fupdate: menu.acl.update ? menu.acl.update : 0,
            fdelete: menu.acl.delete ? menu.acl.delete : 0,
            fview: menu.acl.view ? menu.acl.view : 0,
            fapproval: menu.acl.approval ? menu.acl.approval : 0,
            //created_byid: apps.id,
            //  created_at: now.toUTCString(),
          };
          menuArr.push(obj);
        });
        // console.log("cara SQL Ryan");
        // console.log(
        //   pool
        //     .insert(menuArr)
        //     .returning(["id", "idtenant"])
        //     .into("mst_usergroupacl")
        //     .toSQL().sql
        // );
        const menusResult = await pool
          .insert(menuArr)
          .returning(["id", "idtenant"])
          .into("mst_usergroupacl");
        if (menusResult.length > 0) {
          return 1;
        } else {
          return 0;
        }
      } else {
        return 0;
      }
    } else {
      return 0;
    }
  } catch (err) {
    console.log(err);
  }
};

const deleteGroup = async (dataobject, apps) => {
  const { group, modules, menus } = dataobject;
  try {
    // let groupResult = await pool.query(
    //   "Delete from public.mst_usergroup where id =  $1",
    //   [group.id]
    // );

    await pool("mst_usergroup").where("id", group.id).del();
    // let moduleResult = await pool.query(
    //   "Delete from public.mst_usergroupacl where idgroup =  $1",
    //   [group.id]
    // );
    await pool("mst_usergroupacl").where("idgroup", group.id).del();
    return 1;
  } catch (err) {
    console.log(err);
    return 0;
  }
};

const editGroup = async (group, apps, idTenant) => {
  try {
    // let groupResult = await pool.query(
    //   "UPDATE public.mst_usergroup set groupname = $1,updated_at = to_timestamp($2 / 1000.0) where id =  $3 returning *",
    //   [group.groupName, Date.now(), group.groupId]
    // );
    var now = new Date();
    now.setHours(now.getHours() + 7);
    const groupResult = await pool("mst_usergroup")
      .returning(["id", "groupname"])
      .where("id", group.groupId)
      .update({
        groupname: group.groupName,
        active: 1
        //updated_at: now.toUTCString(),
      });
      // return groupResult[0].id;
    return groupResult[0];
  } catch (err) {
    console.log(err);

    return 0;
  }
};
const editGroupActive = async (id, isactive) => {
  try {
    // let groupResult = await pool.query(
    //   "UPDATE public.mst_usergroup set groupname = $1,updated_at = to_timestamp($2 / 1000.0) where id =  $3 returning *",
    //   [group.groupName, Date.now(), group.groupId]
    // );
    var now = new Date();
    now.setHours(now.getHours() + 7);
    const groupResult = await pool("mst_usergroup")
      .returning(["id", "groupname"])
      .where("id", id)
      .update({
        active: isactive,
        //updated_at: now.toUTCString(),
      });
    return groupResult[0].id;
  } catch (err) {
    console.log(err);

    return 0;
  }
};



const editModules = async (modules, groupId, apps, idTenant, grouptype) => {
  try {
    // await pool.query(
    //   "Delete FROM public.mst_usergroupacl where  idgroup= $1 and idmodule is not null",
    //   [groupId]
    // );
    console.log("######################### GROUP ID DELET  "+groupId);
    // console.log(modules);
    await pool("mst_usergroupacl")
      .where("idgroup", groupId)
      .whereNotNull("idmodule")
      .del();

    let arrData = [];
    var now = new Date();
    now.setHours(now.getHours() + 7);

    await modules.map(async (module) => {
      let obj = {
        idgroup: groupId ? groupId : 0,
        idtenant: idTenant ? idTenant : 0,
        idmodule: module.id ? module.id : 0,
        fview: module.view ? module.view : 0,
        // created_byid: apps.id,
        //created_at: now.toUTCString(),
      };
      arrData.push(obj);
    });

    if (arrData.length > 0) {
      await pool.insert(arrData).into("mst_usergroupacl");
      console.log(arrData);
      console.log(pool.insert(arrData).into("mst_usergroupacl").toSQL().sql);
      return 1;
    } else {
      return 0;
    }
    // await modules.map(async (module) => {
    //   moduleData = await pool.query(
    //     "INSERT INTO public.mst_usergroupacl ( idgroup, idtenant, idmodule, fview, created_byid, created_at) VALUES ( $1,$2,$3,$4,$5,to_timestamp($6 / 1000.0)) RETURNING *",
    //     [
    //       groupId,
    //       idTenant,
    //       // module.moduleId,
    //       module.id,
    //       // module.acl.view,
    //       module.view,
    //       apps.id,
    //       Date.now(),
    //     ]
    //   );
    // });
  } catch (err) {
    console.log(err);

    return 0;
  }
};

const editMenus = async (menus, groupId, apps, idTenant, grouptype) => {
  try {
    // let moduleResult = await pool.query(
    //   "Delete  FROM public.mst_usergroupacl where idgroup =$1 and idmenu is not null",
    //   [groupId]
    // );
    await pool("mst_usergroupacl")
      .where("idgroup", groupId)
      .whereNotNull("idmenu")
      .del();
    let arrData = [];
    var now = new Date();
    now.setHours(now.getHours() + 7);
    await menus.map(async (menu) => {
      let obj = {
        idgroup: groupId ? groupId : 0,
        idtenant: idTenant ? idTenant : 0,
        idmodule: menu.moduleId ? menu.moduleId : 0,
        idmenu: menu.menuId ? menu.menuId : 0,
        fcreate: menu.acl.create ? menu.acl.create : 0,
        fread: menu.acl.read ? menu.acl.read : 0,
        fupdate: menu.acl.update ? menu.acl.update : 0,
        fdelete: menu.acl.delete ? menu.acl.delete : 0,
        fview: menu.acl.view ? menu.acl.view : 0,
        fapproval: menu.acl.approval ? menu.acl.approval : 0,
        //created_byid: apps.id,
        // created_at: now.toUTCString(),
      };
      arrData.push(obj);
    });
    if (arrData.length > 0) {
      await pool.insert(arrData).into("mst_usergroupacl");
      return 1;
      // await menus.map(async (menu) => {
      //   await pool.query(
      //     "INSERT INTO public.mst_usergroupacl ( idgroup, idtenant, idmodule, idmenu,  fcreate, fread, fupdate, fdelete, fview, created_byid, created_at) VALUES ( $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,to_timestamp($11 / 1000.0)) RETURNING *",
      //     [
      //       groupId,
      //       idTenant,
      //       menu.moduleId,
      //       menu.menuId,
      //       menu.acl.create,
      //       menu.acl.read,
      //       menu.acl.update,
      //       menu.acl.delete,
      //       menu.acl.view,
      //       apps.id,
      //       Date.now(),
      //     ]
      //   );
      // });
    } else {
      return0;
    }
  } catch (err) {
    console.log(err);

    return 0;
  }
};

module.exports = {
  insertGroup,
  deleteGroup,
  editGroup,
  editGroupActive,
  editModules,
  editMenus,
};
