const express = require("express"),
  app = express();
const router = express.Router();

// const Customers = require("../../models/customers")
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const encPass = require("../../routes/services/utils/uuidGenerator");
const logger = require("../../routes/services/utils/logger");
const moment = require("moment");
const checkAuth = require("../../middleware/check-auth");
const loginUtil = require("./utils/authentication/loginAttempt");
const userActivity = require("./utils/authentication/userActivity");
const passwordEnc = require("./utils/password/encryptionPass");
const sysParam = require("./utils/user_management/getSysParam");
const pool = require("../../connection/db");
const logs = require("./utils/logger");
const fileLog = logs.logging();
const axios = require("axios");
var conf = require("../../config.json");
let loginParam, sessionParam;

const getParam = async () => {
  loginParam = await sysParam.getUMParam();
};
router.use(checkAuth);

router.post("/signviakkt", async (req, res, next) => {
  const fileLog = logger.logging("Krakatoa");
  try {
    await getParam();
    var idtenant = "0";
    console.log("INI REQUEST");
    const { credential, secret } = req.body;
    let objPayload = { userid: credential };
    fileLog.info(
      `Login attemp - App: KRAKATOA , payload: ${JSON.stringify(objPayload)}`
    );
    const resuserexist = await pool
      .select(
        "tu.id",
        "tu.fullname",
        "tu.userid",
        "tu.idtenant",
        "tu.idsubtenant",
        "tu.leveltenant",
        "tn.tnname",
        "tn.tnstatus",
        "bio.bioemailactive",
        "bio.biophoneactive",
        "bio.bioaddress",
        "bio.bionik",
        "bio.bionpwp",
        "bio.bioidtipenik",
        "bio.bioidcorel"
      )
      .from("cdid_tenant_user as tu")
      .innerJoin("cdid_tenant as tn", "tu.idtenant", "tn.id")
      .innerJoin(
        pool
          .select()
          .from("mst_biodata_corell")
          .where({ bioidcorel: 3 })
          .as("bio"),
        "tu.id",
        "bio.biocorelobjid"
      )
      .where("userid", credential);

    console.log("INI LOGIN USER " + resuserexist.length);
    if (resuserexist.length > 0) {
      console.log("INI REQUEST CEK USER DAN BIO CORELL");
      var jsonbody = resuserexist[0];
      let appObject = [];
      let countapp = 0;
      idtenant = jsonbody.idtenant;
      let orgdefault = { orgid: 0 };
      // res.status(200).json({ status: 200, data: jsonbody });

      if (jsonbody.leveltenant > 0) {
        idtenant =
          jsonbody.leveltenant > 1 ? jsonbody.tnparentid : jsonbody.idtenant;
      }
      jsonbody = { ...jsonbody, ...orgdefault };
      var application = {};
      var appsModuleObj = {};

      if (jsonbody.leveltenant < 1) {
        // const liscenseapp = await pool.query(
        //   "SELECT clis.id_application id, TO_CHAR(clis.expiredate,'yyyy-MM-dd') expiredate, clis.id_application, clis.paidstatus, clis.active, clis.defaultactive, mapp.appname, mapp.applabel,mapp.description,mapp.routelink FROM public.cdid_liscenses clis INNER JOIN public.mst_application mapp ON clis.id_application = mapp.id where clis.cdid_tenant=$1 and clis.active=1 order by clis.defaultactive desc;",
        //   [idtenant]
        // );

        const liscenseapp = await pool
          .select(
            "clis.id_application as id",
            "clis.expiredate",
            "clis.id_application",
            "clis.paidstatus",
            "clis.active",
            "clis.defaultactive",
            "mapp.appname",
            "mapp.applabel",
            "mapp.description",
            "mapp.routelink"
          )
          .from("cdid_liscenses as clis")
          .innerJoin(
            "mst_application as mapp",
            "clis.id_application",
            "mapp.id"
          )
          .where({ "clis.cdid_tenant": idtenant, "clis.active": 1 });
        if (liscenseapp.length >= 0) {
          countapp = liscenseapp.length;
          // appObject = liscenseapp.rows;
          // if (jsonbody.leveltenant > 1) {
          appObject = liscenseapp;
          // } else {
          //   var itemApp = liscenseapp.rows[0];
          //   appObject.push(itemApp);
          // }
          var menusModuleObj = [];
          var appsModuleList = [];
          appsModuleObj = { sidemenus: appsModuleList };
          application = { appscount: countapp, apps: appObject };
        }
      } else {
        // res.status(200).json({ status: 200, data: jsonbody });
        console.log("INI REQUEST CEK APLIKASI");
        // CHeck Organization of Users.
        //         SELECT ou.id, ou.userid, ou.orgid, ou.grpid, ou.created_at, oa.idapp, ma.appname,ma.applabel,ma.apptype,ma.routelink FROM public.cdid_orguserassign ou INNER JOIN cdid_orgapplications oa ON
        // ou.orgid = oa.idorg INNER JOIN mst_application ma ON oa.idapp=ma.id WHERE ou.userid =4
        // const liscenseapp = await pool.query(
        //   "SELECT ma.id_application id, ma.expiredate, ma.id_application, ma.paidstatus, ma.active, ma.defaultactive, ma.appname,ma.applabel,ma.routelink FROM public.cdid_orguserassign ou INNER JOIN cdid_orgapplications oa ON ou.orgid = oa.idorg INNER JOIN (SELECT clis.id_application id, TO_CHAR(clis.expiredate,'yyyy-MM-dd') expiredate, clis.id_application, clis.paidstatus, clis.active, clis.defaultactive, mapp.appname, mapp.applabel,mapp.description,mapp.routelink FROM public.cdid_liscenses clis INNER JOIN public.mst_application mapp ON clis.id_application = mapp.id where clis.cdid_tenant=$1 and clis.active=1 order by clis.defaultactive desc) ma ON oa.idapp=ma.id WHERE ou.userid =$2",
        //   [idtenant, jsonbody.id]
        // );

        const liscenseapp = await pool
          .select(
            "ma.id_application as id",
            "ma.expiredate",
            "ma.id_application",
            "ma.paidstatus",
            "ma.active",
            "ma.defaultactive",
            "ma.appname",
            "ma.applabel",
            "ma.routelink"
          )
          .from("cdid_orguserassign as ou")
          .innerJoin("cdid_orgapplications as oa", "ou.orgid", "oa.idorg")
          .innerJoin(
            pool
              .select(
                "clis.id_application as id",
                "clis.expiredate as expiredate",
                "clis.id_application",
                "clis.paidstatus",
                "clis.active",
                "clis.defaultactive",
                "mapp.appname",
                "mapp.applabel",
                "mapp.description",
                "mapp.routelink"
              )
              .from("cdid_liscenses  as clis")
              .innerJoin(
                "mst_application as mapp",
                "clis.id_application",
                "mapp.id"
              )
              .where({ "clis.cdid_tenant": idtenant, "clis.active": 1 })
              .orderBy("clis.defaultactive ", "desc")
              .as("ma"),
            "oa.idapp",
            "ma.id"
          )
          .where({ "ou.userid": jsonbody.id });
        if (liscenseapp) {
          countapp = liscenseapp.length;
          appObject = liscenseapp;
          var menusModuleObj = [];
          var appsModuleList = [];
          appsModuleObj = { sidemenus: appsModuleList };
          application = { appscount: countapp, apps: appObject };
        }
      }
      jsonbody = { ...jsonbody, ...application, ...appsModuleObj };
      // res.status(200).json({ status: 200, data: jsonbody });
      //#################### CHECK Session if exist ##########################
      console.log("INI REQUEST MASALAH SESSION");
      // console.log(jsonbody.id);
      let sessionid = "";
      let creator = jsonbody.id;

      // const checkTodayLog = await pool.query(
      //   "SELECT * FROM public.trx_sessionlog where created_by=$1 and status=1 order by created_date desc limit 1",
      //   [jsonbody.id]
      // );
      const checkTodayLog = await pool
        .select()
        .from("trx_sessionlog")
        .where({ created_by: creator, status: 1 })
        .orderBy("created_at", "desc");

      // console.log(checkTodayLog);
      if (checkTodayLog.length > 0) {
        sessionid = checkTodayLog[0].id;
        var token = jwttools.encryptdata(sessionid);
        // setTimeout(function () {
        res.status(200).json({ status: 200, data: token });
        // }, 200);
      } else {
        console.log("INI NGGA ADA LOG YANG MASUK");
        //const today = moment().format("YYYY-MM-DD HH:mm:ss");
        const now = new Date();
        const expire = new Date(
          now.getTime() + loginParam.SESSION_IDLE * 60000
        );
        const insertTodayLog = await pool
          .insert({
            created_by: jsonbody.id,
            userinfo: JSON.stringify(jsonbody),
            expire_date: expire,
            status: 1,
          })
          .into("trx_sessionlog")
          .returning(["id", "status"]);
        // const insertTodayLog = await pool.query(
        //   "INSERT INTO public.trx_sessionlog (created_by, userinfo, expire_date, status) VALUES ($1, $2, $3, $4) RETURNING *",
        //   [jsonbody.id, jsonbody, expire, 1]
        // );
        // console.log(insertTodayLog);
        if (insertTodayLog.length > 0) {
          // console.log("Masuk Kampreeeet");
          // res.status(200).json({ status: 200, data: jsonbody });
          sessionid = insertTodayLog[0].id;
          // sessionid = insertTodayLog[0];
          var token = jwttools.encryptdata(sessionid);
          console.log(token);
          fileLog.info(`Login response - App: KRAKATOA , status: Success`);
          setTimeout(function () {
            res.status(200).json({ status: 200, data: token });
          }, 200);
        } else {
          res.status(500).json({ status: 500, data: "No User found" });
        }
      }
    } else {
      fileLog.error(`Login response - App: KRAKATOA , status: No User found`);
      res.status(500).json({ status: 500, data: "No User found" });
    }
    // res.status(200).json({ status: 200, data: resuserexist });
  } catch (err) {
    console.log(err);
    fileLog.error(`Login response - App: KRAKATOA , ${err}`);
    res.status(500).json({ status: 500, data: "No User found" });
  }
});
router.post("/signviaadmin", async (req, res, next) => {
  try {
    await getParam();
    var jsonbody = {};
    var idtenant = "0";
    var ps;
    var appid;
    let modulename = "";
    let modulenameEnd = "";
    fileLog.info(`Login Request `);
    console.log("INI REQUEST signviaadmin");
    const { credential, secret, appname } = req.body;
    const fileLogadmin = logger.logging(appname);
    let totalAttempt = await loginUtil.checktAttempt(credential);

    if (totalAttempt > loginParam.LOGIN_ATTEMPT) {
      await loginUtil.updateLockTime(credential);
      //let isUnlockTime = await loginUtil.isUnlockTime(credential);
      //if (isUnlockTime == false) {
      res.status(200).json({
        status: 403,
        data: `Login Attempt is more than ${loginParam.LOGIN_ATTEMPT} times. User has been locked, please contact your admin`,
      });
      return;
      //}
    }
    let objPayload = { userid: credential };

    const resuserexist = await pool
      .select(
        "tu.id",
        "tu.fullname",
        "tu.userid",
        "tu.idtenant",
        "tu.idsubtenant",
        "tu.leveltenant",
        "tu.last_login",
        "tu.pwd",
        "tu.active",
        "tn.tnname",
        "tn.tnstatus",
        "bio.bioemailactive",
        "bio.biophoneactive",
        "bio.bioaddress",
        "bio.bionik",
        "bio.bionpwp",
        "bio.bioidtipenik",
        "bio.bioidcorel"
      )
      .from("cdid_tenant_user as tu")
      .innerJoin("cdid_tenant as tn", "tu.idtenant", "tn.id")
      .innerJoin(
        pool
          .select()
          .from("mst_biodata_corell")
          .where({ bioidcorel: 3 })
          .as("bio"),
        "tu.id",
        "bio.biocorelobjid"
      )
      .where("tu.userid", credential).whereIn("tu.active", [1, 5, 9]);
      // .where({ userid: credential, "tu.active": 1 });
      // 1. Aktive
      // 2. Disabled/Notactived
      // 3. Aktive namun belum Approve
      // 5. Edit To Deactived belum Approve
      // 7. Edit To Actived belum Approve
      // 9. Delete belum di Approve
    // fileLogadmin.info(`Login attemp - App: ${appname} , payload: ${JSON.stringify(objPayload)}`);
    console.log("USER EXIST signviaadmin ",resuserexist);





    if (resuserexist.length > 0) {
      let validPassword = await passwordEnc.comparePassword(
        secret,
        resuserexist[0].pwd
      );
      // console.log(validPassword);
      if (!validPassword) {
        await loginUtil.updateAttempt(credential);
        res.status(500).json({ status: 500, data: "No User found" });
        return;
      }
      if (resuserexist[0].tnstatus !== 1) {
        res.status(500).json({ status: 500, data: "No User found" });
        return;
      }
      let isInActiveUserTime = await userActivity.isInActiveUserTime(
        credential
      );
      if (isInActiveUserTime) {
        res.status(500).json({ status: 500, data: "No User found" });
        return;
      }

      let isChangePasswordTime = await userActivity.isChangePasswordTime(
        credential
      );
      let notif;
      if (isChangePasswordTime) {
        notif = {
          changePassword: loginParam.CHANGE_PASSWORD_MESSAGE,
        };
      }

      jsonbody = resuserexist[0];
      jsonbody = { ...jsonbody, notif };
      let appObject = [];
      let countapp = 0;
      idtenant = jsonbody.idtenant;
      let orgdefault = { orgid: 0 };
      if (jsonbody.leveltenant > 0) {
        idtenant =
          jsonbody.leveltenant > 1 ? jsonbody.tnparentid : jsonbody.idtenant;
      }
      jsonbody = { ...jsonbody }; //, ...orgdefault };
      var application = {};
      var appsModuleObj = {};
      // console.log(jsonbody.leveltenant);
      console.log("LEVEL TENANT signviaadmin",jsonbody.leveltenant);
      if (jsonbody.leveltenant < 1) {
        const liscenseapp = await pool
          .select(
            "clis.id_application as id",
            "clis.expiredate",
            "clis.id_application",
            "clis.paidstatus",
            "clis.active",
            "clis.defaultactive",
            "mapp.appname",
            "mapp.applabel",
            "mapp.description",
            "mapp.routelink",
            "mapp.withapproval"
          )
          .from("cdid_liscenses as clis")
          .innerJoin(
            "mst_application as mapp",
            "clis.id_application",
            "mapp.id"
          )
          .where({ "clis.cdid_tenant": idtenant, "clis.active": 1 })
          .orderBy("clis.defaultactive", "desc");
          console.log("LISCENSE APP signviaadmin ",);
        if (liscenseapp) {
          countapp = liscenseapp.length;
          // appObject = liscenseapp.rows;
          // if (jsonbody.leveltenant > 1) {
          appObject = liscenseapp;
          // } else {
          //   var itemApp = liscenseapp.rows[0];
          //   appObject.push(itemApp);
          // }
          var menusModuleObj = [];
          var appsModuleList = [];
          appsModuleObj = { sidemenus: appsModuleList };
          application = { appscount: countapp, apps: appObject };
        }
      } else {
        const liscenseapp = await pool
          .select(
            "ma.id_application as id",
            "ma.expiredate",
            "ma.id_application",
            "ma.paidstatus",
            "ma.active",
            "ma.defaultactive",
            "ma.appname",
            "ma.applabel",
            "ma.routelink",
            "oa.idorg",
            "ma.withapproval"
          )
          .from("cdid_orguserassign as ou")
          .innerJoin("cdid_orgapplications as oa", "ou.orgid", "oa.idorg")
          .innerJoin(
            pool
              .select(
                "clis.id_application as id",
                "clis.expiredate as expiredate",
                "clis.id_application",
                "clis.paidstatus",
                "clis.active",
                "clis.defaultactive",
                "mapp.appname",
                "mapp.applabel",
                "mapp.description",
                "mapp.routelink",
                "mapp.withapproval"
              )
              .from("cdid_liscenses  as clis")
              .innerJoin(
                "mst_application as mapp",
                "clis.id_application",
                "mapp.id"
              )
              .where({
                "clis.cdid_tenant": idtenant,
                "clis.active": 1,
                "mapp.appname": appname,
              })
              .orderBy("clis.defaultactive ", "desc")
              .as("ma"),
            "oa.idapp",
            "ma.id"
          )
          .where({ "ou.userid": jsonbody.id });

        // console.log(req.body);  
        console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        // console.log(pool
        //   .select(
        //     "ma.id_application as id",
        //     "ma.expiredate",
        //     "ma.id_application",
        //     "ma.paidstatus",
        //     "ma.active",
        //     "ma.defaultactive",
        //     "ma.appname",
        //     "ma.applabel",
        //     "ma.routelink",
        //     "oa.idorg",
        //     "ma.withapproval"
        //   )
        //   .from("cdid_orguserassign as ou")
        //   .innerJoin("cdid_orgapplications as oa", "ou.orgid", "oa.idorg")
        //   .innerJoin(
        //     pool
        //       .select(
        //         "clis.id_application as id",
        //         "clis.expiredate as expiredate",
        //         "clis.id_application",
        //         "clis.paidstatus",
        //         "clis.active",
        //         "clis.defaultactive",
        //         "mapp.appname",
        //         "mapp.applabel",
        //         "mapp.description",
        //         "mapp.routelink",
        //         "mapp.withapproval"
        //       )
        //       .from("cdid_liscenses  as clis")
        //       .innerJoin(
        //         "mst_application as mapp",
        //         "clis.id_application",
        //         "mapp.id"
        //       )
        //       .where({
        //         "clis.cdid_tenant": idtenant,
        //         "clis.active": 1,
        //         "mapp.appname": appname,
        //       })
        //       .orderBy("clis.defaultactive ", "desc")
        //       .as("ma"),
        //     "oa.idapp",
        //     "ma.id"
        //   )
        //   .where({ "ou.userid": jsonbody.id }).toSQL().sql);
        // console.log("Data Liscnse ", liscenseapp);
        console.log("LISCENSE APP signviaadmin ", liscenseapp);
        if (liscenseapp) {
          // console.log(liscenseapp);
          countapp = liscenseapp.length;
          appObject = liscenseapp;
          console.log(appObject);
          var menusModuleObj = [];
          var appsModuleList = [];
          appid =
            appObject[0].appname === appname ? appObject[0].id_application : 0;
          // appsModuleObj = { sidemenus: appsModuleList };
          //************ Looking Side menu of the user */
          application = { appscount: countapp, apps: appObject };
          let checkUser = await pool
            .where("iduser", jsonbody.id)
            .select("*")
            .from("cdid_tenant_user_groupacl");
          let userAssignData = checkUser.length;
          console.log("line 165 " + userAssignData);
          let getmoduleadminapps;
          if (userAssignData > 0) {
            console.log(">>>>> QUERY MENU YANG ADA GROUP");
            getmoduleadminapps = await pool
              .select(
                "mug.idmenu",
                "tug.iduser",
                "tug.idgroupuseracl",
                "mmdl.id as idmdl",
                "mmdl.idapplication",
                "mmdl.modulename",
                "mmnu.name",
                "mmnu.title",
                "mmnu.routelink",
                "mmnu.routepath",
                "mug.fcreate",
                "mug.fread",
                "mug.fupdate",
                "mug.fdelete",
                "mug.fview",
                "mug.fapproval"
              )
              .from("cdid_tenant_user_groupacl as tug")
              .innerJoin(
                "mst_usergroupacl as mug",
                "tug.idgroupuseracl",
                "mug.idgroup"
              )
              .innerJoin("mst_module as mmdl", "mug.idmodule ", "mmdl.id ")
              .innerJoin("mst_menu as mmnu", "mug.idmenu", "mmnu.id")
              .where({ "tug.iduser": jsonbody.id })
              .whereNot("mug.fread", 0)
              .orderBy([{ column: 'mmdl.id' }, { column: 'mug.idmenu', order: 'asc' }])




              // .orderBy([{ column: 'mmdl.idapplication' }, { column: 'mug.idmenu', order: 'asc' }])
              // .orderBy("mmdl.idapplication", "mug.idmenu");
              // .orderBy("mug.idmenu", "mmdl.idapplication");
            //.distinctOn("mug.idmenu");
          } else {
            console.log(">>>>> QUERY MENU YANG TIDAK GROUP");
            getmoduleadminapps = await pool
              .select(
                "mmd.id",
                "mbt.*",
                "mmd.status",
                "mmd.idapplication",
                "mmd.modulecode",
                "mmd.moduletype",
                "mmd.iconclass",
                "mnu.name",
                "mnu.title",
                "mnu.routelink "
              )
              .from("mst_module as mmd")
              .innerJoin(
                pool
                  .select("modulename")
                  .from("mst_moduleby_tenant  as mmdt")
                  .where({ idapplication: appid, idtenant: idtenant })
                  .distinct()
                  .as("mbt"),
                "mbt.modulename",
                "mmd.modulename"
              )
              .innerJoin("mst_menu as mnu", " mmd.id", "mnu.idmodule")
              .orderBy([{ column: 'mmd.id' }, { column: 'mnu.id', order: 'asc' }])
              // .orderBy("mmd.id", "mnu.id");
          }
         
          console.log(">>> JUMLAH Module APP " + JSON.stringify(getmoduleadminapps));
          if (getmoduleadminapps.length > 0) {
            let modelesTmp = getmoduleadminapps;
            let tmpobj = {};
            let firstTime = 0;
            modelesTmp.forEach((element) => {
              // tmpobj = element;
              // console.log(object);
              if (element.modulename != modulename) {
                // tmpobj = element;
                if (modulename != "") {
                  let modulenmenus = {}
                  // tmpobj = element;
                  modulenmenus = {
                    label:element.modulename === modulename? element.modulename: modulename,
                    icon: element.modulename === modulename? element.iconclass: tmpobj.iconclass,
                    expanded: false,
                    items: menusModuleObj,
                  };
                  if(element.modulename != modulename) tmpobj = element;

                  appsModuleList.push(modulenmenus);
                    modulename = element.modulename;
                    modulenameEnd = element.modulename;
                    appsid = element.idapplication;
                    menusModuleObj = [];
                  
                } else {
                  tmpobj = element;
                  modulename = element.modulename;
                  modulenameEnd = element.modulename;
                  appsid = element.idapplication;
                  // console.log(">>>>>>> modulename awal >>>> " + modulename);
                  menusModuleObj = [];
                  
                }
              } 
              // console.log("Insert menu aja");
              menusModuleObj.push({
                label: element.title,
                icon: "pi pi-link",
                routerLink: element.routelink,
                acl: {
                  create: element.fcreate,
                  read: element.fread,
                  update: element.fupdate,
                  delete: element.fdelete,
                  view: element.fview,
                  approval: element.fapproval,
                },
              });
            });

            if (modulename === modulenameEnd) {
              // console.log(
              //   ">>>>>>> Modul akhir " + (modulename === modulenameEnd)
              // );
              let modulenmenus = {
                label: modulename,
                expanded: false,
                icon: tmpobj.iconclass,
                items: menusModuleObj,
              };
              appsModuleList.push(modulenmenus);
              appsModuleObj = { sidemenus: appsModuleList };
            }
            // dcodeInfo = { ...dcodeInfo, ...application, ...appsModuleObj };
            // console.log(application);
            await application.apps.map((app) => {
              app.idapp = app.id;
            });
            // console.log(application);
            jsonbody = { ...jsonbody, ...application, ...appsModuleObj };
            // console.log(">>>>>>> Kirim Balik " + JSON.stringify(jsonbody));
          }
        }
      }
      // jsonbody = { ...jsonbody, ...application, ...appsModuleObj };

      //#################### CHECK Session if exist ##########################
      let sessionid = "";
      await loginUtil.updateAttempt(credential, 0);
      await loginUtil.updateLoginTime(credential);

      const checkTodayLog = await pool
        .where({ created_by: jsonbody.id, status: 1 })
        .select("*")
        .from("trx_sessionlog ")
        .orderBy("created_at");

      if (checkTodayLog.length > 0) {
        sessionid = checkTodayLog[0].id;
        var token = jwttools.encryptdata(sessionid);
        setTimeout(function () {
          // fileLogadmin.info(`Login response - App: ${appname} , status: Success`);
          res
            .status(200)
            .json({ status: 200, data: token, tokenId: sessionid });
        }, 500);
      } else {
        //const today = moment().format("YYYY-MM-DD HH:mm:ss");
        const now = new Date();

        const expire = new Date(
          now.getTime() + loginParam.SESSION_IDLE * 60000
        );

        // console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> EXP " + expire);

        // const insertTodayLog = await pool.query(
        //   "INSERT INTO public.trx_sessionlog(created_by, userinfo, expire_date, status) VALUES ($1, $2, $3, $4) RETURNING *",
        //   [jsonbody.id, jsonbody, expire, 1]
        // );
        const insertTodayLog = await pool("trx_sessionlog")
          // .returning([
          //   "id",
          //   "expire_date",
          //   "created_by",
          //   "status",
          //   "created_at",
          //   "updated_at",
          // ])
          .insert({
            created_by: jsonbody.id,
            userinfo: JSON.stringify(jsonbody),
            expire_date: expire,
            status: 1,
          });
        console.log("Retirning from session log ", insertTodayLog);
        if (insertTodayLog.length > 0) {
          sessionid=insertTodayLog[0];
          // sessionid = insertTodayLog[0].id;
          var token = jwttools.encryptdata(sessionid);
          fileLogadmin.info(
            `Login response - App: ${appname} , status: Success`
          );
          setTimeout(function () {
            res
              .status(200)
              .json({ status: 200, data: token, tokenId: sessionid });
          }, 500);
        } else {
          res.status(500).json({ status: 500, data: "No User found" });
        }
      }
    } else {
      // if resuserexist < 1
      await loginUtil.updateAttempt(credential);
      fileLogadmin.error(
        `Login response - App: ${appname} , status: No User found`
      );
      res.status(500).json({ status: 500, data: "No User found" });
    }
  } catch (err) {
    const { credential, secret, appname } = req.body;
    const fileLogadmin = logger.logging(appname);
    fileLogadmin.error(`Login response - App: ${appname} , ${err}`);
    console.log(err);
    //throw new Error("Generated Error.");
    next(err);
    res.status(500).json({ status: 500, data: "No User found" });
  }
  // let test = {message:"berhasil"};
  // res.status(200).json({ status: 200, data: test });
});
router.post("/signviaadminsc", async (req, res, next) => {
  try {
    await getParam();
    var jsonbody = {};
    var idtenant = "0";
    var ps;
    var appid;
    let modulename = "";
    let modulenameEnd = "";
    fileLog.info(`Login Request `);
    console.log("INI REQUEST signviaadminsc");
    const { credential, secret, appname } = req.body;
    const fileLogadmin = logger.logging(appname);
    let totalAttempt = await loginUtil.checktAttempt(credential);

    if (totalAttempt > loginParam.LOGIN_ATTEMPT) {
      await loginUtil.updateLockTime(credential);
      //let isUnlockTime = await loginUtil.isUnlockTime(credential);
      //if (isUnlockTime == false) {
      res.status(200).json({
        status: 403,
        data: `Login Attempt is more than ${loginParam.LOGIN_ATTEMPT} times. User has been locked, please contact your admin`,
      });
      return;
      //}
    }
    let objPayload = { userid: credential };
    
    const checklevelTenant = await pool
    .select(
      "tu.id",
      "tu.fullname",
      "tu.userid",
      "tu.idtenant",
      "tu.idsubtenant",
      "tu.leveltenant",
      "tu.last_login",
      "tu.pwd",
      "tu.active",
      "bio.bioemailactive",
      "bio.biophoneactive",
      "bio.bioaddress",
      "bio.bionik",
      "bio.bionpwp",
      "bio.bioidtipenik",
      "bio.bioidcorel"
    )
    .from("cdid_tenant_user as tu")
    // .innerJoin("cdid_tenant as tn", "tu.idtenant", "tn.id")
    .innerJoin(
      pool
        .select()
        .from("mst_biodata_corell")
        .where({ bioidcorel: 3 })
        .as("bio"),
      "tu.id",
      "bio.biocorelobjid"
    )
    .where("tu.userid", credential).whereIn("tu.active", [1, 5, 9]);

    console.log(">>>>>>>>>>>>>>>>>> Check User Level ", checklevelTenant[0].leveltenant);

    
    //##################### CHECK IF SUPPLIER OR EMPLOYEE #######################
    if(checklevelTenant[0].leveltenant > 1) {
      console.log(">>>>>>>>>>>>>>>>>> KELUAR NIH");
      let companybyUserList=[];
      

      var data = {"id":checklevelTenant[0].id, "credential": credential, "password": secret};
      var tokenID = "XXXXXX"
      var retunobject= jwttools.encryptdata(data);
      // setTimeout(function () {
      axios
      .get(
        conf.spbadminbe + "/spb/usermapping/getusermappingsbyiduser/"+checklevelTenant[0].id,
        {
          headers: { Authorization: "Bearer "+retunobject },
        }
      )
      .then(async (resp) => {
        // console.log("BALIKAN Company >>>>>>>>>>> ",resp.data);
        let datas = resp.data.data;
        if(datas.length > 0) {
          await datas.map((element) => {
            // console.log("Element ", element.companyname);
            companybyUserList.push(element.companyname);
          })
          console.log("Element ", companybyUserList);
          res.status(200).json({ status: 200, data: retunobject, tokenId: tokenID, companylist:companybyUserList });

        }
        
        // res.status(200).json({ status: 200, data: datas });
      })
      .catch((err) => {
        console.log("Error API ", err);
        return;
      });




        
      // }, 500);
      return true;

    }

//#############################################################################





    const resuserexist = await pool
      .select(
        "tu.id",
        "tu.fullname",
        "tu.userid",
        "tu.idtenant",
        "tu.idsubtenant",
        "tu.leveltenant",
        "tu.last_login",
        "tu.pwd",
        "tu.active",
        "tn.tnname",
        "tn.tnstatus",
        "bio.bioemailactive",
        "bio.biophoneactive",
        "bio.bioaddress",
        "bio.bionik",
        "bio.bionpwp",
        "bio.bioidtipenik",
        "bio.bioidcorel"
      )
      .from("cdid_tenant_user as tu")
      .innerJoin("cdid_tenant as tn", "tu.idtenant", "tn.id")
      .innerJoin(
        pool
          .select()
          .from("mst_biodata_corell")
          .where({ bioidcorel: 3 })
          .as("bio"),
        "tu.id",
        "bio.biocorelobjid"
      )
      .where("tu.userid", credential).whereIn("tu.active", [1, 5, 9]);
      // .where({ userid: credential, "tu.active": 1 });
      // 1. Aktive
      // 2. Disabled/Notactived
      // 3. Aktive namun belum Approve
      // 5. Edit To Deactived belum Approve
      // 7. Edit To Actived belum Approve
      // 9. Delete belum di Approve
    // fileLogadmin.info(`Login attemp - App: ${appname} , payload: ${JSON.stringify(objPayload)}`);
    // console.log("USER EXIST ",resuserexist);

    if (resuserexist.length > 0) {
      let validPassword = await passwordEnc.comparePassword(
        secret,
        resuserexist[0].pwd
      );
      // console.log(validPassword);
      if (!validPassword) {
        await loginUtil.updateAttempt(credential);
        res.status(500).json({ status: 500, data: "No User found" });
        return;
      }
      if (resuserexist[0].tnstatus !== 1) {
        res.status(500).json({ status: 500, data: "No User found" });
        return;
      }
      let isInActiveUserTime = await userActivity.isInActiveUserTime(
        credential
      );
      if (isInActiveUserTime) {
        res.status(500).json({ status: 500, data: "No User found" });
        return;
      }

      let isChangePasswordTime = await userActivity.isChangePasswordTime(
        credential
      );
      let notif;
      if (isChangePasswordTime) {
        notif = {
          changePassword: loginParam.CHANGE_PASSWORD_MESSAGE,
        };
      }

      jsonbody = resuserexist[0];
      jsonbody = { ...jsonbody, notif };
      let appObject = [];
      let countapp = 0;
      idtenant = jsonbody.idtenant;
      let orgdefault = { orgid: 0 };
      if (jsonbody.leveltenant > 0) {
        idtenant =
          jsonbody.leveltenant > 1 ? jsonbody.tnparentid : jsonbody.idtenant;
      }
      jsonbody = { ...jsonbody }; //, ...orgdefault };
      var application = {};
      var appsModuleObj = {};
      // console.log(jsonbody.leveltenant);
      console.log("LEVEL TENANT signviaadminsc",jsonbody.leveltenant);
      if (jsonbody.leveltenant < 1) {
        const liscenseapp = await pool
          .select(
            "clis.id_application as id",
            "clis.expiredate",
            "clis.id_application",
            "clis.paidstatus",
            "clis.active",
            "clis.defaultactive",
            "mapp.appname",
            "mapp.applabel",
            "mapp.description",
            "mapp.routelink",
            "mapp.withapproval"
          )
          .from("cdid_liscenses as clis")
          .innerJoin(
            "mst_application as mapp",
            "clis.id_application",
            "mapp.id"
          )
          .where({ "clis.cdid_tenant": idtenant, "clis.active": 1 })
          .orderBy("clis.defaultactive", "desc");
          console.log("LISCENSE APP signviaadminsc ",);
        if (liscenseapp) {
          countapp = liscenseapp.length;
          // appObject = liscenseapp.rows;
          // if (jsonbody.leveltenant > 1) {
          appObject = liscenseapp;
          // } else {
          //   var itemApp = liscenseapp.rows[0];
          //   appObject.push(itemApp);
          // }
          var menusModuleObj = [];
          var appsModuleList = [];
          appsModuleObj = { sidemenus: appsModuleList };
          application = { appscount: countapp, apps: appObject };
        }
      } else {
        const liscenseapp = await pool
          .select(
            "ma.id_application as id",
            "ma.expiredate",
            "ma.id_application",
            "ma.paidstatus",
            "ma.active",
            "ma.defaultactive",
            "ma.appname",
            "ma.applabel",
            "ma.routelink",
            "oa.idorg",
            "ma.withapproval"
          )
          .from("cdid_orguserassign as ou")
          .innerJoin("cdid_orgapplications as oa", "ou.orgid", "oa.idorg")
          .innerJoin(
            pool
              .select(
                "clis.id_application as id",
                "clis.expiredate as expiredate",
                "clis.id_application",
                "clis.paidstatus",
                "clis.active",
                "clis.defaultactive",
                "mapp.appname",
                "mapp.applabel",
                "mapp.description",
                "mapp.routelink",
                "mapp.withapproval"
              )
              .from("cdid_liscenses  as clis")
              .innerJoin(
                "mst_application as mapp",
                "clis.id_application",
                "mapp.id"
              )
              .where({
                "clis.cdid_tenant": idtenant,
                "clis.active": 1,
                "mapp.appname": appname,
              })
              .orderBy("clis.defaultactive ", "desc")
              .as("ma"),
            "oa.idapp",
            "ma.id"
          )
          .where({ "ou.userid": jsonbody.id });

        // console.log(req.body);  
        console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>> signviaadminsc >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        // console.log("LISCENSE APP signviaadminsc ", liscenseapp);
        if (liscenseapp) {
          // console.log(liscenseapp);
          countapp = liscenseapp.length;
          appObject = liscenseapp;
          // console.log(appObject);
          var menusModuleObj = [];
          var appsModuleList = [];
          appid =
            appObject[0].appname === appname ? appObject[0].id_application : 0;
          // appsModuleObj = { sidemenus: appsModuleList };
          //************ Looking Side menu of the user */
          application = { appscount: countapp, apps: appObject };
          let checkUser = await pool
            .where("iduser", jsonbody.id)
            .select("*")
            .from("cdid_tenant_user_groupacl");
          let userAssignData = checkUser.length;
          console.log("line 1056 " + userAssignData);
          let getmoduleadminapps;
          if (userAssignData > 0) {
            console.log(">>>>> QUERY MENU YANG ADA GROUP");
            getmoduleadminapps = await pool
              .select(
                "mug.idmenu",
                "tug.iduser",
                "tug.idgroupuseracl",
                "mmdl.id as idmdl",
                "mmdl.idapplication",
                "mmdl.modulename",
                "mmdl.iconclass",
                "mmnu.name",
                "mmnu.title",
                "mmnu.routelink",
                "mmnu.routepath",
                "mug.fcreate",
                "mug.fread",
                "mug.fupdate",
                "mug.fdelete",
                "mug.fview",
                "mug.fapproval"
              )
              .from("cdid_tenant_user_groupacl as tug")
              .innerJoin(
                "mst_usergroupacl as mug",
                "tug.idgroupuseracl",
                "mug.idgroup"
              )
              .innerJoin("mst_module as mmdl", "mug.idmodule ", "mmdl.id ")
              .innerJoin("mst_menu as mmnu", "mug.idmenu", "mmnu.id")
              .where({ "tug.iduser": jsonbody.id })
              .whereNot("mug.fread", 0)
              .orderBy([{ column: 'mmdl.id' }, { column: 'mug.idmenu', order: 'asc' }])

              // .orderBy([{ column: 'mmdl.idapplication' }, { column: 'mug.idmenu', order: 'asc' }])
              // .orderBy("mmdl.idapplication", "mug.idmenu");
              // .orderBy("mug.idmenu", "mmdl.idapplication");
            //.distinctOn("mug.idmenu");
          } else {
            console.log(">>>>> QUERY MENU YANG TIDAK GROUP");
            getmoduleadminapps = await pool
              .select(
                "mmd.id",
                "mbt.*",
                "mmd.status",
                "mmd.idapplication",
                "mmd.modulecode",
                "mmd.moduletype",
                "mmd.iconclass",
                "mnu.name",
                "mnu.title",
                "mnu.routelink "
              )
              .from("mst_module as mmd")
              .innerJoin(
                pool
                  .select("modulename")
                  .from("mst_moduleby_tenant  as mmdt")
                  .where({ idapplication: appid, idtenant: idtenant })
                  .distinct()
                  .as("mbt"),
                "mbt.modulename",
                "mmd.modulename"
              )
              .innerJoin("mst_menu as mnu", " mmd.id", "mnu.idmodule")
              .orderBy([{ column: 'mmd.id' }, { column: 'mnu.id', order: 'asc' }])
              // .orderBy("mmd.id", "mnu.id");
          }
         
          // console.log(">>> JUMLAH Module APP " + JSON.stringify(getmoduleadminapps));
          if (getmoduleadminapps.length > 0) {
            let modelesTmp = getmoduleadminapps;
            let tmpobj = {};
            let firstTime = 0;
            modelesTmp.forEach((element) => {
              // tmpobj = element;
              // console.log(object);
              if (element.modulename != modulename) {
                // tmpobj = element;
                if (modulename != "") {
                  let modulenmenus = {}
                  // tmpobj = element;
                  modulenmenus = {
                    label:element.modulename === modulename? element.modulename: modulename,
                    icon: element.modulename === modulename? element.iconclass: tmpobj.iconclass,
                    expanded: false,
                    items: menusModuleObj,
                  };
                  if(element.modulename != modulename) tmpobj = element;

                  appsModuleList.push(modulenmenus);
                    modulename = element.modulename;
                    modulenameEnd = element.modulename;
                    appsid = element.idapplication;
                    menusModuleObj = [];
                  
                } else {
                  tmpobj = element;
                  modulename = element.modulename;
                  modulenameEnd = element.modulename;
                  appsid = element.idapplication;
                  // console.log(">>>>>>> modulename awal >>>> " + modulename);
                  menusModuleObj = [];
                  
                }
              } 
              // console.log("Insert menu aja");
              menusModuleObj.push({
                label: element.title,
                icon: "pi pi-link",
                routerLink: element.routelink,
                acl: {
                  create: element.fcreate,
                  read: element.fread,
                  update: element.fupdate,
                  delete: element.fdelete,
                  view: element.fview,
                  approval: element.fapproval,
                },
              });
            });

            if (modulename === modulenameEnd) {
              // console.log(
              //   ">>>>>>> Modul akhir " + (modulename === modulenameEnd)
              // );
              let modulenmenus = {
                label: modulename,
                expanded: false,
                icon: tmpobj.iconclass,
                items: menusModuleObj,
              };
              appsModuleList.push(modulenmenus);
              appsModuleObj = { sidemenus: appsModuleList };
            }
            // dcodeInfo = { ...dcodeInfo, ...application, ...appsModuleObj };
            // console.log(application);
            await application.apps.map((app) => {
              app.idapp = app.id;
            });
            // console.log(application);
            jsonbody = { ...jsonbody, ...application, ...appsModuleObj };
            // console.log(">>>>>>> Kirim Balik " + JSON.stringify(jsonbody));
          }
        }
      }
      // jsonbody = { ...jsonbody, ...application, ...appsModuleObj };

      //#################### CHECK Session if exist ##########################
      let sessionid = "";
      await loginUtil.updateAttempt(credential, 0);
      await loginUtil.updateLoginTime(credential);

      const checkTodayLog = await pool
        .where({ created_by: jsonbody.id, status: 1 })
        .select("*")
        .from("trx_sessionlog ")
        .orderBy("created_at");

      if (checkTodayLog.length > 0) {
        // console.log(">>>>>>>>>>>>>>>>>>>>>>> SINI 1 <<<<<<<<<<<<<<<<<<<");
        sessionid = checkTodayLog[0].id;
        var token = jwttools.encryptdata(sessionid);
        setTimeout(function () {
          // fileLogadmin.info(`Login response - App: ${appname} , status: Success`);
          res
            .status(200)
            .json({ status: 200, data: token, tokenId: sessionid });
        }, 500);
      } else {
        //const today = moment().format("YYYY-MM-DD HH:mm:ss");
        const now = new Date();

        const expire = new Date(
          now.getTime() + loginParam.SESSION_IDLE * 60000
        );

        // console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> EXP " + expire);

        // const insertTodayLog = await pool.query(
        //   "INSERT INTO public.trx_sessionlog(created_by, userinfo, expire_date, status) VALUES ($1, $2, $3, $4) RETURNING *",
        //   [jsonbody.id, jsonbody, expire, 1]
        // );
        const insertTodayLog = await pool("trx_sessionlog")
          // .returning([
            // "id",
            // "expire_date",
            // "created_by",
            // "status",
            // "created_at",
            // "updated_at",
          // ])
          .insert({
            created_by: jsonbody.id,
            userinfo: JSON.stringify(jsonbody),
            expire_date: expire,
            status: 1,
          });
        // console.log("Returning from session log ", insertTodayLog);
        if (insertTodayLog.length > 0) {
          sessionid=insertTodayLog[0];
          // sessionid = insertTodayLog[0].id;
        
          var token = jwttools.encryptdata(sessionid);
        
          // fileLogadmin.info(
          //   `Login response - App: ${appname} , status: Success`
          // );
         
          res.status(200).json({ status: 200, data: token, tokenId: sessionid });
          // setTimeout(function () {
          //   res
          //     .status(200)
          //     .json({ status: 200, data: token, tokenId: sessionid });
          // }, 500);
        } else {
          res.status(500).json({ status: 500, data: "No User found" });
        }
      }
    } else {
      // if resuserexist < 1
      await loginUtil.updateAttempt(credential);
      fileLogadmin.error(
        `Login response - App: ${appname} , status: No User found`
      );
      res.status(500).json({ status: 500, data: "No User found" });
    }
  } catch (err) {
    const { credential, secret, appname } = req.body;
    const fileLogadmin = logger.logging(appname);
    fileLogadmin.error(`Login response - App: ${appname} , ${err}`);
    console.log(err);
    //throw new Error("Generated Error.");
    next(err);
    res.status(500).json({ status: 500, data: "No User found" });
  }
  // let test = {message:"berhasil"};
  // res.status(200).json({ status: 200, data: test });
});
router.post("/signviacompanysc", async (req, res, next) => {
  try {
    await getParam();
    var jsonbody = {};
    var idtenant = "0";
    var ps;
    var appid;
    let modulename = "";
    let modulenameEnd = "";
    fileLog.info(`Login Request `);
    console.log("INI REQUEST signviaadminsc");
    const { credential, secret, appname, tnid } = req.body;
    const fileLogadmin = logger.logging(appname);
    let totalAttempt = await loginUtil.checktAttempt(credential);

    if (totalAttempt > loginParam.LOGIN_ATTEMPT) {
      await loginUtil.updateLockTime(credential);
      //let isUnlockTime = await loginUtil.isUnlockTime(credential);
      //if (isUnlockTime == false) {
      res.status(200).json({
        status: 403,
        data: `Login Attempt is more than ${loginParam.LOGIN_ATTEMPT} times. User has been locked, please contact your admin`,
      });
      return;
      //}
    }

    let objPayload = { userid: credential };
    
    //##################### CHECK IF SUPPLIER OR EMPLOYEE #######################
    
    //#############################################################################
    idtenant = tnid;
    const resuserexist = await pool
      .select(
        "tu.id",
        "tu.fullname",
        "tu.userid",
        "tu.idtenant",
        pool.raw(tnid+" as idsubtenant"),
        "tu.leveltenant",
        "tu.last_login",
        "tu.pwd",
        "tu.active",
        "bio.bioname as tnname",
        "tu.active as tnstatus",
        "bio.bioemailactive",
        "bio.biophoneactive",
        "bio.bioaddress",
        "bio.bionik",
        "bio.bionpwp",
        "bio.bioidtipenik",
        "bio.bioidcorel"
      )
      .from("cdid_tenant_user as tu")
      .innerJoin(
        pool
          .select()
          .from("mst_biodata_corell")
          .where({ bioidcorel: 3 })
          .as("bio"),
        "tu.id",
        "bio.biocorelobjid"
      )
      .where("tu.userid", credential).whereIn("tu.active", [1, 5, 9]);


      console.log(">>>>>>>>>>>>>>>>>> Check User Level ", resuserexist[0].leveltenant);






      // .where({ userid: credential, "tu.active": 1 });
      // 1. Aktive
      // 2. Disabled/Notactived
      // 3. Aktive namun belum Approve
      // 5. Edit To Deactived belum Approve
      // 7. Edit To Actived belum Approve
      // 9. Delete belum di Approve
    // fileLogadmin.info(`Login attemp - App: ${appname} , payload: ${JSON.stringify(objPayload)}`);
    // console.log("USER EXIST ",resuserexist);

    if (resuserexist.length > 0) {
      let validPassword = await passwordEnc.comparePassword(
        secret,
        resuserexist[0].pwd
      );
      // console.log(validPassword);
      if (!validPassword) {
        await loginUtil.updateAttempt(credential);
        res.status(500).json({ status: 500, data: "No User found" });
        return;
      }
      if (resuserexist[0].tnstatus !== 1) {
        res.status(500).json({ status: 500, data: "No User found" });
        return;
      }
      let isInActiveUserTime = await userActivity.isInActiveUserTime(
        credential
      );
      if (isInActiveUserTime) {
        res.status(500).json({ status: 500, data: "No User found" });
        return;
      }

      let isChangePasswordTime = await userActivity.isChangePasswordTime(
        credential
      );
      let notif;
      if (isChangePasswordTime) {
        notif = {
          changePassword: loginParam.CHANGE_PASSWORD_MESSAGE,
        };
      }

      jsonbody = resuserexist[0];
      jsonbody = { ...jsonbody, notif };
      let appObject = [];
      let countapp = 0;
      idtenant = jsonbody.idtenant;
      let orgdefault = { orgid: 0 };
      if (jsonbody.leveltenant > 0) {
        idtenant = tnid
          // jsonbody.leveltenant > 1 ? jsonbody.tnparentid : jsonbody.idtenant;
      }
      jsonbody = { ...jsonbody }; //, ...orgdefault };
      var application = {};
      var appsModuleObj = {};
      // console.log(jsonbody.leveltenant);
      console.log("LEVEL TENANT signviacompanysc",jsonbody.leveltenant);
      // console.log("LEVEL TENANT signviacompanysc",jsonbody.leveltenant);


      if (jsonbody.leveltenant < 1) {
      } else {
        const liscenseapp = await pool
          .select(
            "ma.id_application as id",
            "ma.expiredate",
            "ma.id_application",
            "ma.paidstatus",
            "ma.active",
            "ma.defaultactive",
            "ma.appname",
            "ma.applabel",
            "ma.routelink",
            "oa.idorg",
            "ma.withapproval"
          )
          .from("cdid_orguserassign as ou")
          .innerJoin("cdid_orgapplications as oa", "ou.orgid", "oa.idorg")
          .innerJoin(
            pool
              .select(
                "clis.id_application as id",
                "clis.expiredate as expiredate",
                "clis.id_application",
                "clis.paidstatus",
                "clis.active",
                "clis.defaultactive",
                "mapp.appname",
                "mapp.applabel",
                "mapp.description",
                "mapp.routelink",
                "mapp.withapproval"
              )
              .from("cdid_liscenses  as clis")
              .innerJoin(
                "mst_application as mapp",
                "clis.id_application",
                "mapp.id"
              )
              .where({
                "clis.cdid_tenant": idtenant,
                "clis.active": 1,
                "mapp.appname": appname,
              })
              .orderBy("clis.defaultactive ", "desc")
              .as("ma"),
            "oa.idapp",
            "ma.id"
          )
          .where({ "ou.userid": jsonbody.id });

        // console.log(req.body);  
        console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>> signviacompanysc >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        // console.log("LISCENSE APP signviaadminsc ", liscenseapp);
        if (liscenseapp) {
          // console.log(liscenseapp);
          countapp = liscenseapp.length;
          appObject = liscenseapp;
          // console.log(appObject);
          var menusModuleObj = [];
          var appsModuleList = [];
          appid =
            appObject[0].appname === appname ? appObject[0].id_application : 0;
          // appsModuleObj = { sidemenus: appsModuleList };
          //************ Looking Side menu of the user */
          application = { appscount: countapp, apps: appObject };
          let checkUser = await pool
            .where("iduser", jsonbody.id)
            .select("*")
            .from("cdid_tenant_user_groupacl");
          let userAssignData = checkUser.length;
          console.log("line 1056 " + userAssignData);
          let getmoduleadminapps;
          if (userAssignData > 0) {
            console.log(">>>>> QUERY MENU YANG ADA GROUP");
            getmoduleadminapps = await pool
              .select(
                "mug.idmenu",
                "tug.iduser",
                "tug.idgroupuseracl",
                "mmdl.id as idmdl",
                "mmdl.idapplication",
                "mmdl.modulename",
                "mmnu.name",
                "mmnu.title",
                "mmnu.routelink",
                "mmnu.routepath",
                "mug.fcreate",
                "mug.fread",
                "mug.fupdate",
                "mug.fdelete",
                "mug.fview",
                "mug.fapproval"
              )
              .from("cdid_tenant_user_groupacl as tug")
              .innerJoin(
                "mst_usergroupacl as mug",
                "tug.idgroupuseracl",
                "mug.idgroup"
              )
              .innerJoin("mst_module as mmdl", "mug.idmodule ", "mmdl.id ")
              .innerJoin("mst_menu as mmnu", "mug.idmenu", "mmnu.id")
              .where({ "tug.iduser": jsonbody.id })
              .whereNot("mug.fread", 0)
              .orderBy([{ column: 'mmdl.id' }, { column: 'mug.idmenu', order: 'asc' }])
              // .orderBy([{ column: 'mmdl.idapplication' }, { column: 'mug.idmenu', order: 'asc' }])
              // .orderBy("mmdl.idapplication", "mug.idmenu");
              // .orderBy("mug.idmenu", "mmdl.idapplication");
            //.distinctOn("mug.idmenu");
          } else {
            console.log(">>>>> QUERY MENU YANG TIDAK GROUP");
            getmoduleadminapps = await pool
              .select(
                "mmd.id",
                "mbt.*",
                "mmd.status",
                "mmd.idapplication",
                "mmd.modulecode",
                "mmd.moduletype",
                "mmd.iconclass",
                "mnu.name",
                "mnu.title",
                "mnu.routelink "
              )
              .from("mst_module as mmd")
              .innerJoin(
                pool
                  .select("modulename")
                  .from("mst_moduleby_tenant  as mmdt")
                  .where({ idapplication: appid, idtenant: idtenant })
                  .distinct()
                  .as("mbt"),
                "mbt.modulename",
                "mmd.modulename"
              )
              .innerJoin("mst_menu as mnu", " mmd.id", "mnu.idmodule")
              .orderBy([{ column: 'mmd.id' }, { column: 'mnu.id', order: 'asc' }])
              // .orderBy("mmd.id", "mnu.id");
          }
         
          // console.log(">>> JUMLAH Module APP " + JSON.stringify(getmoduleadminapps));
          if (getmoduleadminapps.length > 0) {
            let modelesTmp = getmoduleadminapps;
            let tmpobj = {};
            let firstTime = 0;
            modelesTmp.forEach((element) => {
              // tmpobj = element;
              // console.log(object);
              if (element.modulename != modulename) {
                // tmpobj = element;
                if (modulename != "") {
                  let modulenmenus = {}
                  // tmpobj = element;
                  modulenmenus = {
                    label:element.modulename === modulename? element.modulename: modulename,
                    icon: element.modulename === modulename? element.iconclass: tmpobj.iconclass,
                    expanded: false,
                    items: menusModuleObj,
                  };
                  if(element.modulename != modulename) tmpobj = element;

                  appsModuleList.push(modulenmenus);
                    modulename = element.modulename;
                    modulenameEnd = element.modulename;
                    appsid = element.idapplication;
                    menusModuleObj = [];
                  
                } else {
                  tmpobj = element;
                  modulename = element.modulename;
                  modulenameEnd = element.modulename;
                  appsid = element.idapplication;
                  // console.log(">>>>>>> modulename awal >>>> " + modulename);
                  menusModuleObj = [];
                  
                }
              } 
              // console.log("Insert menu aja");
              menusModuleObj.push({
                label: element.title,
                icon: "pi pi-link",
                routerLink: element.routelink,
                acl: {
                  create: element.fcreate,
                  read: element.fread,
                  update: element.fupdate,
                  delete: element.fdelete,
                  view: element.fview,
                  approval: element.fapproval,
                },
              });
            });

            if (modulename === modulenameEnd) {
              // console.log(
              //   ">>>>>>> Modul akhir " + (modulename === modulenameEnd)
              // );
              let modulenmenus = {
                label: modulename,
                expanded: false,
                icon: tmpobj.iconclass,
                items: menusModuleObj,
              };
              appsModuleList.push(modulenmenus);
              appsModuleObj = { sidemenus: appsModuleList };
            }
            // dcodeInfo = { ...dcodeInfo, ...application, ...appsModuleObj };
            // console.log(application);
            await application.apps.map((app) => {
              app.idapp = app.id;
            });
            // console.log(application);
            jsonbody = { ...jsonbody, ...application, ...appsModuleObj };
            // console.log(">>>>>>> Kirim Balik " + JSON.stringify(jsonbody));
          }
        }
      }
      // jsonbody = { ...jsonbody, ...application, ...appsModuleObj };

      //#################### CHECK Session if exist ##########################
      let sessionid = "";
      await loginUtil.updateAttempt(credential, 0);
      await loginUtil.updateLoginTime(credential);

      const checkTodayLog = await pool
        .where({ created_by: jsonbody.id, status: 1 })
        .select("*")
        .from("trx_sessionlog ")
        .orderBy("created_at");

      if (checkTodayLog.length > 0) {
        console.log(">>>>>>>>>>>>>>>>>>>>>>> SINI COMPANY <<<<<<<<<<<<<<<<<<<");
        sessionid = checkTodayLog[0].id;
        var token = jwttools.encryptdata(sessionid);
        setTimeout(function () {
          // fileLogadmin.info(`Login response - App: ${appname} , status: Success`);
          res
            .status(200)
            .json({ status: 200, data: token, tokenId: sessionid });
        }, 500);
      } else {
        //const today = moment().format("YYYY-MM-DD HH:mm:ss");
        const now = new Date();

        const expire = new Date(
          now.getTime() + loginParam.SESSION_IDLE * 60000
        );

        // console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> EXP " + expire);

        // const insertTodayLog = await pool.query(
        //   "INSERT INTO public.trx_sessionlog(created_by, userinfo, expire_date, status) VALUES ($1, $2, $3, $4) RETURNING *",
        //   [jsonbody.id, jsonbody, expire, 1]
        // );
        const insertTodayLog = await pool("trx_sessionlog")
          // .returning([
            // "id",
            // "expire_date",
            // "created_by",
            // "status",
            // "created_at",
            // "updated_at",
          // ])
          .insert({
            created_by: jsonbody.id,
            userinfo: JSON.stringify(jsonbody),
            expire_date: expire,
            status: 1,
          });
        // console.log("Returning from session log ", insertTodayLog);
        if (insertTodayLog.length > 0) {
          sessionid=insertTodayLog[0];
          // sessionid = insertTodayLog[0].id;
        
          var token = jwttools.encryptdata(sessionid);
        
          // fileLogadmin.info(
          //   `Login response - App: ${appname} , status: Success`
          // );
         
          res.status(200).json({ status: 200, data: token, tokenId: sessionid });
          // setTimeout(function () {
          //   res
          //     .status(200)
          //     .json({ status: 200, data: token, tokenId: sessionid });
          // }, 500);
        } else {
          res.status(500).json({ status: 500, data: "No User found" });
        }
      }
    } else {
      // if resuserexist < 1
      await loginUtil.updateAttempt(credential);
      fileLogadmin.error(
        `Login response - App: ${appname} , status: No User found`
      );
      res.status(500).json({ status: 500, data: "No User found" });
    }
  } catch (err) {
    const { credential, secret, appname } = req.body;
    const fileLogadmin = logger.logging(appname);
    fileLogadmin.error(`Login response - App: ${appname} , ${err}`);
    console.log(err);
    //throw new Error("Generated Error.");
    next(err);
    res.status(500).json({ status: 500, data: "No User found" });
  }
  // let test = {message:"berhasil"};
  // res.status(200).json({ status: 200, data: test });
});
router.post("/changeapp", async (req, res, next) => {
  try {
    const { appid } = req.body;
    let systemdate = new Date();
    const authHeader = req.headers.authorization;
    var dcodeInfo = req.userData;
    var tokenID = req.tokenID;
    var idTenant =
      dcodeInfo.leveltenant > 1 ? dcodeInfo.idsubtenant : dcodeInfo.idtenant;

    const liscenseapp = await pool
      .select(
        "lapp.id",
        "lapp.expiredate",
        "lapp.cdid_tenant",
        "lapp.paidstatus",
        "lapp.active",
        //"1 as defaultactive",
        "lapp.appname",
        "lapp.applabel",
        "oau.userid",
        "oau.orgid",
        "oau.grpid",
        "mo.orgname",
        "oapp.idapp",
        "lapp.routelink"
      )
      .from("cdid_orguserassign as oau")
      .innerJoin("mst_organizations as mo", "oau.orgid", "mo.id")
      .innerJoin("cdid_orgapplications as oapp", "mo.id", "oapp.idorg")
      .leftJoin(
        pool
          .select(
            "clis.id",
            "clis.licensetokens",
            "clis.licensejson",
            "clis.expiredate as expiredate",
            "cdid_tenant",
            "clis.id_application",
            "clis.paidstatus",
            "clis.active",
            "clis.defaultactive",
            "mapp.appname",
            "mapp.applabel",
            "mapp.description",
            "mapp.routelink"
          )
          .from("cdid_liscenses as clis")
          .innerJoin(
            "mst_application as mapp",
            "clis.id_application",
            "mapp.id"
          )
          .where({ "clis.cdid_tenant": idTenant, "clis.active": 1 })
          .orderBy("clis.defaultactive", "desc")
          .as("lapp"),
        "oapp.idapp",
        "lapp.id_application"
      )
      .where({ " oau.userid": dcodeInfo.id, "oapp.idapp": appid });
    // console.log(">>> PARAM " + idTenant + ", " + dcodeInfo.id + ", " + appid);
    // console.log(">>> JUMLAH Liscense APP " + liscenseapp.length);
    if (liscenseapp.length > 0) {
      countapp = liscenseapp.length;
      appObject = liscenseapp;

      console.log(">>> APP Active :" + JSON.stringify(appObject));

      var menusModuleObj = [];
      var appsModuleList = [];
      var appsModuleObj = {};
      var application = { appscount: countapp, apps: appObject };
      let modulename = "";
      let modulenameEnd = "";
      var queryMenu;
      var ps;
      /// Select side menu
      console.log(">>> Level tenant " + dcodeInfo.leveltenant);

      let getmoduleadminapps;
      switch (parseInt(dcodeInfo.leveltenant)) {
        case 0:
          getmoduleadminapps = await pool
            .where({ "mm.idapplication": appid, "mm.moduletype": 1 })
            .select(
              "mm.id",
              "mm.modulename",
              "mm.status",
              "mm.idapplication",
              "mm.modulecode",
              "mm.moduletype",
              "mnu.name",
              "mnu.title",
              "mnu.routelink"
            )
            .from("mst_module as mm ")
            .innerJoin("mst_menu as mnu", "mm.id", "mnu.idmodule");
          break;
        case 1:
          let checkUser = await pool
            .where({ iduser: dcodeInfo.id })
            .select("*")
            .from("cdid_tenant_user_groupacl ");

          let userAssignData = checkUser.length;
          //console.log("line 508 " + dcodeInfo.id);
          // console.log("line 508 " + userAssignData);

          if (userAssignData > 0) {
            getmoduleadminapps = await pool
              .select(
                "mug.idmenu",
                "tug.iduser",
                "tug.idgroupuseracl",
                "mmdl.idapplication",
                "mmdl.modulename",
                "mmnu.name",
                "mmnu.title",
                "mmnu.routelink",
                "mmnu.routepath",
                "mug.fcreate",
                "mug.fread",
                "mug.fupdate",
                "mug.fdelete",
                "mug.fview",
                "mug.fapproval"
              )
              .from("cdid_tenant_user_groupacl as tug")
              .innerJoin(
                "mst_usergroupacl as mug",
                "tug.idgroupuseracl",
                "mug.idgroup"
              )
              .innerJoin("mst_menu as mmnu", "mug.idmenu", "mmnu.id")
              .innerJoin("mst_module as mmdl", "mug.idmodule", "mmdl.id")
              .where({ "tug.iduser": dcodeInfo.id })
              .orderBy("mug.idmenu", "mmdl.idapplication");
            //.distinctOn("mug.idmenu");
          } else {
            getmoduleadminapps = await pool
              .select(
                "mmd.id",
                "mbt.*",
                "mmd.status",
                "mmd.idapplication",
                "mmd.modulecode",
                "mmd.moduletype",
                "mmd.iconclass",
                "mnu.name",
                "mnu.title",
                "mnu.routelink "
              )
              .from("mst_module as mmd")
              .innerJoin(
                pool
                  .select("modulename")
                  .from("mst_moduleby_tenant  as mmdt")
                  .where({ idapplication: appid, idtenant: dcodeInfo.idtenant })
                  .distinct()
                  .as("mbt"),
                "mbt.modulename",
                "mmd.modulename"
              )
              .innerJoin("mst_menu as mnu", " mmd.id", "mnu.idmodule");
          }

          break;
        case 2:
          let checkUserACL = await pool
            .where({ iduser: dcodeInfo.id })
            .select("*")
            .from("cdid_tenant_user_groupacl ");
          let userAssignDataACL = checkUserACL.length;

          if (userAssignDataACL > 0) {
            getmoduleadminapps = await pool
              .select(
                "mug.idmenu",
                "tug.iduser",
                "tug.idgroupuseracl",
                "mmdl.idapplication",
                "mmdl.modulename",
                "mmnu.name",
                "mmnu.title",
                "mmnu.routelink",
                "mmnu.routepath",
                "mug.fcreate",
                "mug.fread",
                "mug.fupdate",
                "mug.fdelete",
                "mug.fview",
                "mug.fapproval"
              )
              .from("cdid_tenant_user_groupacl as tug")
              .innerJoin(
                "mst_usergroupacl as mug",
                "tug.idgroupuseracl",
                "mug.idgroup"
              )
              .innerJoin("mst_menu as mmnu", "mug.idmenu", "mmnu.id")
              .innerJoin("mst_module as mmdl", "mug.idmodule", "mmdl.id")
              .where({ "tug.iduser": dcodeInfo.id })
              .orderBy("mug.idmenu", "mmdl.idapplication");
            //.distinctOn("mug.idmenu");
          } else {
            getmoduleadminapps = await pool
              .select(
                "mmd.id",
                "mbt.*",
                "mmd.status",
                "mmd.idapplication",
                "mmd.modulecode",
                "mmd.moduletype",
                "mnu.name",
                "mnu.title",
                "mnu.routelink "
              )
              .from("mst_module as mmd")
              .innerJoin(
                pool
                  .select("modulename")
                  .from("mst_moduleby_tenant  as mmdt")
                  .where({ idapplication: appid, idtenant: idtenant })
                  .distinct()
                  .as("mbt"),
                "mbt.modulename",
                "mmd.modulename"
              )
              .innerJoin("mst_menu as mnu", " mmd.id", "mnu.idmodule");
          }

          break;
        default:
          getmoduleadminapps = await pool
            .select(
              "mm.id",
              "mm.modulename",
              "mm.status",
              "mm.idapplication",
              "mm.modulecode",
              "mm.moduletype",
              "mnu.name",
              "mnu.title",
              "mnu.routelink"
            )
            .from("mst_module mm")
            .innerJoin("mst_menu as mnu", " mm.id", "mnu.idmodule")
            .where({ "mm.idapplication": appid, "mm.moduletype": 1 });
          break;
      }
      console.log("line 1544 ");
      // console.log(getmoduleadminapps);
      if (getmoduleadminapps.length > 0) {
        let modelesTmp = getmoduleadminapps;
        modelesTmp.forEach((element) => {
          if (element.modulename != modulename) {
            if (modulename != "") {
              let modulenmenus = {
                label: modulename,
                expanded: true,
                items: menusModuleObj,
              };
              appsModuleList.push(modulenmenus);
              modulename = element.modulename;
              modulenameEnd = element.modulename;
              appsid = element.idapplication;
              menusModuleObj = [];
            } else {
              modulename = element.modulename;
              modulenameEnd = element.modulename;
              appsid = element.idapplication;
              // console.log(">>>>>>> modulename awal >>>> " + modulename);
              menusModuleObj = [];
            }
          }
          menusModuleObj.push({
            label: element.title,
            icon: "pi pi-link",
            routerLink: element.routelink,
          });
        });
        if (modulename === modulenameEnd) {
          // console.log(">>>>>>> Modul akhir " + (modulename === modulenameEnd));
          let modulenmenus = {
            label: modulename,
            expanded: true,
            items: menusModuleObj,
          };
          appsModuleList.push(modulenmenus);
          appsModuleObj = { sidemenus: appsModuleList };
        }
        dcodeInfo = { ...dcodeInfo, ...application, ...appsModuleObj };

        const sidemenuUpdateResp = await pool("trx_sessionlog")
          // .returning(["id"])
          .where("id", tokenID)
          .update({ userinfo: dcodeInfo });

        if (sidemenuUpdateResp.length > 0) {
          var token = jwttools.encryptdata(tokenID);
          // setTimeout(function() {
          res.status(200).json({ status: 200, data: token });
          // }, 1000);
        } else {
          res
            .status(500)
            .json({ status: 500, data: "Error Change App Inside" });
        }
      }
    }
  } catch (err) {
    next(err);
    res.status(500).json({ status: 500, data: "Error Change App Catch" });
  }
});

router.post("/signout", async (req, res, next) => {
  try {
    const { appid, appname } = req.body;
    const fileLogadmin = logger.logging(appname);
    console.log(">>>>>>>>>>>>>>>>> PAYLOD SIGNOUT " + JSON.stringify(req.body));
    const dcodeInfo = req.userData;
    const tokenId = req.tokenID;
    // console.log(">>>>>>>>>>>>>>>>> USER DATA SIGN OUT ", dcodeInfo);
    // console.log(">>>>>>>>>>>>>>>>> TIOKEN ID ", tokenId);
    if (tokenId) {
      const signoutResp = await pool("trx_sessionlog")
        // .returning(["id"])
        .where("id", tokenId)
        .update({ status: 2 });
      console.log("On signout return ",signoutResp);
      // if (signoutResp.length > 0) {
        if (signoutResp > 0) {
        fileLogadmin.info(
          `Sign out attemp - App: ${appname} , status: success`
        );
        res.status(200).json({ status: 200, data: { status: "success" } });
      }
    } else {
      res.status(200).json({ status: 200, data: { status: "success" } });
    }
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "Error Change App Catch" });
  }
});

/*POST END*/

router.get("/who", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const dcodeInfo = req.userData;
    const idtrx = req.tokenID;
    const isexp = req.isexpired;
    console.log(">>>>>>>>>>>>>>>>>>>>>>> SINI 1 <<<<<<<<<<<<<<<<<<<");
    res
      .status(200)
      .json({ status: 200, data: dcodeInfo, tokenId: idtrx, exp: isexp });
  } catch (err) {
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});

module.exports = router;
