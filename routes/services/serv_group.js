const express = require("express"),
  app = express();
const router = express.Router();
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");
const eventLog = require("../../middleware/eventLog");
router.use(checkAuth);
router.use(eventLog);

//getAllGroup
router.get("/getAllGroup", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;

  var apps = dcodeInfo.apps[0];

  try {
    console.log(dcodeInfo.leveltenant);

    const userGroup = await pool
      .select("*")
      .from("mst_usergroup")
      .where({ idapplication: apps.id, idtenant: dcodeInfo.idtenant })
      .andWhere(
        "grouptype",
        ">=",
        dcodeInfo.leveltenant == "0" ? 0 : dcodeInfo.leveltenant
      );

    if (userGroup.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: { userGroup: userGroup } });
      }, 100);
    } else {
      res.status(200).json({ status: 200, data: "Data tidak ditemukan" });
    }
  } catch (err) {
    //console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get("/getAllGroupForData", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;

  var apps = dcodeInfo.apps[0];

  try {
    console.log(dcodeInfo.leveltenant);

    const userGroup = await pool
      .select("*")
      .from("mst_usergroup")
      .where({ idapplication: apps.id, idtenant: dcodeInfo.idtenant })
      .andWhere(
        "grouptype",
        ">=",
        dcodeInfo.leveltenant == "0" ? 0 : dcodeInfo.leveltenant
      ).whereIn("active", [1, 7, 9]);

    if (userGroup.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: { userGroup: userGroup } });
      }, 1000);
    } else {
      res.status(200).json({ status: 200, data: "Data tidak ditemukan" });
    }
  } catch (err) {
    //console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

//getGroupBy Type
router.get("/getExternalGroup", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = null;
  if (authHeader) {
    TokenArray = authHeader.split(" ");
    dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
    var apps = dcodeInfo.apps[0];
    try {
      console.log(dcodeInfo.leveltenant);
      const userGroup = await pool.select("*").from("mst_usergroup").where({
        idapplication: apps.id,
        idtenant: dcodeInfo.idtenant,
        grouptype: 2,
      });
      if (userGroup.length > 0) {
        res.status(200).json({ status: 200, data: { userGroup: userGroup } });
      } else {
        res.status(200).json({ status: 200, data: "Data tidak ditemukan" });
      }
    } catch (err) {
      //console.log(err);
      next(err);
      res.status(500).json({ status: 500, data: "internal error" });
    }
  } else {
    console.log("no header");
    res.sendStatus(403);
  }
});

//Get all Module and menu
router.get("/getallmodulewithmenu", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];
  try {
    const modules = await pool.select("*").from("mst_moduleby_tenant");
    if (modules.length > 0) {
      let result = [];
      try {
        let modulesList = modules;

        await Promise.all(
          modulesList.map(async (module) => {
            let moduleDetail = {};
            moduleDetail.moduleId = module.id;
            moduleDetail.moduleName = module.modulename;

            let menus = await pool
              .select("*")
              .from("mst_menu")
              .where({ idmodile: module.id });

            if (menus.length > 0) {
              let menuRows = menus;
              let menuList = [];
              let menuDetail = {};
              await Promise.all(
                menuRows.map((menu) => {
                  menuDetail = {};
                  menuDetail.menuName = menu.title;
                  menuDetail.menuId = menu.id;
                  menuList.push(menuDetail);
                })
              );

              moduleDetail.moduleMenus = menuList;
            }
            result.push(moduleDetail);
          })
        );

        res.status(200).json({ status: 200, data: result });
      } catch (error) {
        console.log(err);
      }
    } else {
      res.status(200).json({ status: 200, data: "Data tidak ditemukan" });
    }
  } catch (err) {
    //console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

//Get all Module
router.get("/getallmodules", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = null;
  if (authHeader) {
    TokenArray = authHeader.split(" ");
    dcodeInfo = await jwttools.decryptdata(TokenArray[1]);

    var apps = dcodeInfo.apps[0];
    try {
      // const modules = await pool.query(
      //   "Select * from public.mst_moduleby_tenant ",
      //   []
      // );

      const modules = await pool.select("*").from("mst_moduleby_tenant");
      if (modules.length > 0) {
        let result = [];
        result = modules;
        res.status(200).json({ status: 200, data: result });
      } else {
        res.status(200).json({ status: 200, data: "Data tidak ditemukan" });
      }
    } catch (err) {
      //console.log(err);
      next(err);
      res.status(500).json({ status: 500, data: "internal error" });
    }
  } else {
    console.log("no header");
    res.sendStatus(403);
  }
});

//Add Group
router.post("/regisGroup", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];
  let idTenant = dcodeInfo.idtenant;
  try {
    // const { group, modules, menus } = req.body;
    if(apps.withapproval > 0) {
      // const { group, modules, menus } = dataobject;
      console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
      // console.log(req.body.group);
      let grptmp = req.body.group
      let activeobj = {active:3}
      grptmp = {...grptmp,...activeobj}
      let cgrptmp = {group:grptmp}
      // console.log(grptmp);
      req.body = {...req.body,...cgrptmp }
      // console.log(req.body);
      // let insertData = await groupUtils.insertGroup(
      //   req.body,
      //   apps,
      //   idTenant,
      //   dcodeInfo.leveltenant
      // );
      // console.log(insertData);
      // if (insertData === 1) {
      //   res.status(200).json({ status: 200, data: "" });
      // } else {
      //   res.status(422).json({ status: 422, data: "Unprocessable data" });
      // }
      console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
    } else {
      let grptmp = req.body.group
      let activeobj = {active:1}
      grptmp = {...grptmp,...activeobj}
      let cgrptmp = {group:grptmp}
      req.body = {...req.body,...cgrptmp }
      console.log("########## GROUP PAYLOAD ", req.body);
      // let insertData = await groupUtils.insertGroup(
      //   req.body,
      //   apps,
      //   idTenant,
      //   dcodeInfo.leveltenant
      // );
      // console.log(insertData);
      // if (insertData === 1) {
      //   res.status(200).json({ status: 200, data: "" });
      // } else {
      //   res.status(422).json({ status: 422, data: "Unprocessable data" });
      // }
    }
    // req.body = {...req.body,...cgrptmp }
      // console.log(req.body);



      let insertData = await groupUtils.insertGroup(
        req.body,
        apps,
        idTenant,
        dcodeInfo.leveltenant
      );

      // res.status(200).json({ status: 200, data: "" });
      if (insertData === 1) {
        res.status(200).json({ status: 200, data: "" });
      } else {
        res.status(422).json({ status: 422, data: "Unprocessable data" });
      }



    
  } catch (err) {
    //console.log(err);
    next(err);
    res.status(422).json({ status: 422, data: "Unprocessable data" });
  }
});
//Get Detail Group
router.get("/getDetailsGroup/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;

  var apps = dcodeInfo.apps[0];
  try {
    let groupId = req.params.id;
    let result = {};

    const userGroup = await pool.select("*").from("mst_usergroup").where({
      id: groupId,
    });

    if (userGroup.length > 0) {
      result.group = userGroup[0];
      //############ Get selected Modules on mst_usergroupacl

      const modules = await pool
        .select(
          "mm.id",
          "mm.modulename",
          "ug.idgroup",
          "ug.idtenant",
          "ug.idmodule"
        )
        .from("mst_usergroupacl as ug")
        .innerJoin("mst_module as mm", "ug.idmodule", "mm.id")
        .where("ug.idgroup", groupId)
        .groupBy("mm.id")
        .groupBy("ug.idgroup")
        .groupBy("ug.idtenant")
        .groupBy("ug.idmodule")
        .groupBy("mm.modulename");

      // const modules = await pool
      //         .select(
      //           "mm.modulename",
      //           "idgroup",
      //           "mm.idtenant",
      //           "idmodule as id",
      //           "idmenu",
      //           "fcreate",
      //           "fread",
      //           "fupdate",
      //           "fdelete",
      //           "fview",
      //           "fapproval",
      //           //"uga.created_byid",
      //           "uga.created_at",
      //           "uga.updated_at"
      //         )
      //         .from("mst_usergroupacl as uga")
      //         .innerJoin("mst_moduleby_tenant as mm", "uga.idmodule", "mm.id")
      //         .where("uga.idgroup", groupId)
      //         .orWhere("uga.idmenu", 0)
      //         .orWhereNull("uga.idmenu");
      console.log("modules : ", modules);
      if (modules.length > 0) {
        result.modules = modules;
        const menus = await pool
          .select("mm.title", "mm.id  as menuId", "uga.*")
          .from("mst_usergroupacl as uga")
          .innerJoin("mst_menu as mm", "uga.idmenu", "mm.id")
          .where("uga.idgroup", groupId)
          .whereNotNull("uga.idmenu");

        result.menus = menus;
        console.log("############# RESULT " + menus.length);
      }
      console.log(result);
      if (result.menus.length > 0) {
        await result.menus.map((menu) => {
          menu.acl = {
            read: menu.fread,
            create: menu.fcreate,
            update: menu.fupdate,
            delete: menu.fdelete,
            approval: menu.fapproval,
          };
        });
        res.status(200).json({ status: 200, data: { result } });
      }
    } else {
      res.status(200).json({ status: 200, data: "Data tidak ditemukan" });
    }
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

//Edit Group
router.post("/editGroup", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];
  try {
    const { group, modules, menus } = req.body;

    if(apps.withapproval > 0) {
      const insertApprovalTmp = await pool("trx_approvaltmp")
      .returning([
        "id",
        "idapp",
        "idtenant",
        "cdmenu",
        "jsondata",
        "status",
        "typedata"
      ])
      .insert({
        idapp:apps.idapp,
        idtenant:dcodeInfo.idtenant,
        cdmenu:"GRPMN",
        jsondata: JSON.stringify(req.body),
        status:0,
        typedata:7
      });
      console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ TEST EDIT GROUP");
      console.log(group);
      if (insertApprovalTmp.length > 0) {
        var userObj = insertApprovalTmp[0];
        let idreturn = userObj.id;
        const resp = await pool("mst_usergroup")
          .returning(["id", "active", "updated_at"])
          .where("id", group.groupId)
          .update({
            updated_at: pool.fn.now(),
            active: 7,
            idapproval: idreturn
          });
          if (resp.length > 0) {
            var userObj = resp[0];
            res.status(200).json({ status: 200, data: userObj });
          }
      }
    } else {
        let idTenant = dcodeInfo.idtenant;
        let editGroup = await groupUtils.editGroup(group, apps, idTenant);
        let editModules = await groupUtils.editModules(
          modules,
          group.groupId,
          apps,
          idTenant,
          dcodeInfo.leveltenant
        );
        let editMenus = await groupUtils.editMenus(
          menus,
          group.groupId,
          apps,
          idTenant,
          dcodeInfo.leveltenant
        );
        if (editGroup && editModules === 1 && editMenus === 1) {
          res.status(200).json({ status: 200, data: "" });
        } else {
          res.status(422).json({ status: 422, data: "Unprocessable data" });
        }
    }




    
  } catch (err) {
    console.log(err);
    next(err);
    res.status(422).json({ status: 422, data: "Unprocessable data" });
  }
});


router.post("/editGroupActive", async (req, res, next) => {
  // const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var toknid = req.tokenID;
  var apps = dcodeInfo.apps[0];
  try {
    const { id, oldactive, isactive, idapproval} = req.body;
    let now = new Date();
    now.setHours(now.getHours() + 7);

    switch(oldactive) {
      case 3:
        // code block
        let idTenant = dcodeInfo.idtenant;
        let editGroup = await groupUtils.editGroupActive(id, isactive);
        if (editGroup) {
          res.status(200).json({ status: 200, data: "" });
        } else {
          res.status(422).json({ status: 422, data: "Unprocessable data" });
        }
     
        
        break;
      case 5, 7:
        console.log(">>>>>>>>>>>>>>>>>>>>>>>.JSON SSSSSSSSSSSSSSSSSSSSS");
        let respAppTmp = await pool.select("jsondata")
        .from("trx_approvaltmp")
        .where({"id": idapproval, "status": 0})
        if(respAppTmp.length > 0){
          let jsdata = respAppTmp[0];
          let jdata = JSON.parse(jsdata.jsondata);
          console.log(jdata);
          
          let idTenant = dcodeInfo.idtenant;
          let editGroup = await groupUtils.editGroup(jdata.group, apps, idTenant);
          let editModules = await groupUtils.editModules(
            jdata.modules,
            jdata.group.groupId,
            apps,
            idTenant,
            dcodeInfo.leveltenant
          );
          let editMenus = await groupUtils.editMenus(
            jdata.menus,
            jdata.group.groupId,
            apps,
            idTenant,
            dcodeInfo.leveltenant
          );
          if (editGroup && editModules === 1 && editMenus === 1) {
            res.status(200).json({ status: 200, data: "" });
          } else {
            res.status(422).json({ status: 422, data: "Unprocessable data" });
          }
        }
        break;
      case 9:
        console.log(">>>>>>>>>>>>>>>>>>>>>>>.JSON SSSSSSSSSSSSSSSSSSSSS");
        let respDel;
        let respAppDelTmp = await pool.select("jsondata")
        .from("trx_approvaltmp")
        .where({"id": idapproval, "status": 0});
        if(respAppDelTmp.length > 0){
          let jsdata = respAppDelTmp[0];
          let groupObj = JSON.parse(jsdata.jsondata);
          // console.log(jsdata);
          let deleteData = await groupUtils.deleteGroup(groupObj, apps);console.log(deleteData);
          if (deleteData === 1) {
            res.status(200).json({ status: 200, data: "" });
          } else {
            res.status(422).json({ status: 422, data: "Unprocessable data" });
          }




        }
        
        break;
        default:
          // code block
      }




    
    
  } catch (err) {
    console.log(err);
    next(err);
    res.status(422).json({ status: 422, data: "Unprocessable data" });
  }
});



//Delete Group
router.post("/deleteGroup", async (req, res, next) => {
  // const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];
  try {
    const { group, modules, menus } = req.body;

    let resp;
    if(apps.withapproval > 0) {
      const insertApprovalTmp = await pool("trx_approvaltmp")
      .returning([
        "id",
        "idapp",
        "idtenant",
        "cdmenu",
        "jsondata",
        "status",
        "typedata"
      ])
      .insert({
        idapp:apps.idapp,
        idtenant:dcodeInfo.idtenant,
        cdmenu:"GRPMN",
        jsondata: JSON.stringify(req.body),
        status:0,
        typedata:9
      });
      if (insertApprovalTmp.length > 0) {
        var userObj = insertApprovalTmp[0];
        let idreturn = userObj.id;
        console.log("LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL");
        // console.log(idreturn);
        // console.log(userObj.jsondata);
        let jdata = JSON.parse(userObj.jsondata)
        console.log(jdata.group.id);
        console.log("LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL");
        const resp = await pool("mst_usergroup")
          .returning(["id", "updated_at"])
          .where("id", jdata.group.id)
          .update({
            active: 9,
            idapproval: idreturn
          });
          if (resp.length > 0) {
            var userObj = resp[0];
            res.status(200).json({ status: 200, data: userObj });
          }
      }
    }else{
       let deleteData = await groupUtils.deleteGroup(req.body, apps);console.log(deleteData);
        if (deleteData === 1) {
          res.status(200).json({ status: 200, data: "" });
        } else {
          res.status(422).json({ status: 422, data: "Unprocessable data" });
        }
    }




   
  } catch (err) {
    //console.log(err);
    next(err);
    res.status(422).json({ status: 422, data: "Unprocessable data" });
  }
});

module.exports = router;

// router.get("/getAllGroup", async (req, res, next) => {
//   const authHeader = req.headers.authorization;
//   var dcodeInfo = null;
//   if (authHeader) {
//     TokenArray = authHeader.split(" ");
//     dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
//     // console.log(">>>>>>>>>>>> decoded "+JSON.stringify(dcodeInfo));
//     var apps = dcodeInfo.apps[0];
//     // console.log(">>>>>>>>>>>> decoded "+JSON.stringify(apps));
//     try {
//       // console.log(">>>>>>>>>>>> Tenanr, Level "+JSON.stringify(dcodeInfo.leveltenant));
//       console.log(dcodeInfo.leveltenant);
//       var query =
//         "Select * from public.mst_usergroup where idapplication=$1 and idtenant=$2";
//       // var idtype = dcodeInfo.leveltenant;

//       // console.log(">>>>>>>>>>>> APPLICATION, Level "+(idtype != "0") );

//       // if(idtype != "0" ) {
//       // query =
//       //   "Select * from public.mst_usergroup where idapplication=$1 and idtenant=$2 and grouptype=$3 or grouptype = $4";
//       query =
//         "Select * from public.mst_usergroup where idapplication=$1 and idtenant=$2 and grouptype >= $3";
//       // };
//       // console.log(">>>>>>>>>>>> Query, Level "+(dcodeInfo.leveltenant == "0") );
//       // console.log(">>>>>>>>>>>> IDApp "+apps.id_application );
//       console.log(query);
//       console.log(apps.id, dcodeInfo.idtenant, 1, dcodeInfo.leveltenant);
//       const userGroup = await pool.query(
//         query,
//         dcodeInfo.leveltenant == "0"
//           ? [apps.id, dcodeInfo.idtenant, 0]
//           : [apps.id, dcodeInfo.idtenant, dcodeInfo.leveltenant]
//       );
//       // const userGroup = await pool.query(
//       //   "Select * from public.mst_usergroup",
//       //   []
//       // );
//       if (userGroup.rows.length > 0) {
//         res
//           .status(200)
//           .json({ status: 200, data: { userGroup: userGroup.rows } });
//       } else {
//         res.status(200).json({ status: 200, data: "Data tidak ditemukan" });
//       }
//     } catch (err) {
//       console.log(err);
//       res.status(500).json({ status: 500, data: "internal error" });
//     }
//   } else {
//     console.log("no header");
//     res.sendStatus(403);
//   }
// });

// router.get("/getallmodulewithmenu", async (req, res, next) => {
//   const authHeader = req.headers.authorization;
//   var dcodeInfo = null;
//   if (authHeader) {
//     TokenArray = authHeader.split(" ");
//     dcodeInfo = await jwttools.decryptdata(TokenArray[1]);

//     var apps = dcodeInfo.apps[0];
//     try {
//       const modules = await pool.query(
//         "Select * from public.mst_moduleby_tenant ",
//         []
//       );
//       if (modules.rows.length > 0) {
//         let result = [];
//         try {
//           let modulesList = modules.rows;

//           await Promise.all(
//             modulesList.map(async (module) => {
//               let moduleDetail = {};
//               moduleDetail.moduleId = module.id;
//               moduleDetail.moduleName = module.modulename;
//               let menus = await pool.query(
//                 "select * from public.mst_menu where idmodule = $1",
//                 [module.id]
//               );
//               if (menus.rows.length > 0) {
//                 let menuRows = menus.rows;
//                 let menuList = [];
//                 let menuDetail = {};
//                 await Promise.all(
//                   menuRows.map((menu) => {
//                     menuDetail = {};
//                     menuDetail.menuName = menu.title;
//                     menuDetail.menuId = menu.id;
//                     menuList.push(menuDetail);
//                   })
//                 );

//                 moduleDetail.moduleMenus = menuList;
//               }
//               result.push(moduleDetail);
//             })
//           );

//           res.status(200).json({ status: 200, data: result });
//         } catch (error) {
//           console.log(err);
//         }
//       } else {
//         res.status(200).json({ status: 200, data: "Data tidak ditemukan" });
//       }
//     } catch (err) {
//       console.log(err);
//       res.status(500).json({ status: 500, data: "internal error" });
//     }
//   } else {
//     console.log("no header");
//     res.sendStatus(403);
//   }
// });

// router.post("/regisGroup", async (req, res, next) => {
//   const authHeader = req.headers.authorization;
//   var dcodeInfo = null;
//   if (authHeader) {
//     TokenArray = authHeader.split(" ");
//     dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
//     var apps = dcodeInfo.apps[0];
//     let idTenant = dcodeInfo.idtenant;
//     try {
//       // const { group, modules, menus } = req.body;
//       let insertData = await groupUtils.insertGroup(
//         req.body,
//         apps,
//         idTenant,
//         dcodeInfo.leveltenant
//       );

//       if (insertData === 1) {
//         res.status(200).json({ status: 200, data: "" });
//       } else {
//         res.status(422).json({ status: 422, data: "Unprocessable data" });
//       }
//     } catch (err) {
//       console.log(err);
//       res.status(422).json({ status: 422, data: "Unprocessable data" });
//     }
//   } else {
//     console.log("no header");
//     res.sendStatus(403);
//   }
// });

// router.post("/regisGroup", async (req, res, next) => {
//   const authHeader = req.headers.authorization;
//   var dcodeInfo = null;
//   if (authHeader) {
//     TokenArray = authHeader.split(" ");
//     dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
//     var apps = dcodeInfo.apps[0];
//     let idTenant = dcodeInfo.idtenant;
//     try {
//       // const { group, modules, menus } = req.body;
//       let insertData = await groupUtils.insertGroup(
//         req.body,
//         apps,
//         idTenant,
//         dcodeInfo.leveltenant
//       );

//       if (insertData === 1) {
//         res.status(200).json({ status: 200, data: "" });
//       } else {
//         res.status(422).json({ status: 422, data: "Unprocessable data" });
//       }
//     } catch (err) {
//       console.log(err);
//       res.status(422).json({ status: 422, data: "Unprocessable data" });
//     }
//   } else {
//     console.log("no header");
//     res.sendStatus(403);
//   }
// });

// router.get("/getDetailsGroup/:id", async (req, res, next) => {
//   const authHeader = req.headers.authorization;
//   var dcodeInfo = null;
//   if (authHeader) {
//     TokenArray = authHeader.split(" ");
//     dcodeInfo = await jwttools.decryptdata(TokenArray[1]);

//     var apps = dcodeInfo.apps[0];
//     try {
//       let groupId = req.params.id;
//       let result = {};
//       const userGroup = await pool.query(
//         "Select * from public.mst_usergroup where id = $1",
//         [groupId]
//       );
//       if (userGroup.rows.length > 0) {
//         result.group = userGroup.rows[0];
//         const modules = await pool.query(
//           "select mm.modulename ,idgroup, mm.idtenant, idmodule as id, idmenu, fcreate, fread, fupdate, fdelete, fview, fapproval, uga.created_byid, uga.created_at, uga.updated_at from public.mst_usergroupacl uga inner join  public.mst_moduleby_tenant mm on uga.idmodule = mm.id where uga.idgroup = $1 and uga.idmenu is null ; ",
//           [groupId]
//         );
//         if (modules.rows.length > 0) {
//           result.modules = modules.rows;
//           const menus = await pool.query(
//             `select mm.title,mm.id  as "menuId",uga.* from public.mst_usergroupacl uga inner join  public.mst_menu mm on uga.idmenu = mm.id  where uga.idgroup = $1 and uga.idmenu is not null`,
//             [groupId]
//           );
//           result.menus = menus.rows;
//         }
//         if (result.menus.length > 0) {
//           await result.menus.map((menu) => {
//             menu.acl = {
//               read: menu.fread,
//               create: menu.fcreate,
//               update: menu.fupdate,
//               delete: menu.fdelete,
//             };
//           });
//           res.status(200).json({ status: 200, data: { result } });
//         }
//       } else {
//         res.status(200).json({ status: 200, data: "Data tidak ditemukan" });
//       }
//     } catch (err) {
//       console.log(err);
//       res.status(500).json({ status: 500, data: "internal error" });
//     }
//   } else {
//     console.log("no header");
//     res.sendStatus(403);
//   }
// });
// router.post("/editGroup", async (req, res, next) => {
//   const authHeader = req.headers.authorization;
//   var dcodeInfo = null;
//   if (authHeader) {
//     TokenArray = authHeader.split(" ");
//     dcodeInfo = await jwttools.decryptdata(TokenArray[1]);

//     var apps = dcodeInfo.apps[0];
//     try {
//       const { group, modules, menus } = req.body;
//       let idTenant = dcodeInfo.idtenant;
//       let editGroup = await groupUtils.editGroup(group, apps, idTenant);

//       let editModules = await groupUtils.editModules(
//         modules,
//         group.groupId,
//         apps,
//         idTenant,
//         dcodeInfo.leveltenant
//       );
//       let editMenus = await groupUtils.editMenus(
//         menus,
//         group.groupId,
//         apps,
//         idTenant,
//         dcodeInfo.leveltenant
//       );
//       if (editGroup && editModules === 1 && editMenus === 1) {
//         res.status(200).json({ status: 200, data: "" });
//       } else {
//         res.status(422).json({ status: 422, data: "Unprocessable data" });
//       }
//     } catch (err) {
//       console.log(err);
//       res.status(422).json({ status: 422, data: "Unprocessable data" });
//     }
//   } else {
//     console.log("no header");
//     res.sendStatus(403);
//   }
// });

// router.post("/deleteGroup", async (req, res, next) => {
//   const authHeader = req.headers.authorization;
//   var dcodeInfo = null;
//   if (authHeader) {
//     TokenArray = authHeader.split(" ");
//     dcodeInfo = await jwttools.decryptdata(TokenArray[1]);

//     var apps = dcodeInfo.apps[0];
//     try {
//       const { group, modules, menus } = req.body;
//       let deleteData = await groupUtils.deleteGroup(req.body, apps);
//       console.log(deleteData);
//       if (deleteData === 1) {
//         res.status(200).json({ status: 200, data: "" });
//       } else {
//         res.status(422).json({ status: 422, data: "Unprocessable data" });
//       }
//     } catch (err) {
//       console.log(err);
//       res.status(422).json({ status: 422, data: "Unprocessable data" });
//     }
//   } else {
//     console.log("no header");
//     res.sendStatus(403);
//   }
// });
