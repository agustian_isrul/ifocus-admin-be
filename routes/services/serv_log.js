const express = require("express");
const router = express.Router();
const logs = require("./utils/logger");
const eventLog = require("./utils/eventLog");
const pool = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");

router.post("", async (req, res, next) => {
  // type : 1 info,2error
  const { type, description, action } = req.body;

  var dcodeInfo = req.userData;
  var tokenID = req.tokenID;
  const app = dcodeInfo.apps[0].appname
    ? dcodeInfo.apps[0].appname
    : "Unkwon App";
  const fileLog = logs.logging(
    dcodeInfo.apps[0].appname ? dcodeInfo.apps[0].appname : "krakatoa-logs"
  );

  switch (type) {
    case 1:
      fileLog.info(`App: ${app} , event: ${description}`);

      break;
    case 2:
      fileLog.error(`App: ${app}, event: ${description}`);

      break;
    default:
      fileLog.info(`App: ${app} , event: ${description}`);

      break;
  }
  res.status(200).json({ status: 200, data: "log saved" });
});
router.use(checkAuth);

router.get("/getAll", async (req, res, next) => {
  try {
    var dcodeInfo = req.userData;
    // const query =
    //   "Select tel.*, tu.userid,TO_CHAR(tel.created_at,'yyyy-MM-dd HH:mm:ss') formatedDate from public.trx_eventlog tel inner join public.cdid_tenant_user tu on tel.created_by = tu.id where idapp = $1 order by created_at desc";
    // const result = await pool.query(query, [dcodeInfo.apps[0].idapp]);
    let now = new Date();
    let oneMonthBefore = new Date();
    oneMonthBefore = oneMonthBefore.setMonth(oneMonthBefore.getMonth() - 1);
    oneMonthBefore = new Date(oneMonthBefore);
    console.log(now.toISOString());
    const result = await pool
      .select(
        "tu.userid",
        "tel.value",
        "tel.description",
        "tel.created_at as formateddate"
      )
      .from("trx_eventlog as tel")
      .innerJoin("cdid_tenant_user as tu", "tel.created_by", "tu.id")
      .where("tel.idapp", dcodeInfo.apps[0].idapp)
      //.andWhere("tel.created_at", "<=", now.toISOString())
      .andWhere("tel.created_at", ">=", oneMonthBefore.toISOString())
      .orderBy("tel.created_at", "desc");
    //console.log(dcodeInfo.apps[0].idapp);
    res.status(200).json({ status: 200, data: result });
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.post("/getWithFilter", async (req, res, next) => {
  try {
    var dcodeInfo = req.userData;
    const { userId, startDate, endDate } = req.body;
    console.log(req.body);
    let startDateFormated = new Date(startDate);
    startDateFormated = startDateFormated.setDate(
      startDateFormated.getDate() + 1
    );
    startDateFormated = new Date(startDateFormated);
    let endDateFormated = new Date(endDate);
    endDateFormated = endDateFormated.setDate(endDateFormated.getDate() + 1);
    endDateFormated = new Date(endDateFormated);
    const result = await pool
      .select(
        "tel.value",
        "tel.description",
        "tel.cdaction",
        "tel.refevent",
        "tu.userid",
        "tel.created_at as formateddate"
      )
      .from("trx_eventlog as tel")
      .innerJoin("cdid_tenant_user as tu", "tel.created_by", "tu.id")
      .where("tel.idapp", dcodeInfo.apps[0].idapp)
      .where((builder) => {
        if (userId) {
          builder.andWhere("tu.userid", userId);
        }
        if (startDate) {
          builder.andWhere(
            "tel.created_at",
            ">=",
            startDateFormated.toISOString()
          );
        }
        if (endDate) {
          builder.andWhere(
            "tel.created_at",
            "<=",
            endDateFormated.toISOString()
          );
        }
      })
      .orderBy("tel.created_at", "desc");
    res.status(200).json({ status: 200, data: result });
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.post("/insertlog", async (req, res, next) => {
  // type : 1 info,2error
  const {
    created_by,
    value,
    idapp,
    idmenu,
    description,
    cdaction,
    refevent,
    valuejson,
  } = req.body;

  await pool("trx_eventlog").insert({
    created_by,
    value,
    idapp,
    idmenu,
    description,
    cdaction,
    refevent,
    valuejson,
  });
  //fileLog.info(`Saving event to eventlog `);

  res.status(200).json({ status: 200, data: "log saved" });
});

router.post("/eventlog", async (req, res, next) => {
  // type : 1 info,2error
  const { userId, eventVal, idapp, cdaction, description } = req.body;

  var dcodeInfo = req.userData;
  var tokenID = req.tokenID;
  const app = dcodeInfo.apps[0].appname
    ? dcodeInfo.apps[0].appname
    : "Unkwon App";
  const fileLog = logs.logging(
    dcodeInfo.apps[0].appname ? dcodeInfo.apps[0].appname : "krakatoa-logs"
  );

  await pool("trx_eventlog").insert({
    created_by: userId ? userId : 0,
    value: eventVal ? eventVal : 0,
    idapp: idapp ? idapp : 0,
    idmenu: 0,
    description: description ? description : 0,
    cdaction: cdaction ? cdaction : 0,
    refevent: req.tokenID ? req.tokenID : 0,
    valuejson: req.body ? JSON.stringify(req.body) : null,
  });
  fileLog.info(`Saving event to eventlog `);

  res.status(200).json({ status: 200, data: "log saved" });
});

router.post("/getapprovaleditdata", async (req, res, next) => {
  console.log(">>>>> GET APPROVAL DATA");
  try {
    const { iddata } = req.body;
    console.log("iddata", iddata);
    let respAppTmp = await pool.select("jsondata")
        .from("trx_approvaltmp")
        .where({"id": iddata, "status": 0})
          if (respAppTmp.length > 0) {
            console.log(respAppTmp);
            res.status(200).json({status: 200, data:respAppTmp});
          }
  } catch (error) {
    next(error);
    res.status(500).json({
      status: 500,
      data: "No data approval found",
    });
  }
});

router.post("/setapprovaldata", async (req, res, next) => {
  console.log(">>>>> ADD APPROVAL DATA");
  try {
    const { idapp, idtenant, cdmenu, jsondata, status, typedata } = req.body;
    // console.log("iddata", iddata);
    const insertApprovalTmp = await pool("trx_approvaltmp")
      .returning([
        "id",
        "idapp",
        "idtenant",
        "cdmenu",
        "jsondata",
        "status",
        "typedata"
      ])
      .insert({
        idapp:idapp,
        idtenant:idtenant,
        cdmenu:cdmenu,
        jsondata: jsondata,
        status:status,
        typedata:typedata
      });
    if (insertApprovalTmp.length > 0) {
      res.status(200).json({status: 200, data:insertApprovalTmp});
    }
  } catch (error) {
    next(error);
    res.status(500).json({
      status: 500,
      data: "No data tenants found",
    });
  }
});






module.exports = router;
