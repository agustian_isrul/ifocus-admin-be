var express = require("express");
const sysParam = require("./utils/user_management/getSysParam");
const router = express.Router();
const pool = require("../../connection/db");
var jwt = require("jsonwebtoken");
const notifikasi = require("./serv_notifikasi.js");
let neParam;

const getParam = async () => {
  neParam = await sysParam.getNEParam();
};

router.post("/sendemailnotification", async (req, res, next) => {
  console.log(">>>>>Send email notification");
  let header = req.headers.host;
  let kodestatus = req.params.id;
  var resultUser = [];

  try {
    const { kodestatus, message } = req.body;
    console.log("kodestatus", kodestatus);

    // const resp = await pool.query(
    //   `SELECT id, category, label, valuestr, valueint, created_at, updated_at FROM public.mst_bussines_param
    //         WHERE category = 'NOTIFY_EMAIL_TYPE' and label = $1`, // ke admi
    //   [kodestatus]
    // );
    const resp = await pool
      .select(
        "id",
        "category",
        "label",
        "valuestr",
        "valueint",
        "created_at",
        "updated_at"
      )
      .from("mst_bussines_param")
      .where("category", "NOTIFY_EMAIL_TYPE")
      .andWhere("label", kodestatus);

    if (resp.length >= 1) {
      //   const emailto = await pool.query(
      //     `SELECT ca.id, ca.iduser, ctu.userid, ca.cd_busparam, ca.idtenant, ca.idapp, ca.created_byid, ca.created_at, ca.updated_at
      //             FROM public.cdid_adminnotification ca
      // 			INNER JOIN public.cdid_tenant_user ctu
      // 			ON ca.iduser = ctu.id WHERE cd_busparam = $1
      // 	        `, // ke admi
      //     [kodestatus]
      //   );
      const emailto = await pool
        .select(
          "ca.id",
          "ca.iduser",
          "ctu.userid",
          "ca.cd_busparam",
          "ca.idtenant",
          "ca.idapp",
          "ca.email_user",
          "ca.created_byid",
          "ca.created_at",
          "ca.updated_at"
        )
        .from("cdid_adminnotification as ca")
        .innerJoin("cdid_tenant_user as ctu", "ca.iduser", "ctu.id")
        .where("cd_busparam", kodestatus);

      for (var i = 0; i < emailto.length; i++) {
        // resultUser[i] = emailto[i].userid;
        resultUser[i] = emailto[i].email_user;
      }

      var row = resultUser.length;
      if (emailto.length >= 1) {
        res.status(200).json({
          status: 200,
        });
        // var notif = await notifikasi.sendMailNotifikasi(resultUser, info => {
        //     console.log(`The mail has beed send `);
        // });
        var notif = await notifikasi.sendMailMessage(
          resultUser,
          message,
          (info) => {
            console.log(`The mail has beed send `);
          }
        );
      }
    }
  } catch (error) {
    res.status(500).json({
      status: 500,
      data: "No data tenants found",
    });
  }
});

router.post("/sendnotification", async (req, res, next) => {
  console.log(">>>>>Send notification");
  let header = req.headers.host;
  let kodestatus = req.params.id;
  var resultUser = [];

  try {
    const { message, emailUser } = req.body;
    console.log("emailUser", emailUser);
    res.status(200).json({
      status: 200,
    });

    var notif = await notifikasi.sendlNotifikasi(emailUser, message, (info) => {
      console.log(`The mail has beed send `);
    });
  } catch (error) {
    res.status(500).json({
      status: 500,
      data: "No data tenants found",
    });
  }
});

module.exports = router;
