var express = require("express");
const router = express.Router();
const crypto = require("crypto");
const nodemailer = require("nodemailer");
const detail = require("../models/configPassword.json");
const pool = require("../../connection/db");
var jwt = require("jsonwebtoken");
const notifikasi = require("./serv_notifikasi.js");
const { info } = require("console");
const passEnc = require("./utils/password/encryptionPass");

// validate
const validatePass = require("./utils/user_management/userValidation");
const sysParam = require("./utils/user_management/getSysParam");
let umParam;
let neParam;

const getParam = async () => {
  umParam = await sysParam.getUMParam();
  neParam = await sysParam.getNEParam();
};
//

// ecript
var key = "supersecretkey";
function encrypt(key, data) {
  var cipher = crypto.createCipher("aes-256-cbc", key);
  var crypted = cipher.update(data, "utf-8", "hex");
  crypted += cipher.final("hex");

  return crypted;
}

function decrypt(key, data) {
  var decipher = crypto.createDecipher("aes-256-cbc", key);
  var decrypted = decipher.update(data, "hex", "utf-8");
  decrypted += decipher.final("utf-8");

  return decrypted;
}
// end

router.post("/forgotpassword", async (req, res, next) => {
  console.log(">>>>>Forgot Password");
  let header = req.headers.host;
  let user = req.body;
  var reqjwt = jwt.sign(
    {
      exp: Math.floor(Date.now() / 1000) + 60 * 60,
      email: user.email,
      note: "forgot",
    },
    "secret"
  );
  console.log("JWT::", reqjwt);

  var token = encrypt(key, reqjwt);
  // console.log("Encrypted Text: " + token);
  // var decryptedText = decrypt(key, token);
  // console.log("Decrypted Text: " + decryptedText);

  try {
    const { email } = req.body;

    const resp = await pool
      .select("*")
      .from("mst_biodata_corell")
      .where("bioemailactive", email);

    if (resp.length >= 1) {
      res.status(200).json({
        status: 200,
      });

      var notif = await notifikasi.sendMail(token, user, (info) => {
        console.log(`The mail has beed send `);
      });
    } else {
      console.log("DATA TIDAK ADA");
      res.status(200).json({
        status: 201,
        data: "Data tenants found",
      });
    }
  } catch (error) {
    res.status(500).json({
      status: 500,
      data: "No data tenants found",
    });
  }
});

router.post("/resetpassword/:id", async (req, res, next) => {
  console.log(">>>>>Reset Password");
  let token = req.params.id;
  var decryptedText = decrypt(key, token);
  var decoded = jwt.decode(decryptedText);
  let emailid = decoded.email;
  let password = req.body.password;
  const exp = decoded.exp;

  // console.log("decryptedText::", decryptedText);
  // console.log("decoded::", decoded);

  try {
    await getParam();

    if (Date.now() >= exp * neParam.EMAIL_EXP) {
      res.status(200).json({
        status: 201,
        data: "exptoken",
      });
    } else {
      console.log("MASUK RESETPASSWORD");

      const check = await pool
        .select("*")
        .from("mst_biodata_corell")
        .where("bioemailactive", emailid);

      let id = check[0].biocorelobjid;

      const getOldPassword = await pool
        .select("*")
        .from("pass_hist")
        .where("user_id", id)
        .orderByRaw("id limit ?", [umParam.PASSWORD_HISTORY]);

      let oldPasswordList = [];
      if (getOldPassword.length > 0) {
        getOldPassword.map((dt) => {
          oldPasswordList.push(dt.pwd);
        });
      }
      let validPass = await validatePass.validatePassword(
        password,
        oldPasswordList
      );
      console.log(validPass);
      if (validPass) {
        let pass = await passEnc.encryptPass(password);
        const insertPasswordHist = await pool
          .insert({
            user_id: id,
            pwd: pass,
          })
          .into("pass_hist")
          .returning(["id"]);

        const resp = await pool("cdid_tenant_user")
          .returning(["id", "userid"])
          .where("id", id)
          .update({ active: 1, pwd: pass });

        res.status(200).json({ status: 200, data: resp.length });
      } else {
        res.status(200).json({ status: 422, data: "Password not valid" });
      }
    }
  } catch (err) {
    next(err);

    res.status(500).json({
      status: 500,
      data: "Cannot Update",
    });
  }
});

router.post("/verifikasi", async (req, res, next) => {
  console.log(">>>>>verifikasi");
  let header = req.headers.host;
  let user = req.body;
  var reqjwt = jwt.sign(
    {
      exp: Math.floor(Date.now() / 1000) + 60 * 60,
      email: user.email,
      note: "verifikasi",
    },
    "secret"
  );
  var token = encrypt(key, reqjwt);
  try {
    const { email } = req.body;

    res.status(200).json({
      status: 200,
    });

    var notif = await notifikasi.sendMailVerifikasi(token, user, (info) => {
      console.log(`The mail has beed send `);
    });
  } catch (error) {
    res.status(500).json({
      status: 500,
      data: "No data tenants found",
    });
  }
});

router.post("/verifikasiemail/:id", async (req, res, next) => {
  console.log(">>>>>Verifikasi");
  let token = req.params.id;
  var decryptedText = decrypt(key, token);
  var decoded = jwt.decode(decryptedText);
  let emailid = decoded.email;
  let password = req.body.password;
  const exp = decoded.exp;

  try {
    await getParam();

    if (Date.now() >= exp * neParam.EMAIL_EXP) {
      res.status(200).json({
        status: 201,
        data: "exptoken",
      });
    } else {
      console.log("EMAIL SUDAH TERVERIFIKASI");

      const check = await pool
        .select("*")
        .from("mst_biodata_corell")
        .where("bioemailactive", emailid);

      let id = check[0].biocorelobjid;

      const getOldPassword = await pool
        .select("*")
        .from("pass_hist")
        .where("user_id", id)
        .orderByRaw("id limit ?", [umParam.PASSWORD_HISTORY]);

      let oldPasswordList = [];
      if (getOldPassword.length > 0) {
        getOldPassword.map((dt) => {
          oldPasswordList.push(dt.pwd);
        });
      }
      let validPass = await validatePass.validatePassword(
        password,
        oldPasswordList
      );
      console.log(validPass);
      if (validPass) {
        let pass = await passEnc.encryptPass(password);
        const insertPasswordHist = await pool
          .insert({
            user_id: id,
            pwd: pass,
          })
          .into("pass_hist")
          .returning(["id"]);

        const resp = await pool("cdid_tenant_user")
          .returning(["id", "userid"])
          .where("id", id)
          .update({ active: 1, pwd: pass });

        res.status(200).json({ status: 200, data: resp.length });
      } else {
        res.status(200).json({ status: 422, data: "Password not valid" });
      }
    }
  } catch (err) {
    next(err);

    res.status(500).json({
      status: 500,
      data: "Cannot Verifikasi Email",
    });
  }
});

module.exports = router;
