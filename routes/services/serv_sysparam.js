const express = require("express"),
  app = express();
const router = express.Router();
const pool = require("../../connection/db");
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const logger = require("../../routes/services/utils/logservices");
const moment = require("moment");
const checkAuth = require("../../middleware/check-auth");

const eventLog = require("../../middleware/eventLog");
router.use(checkAuth);
router.use(eventLog);

router.get("/getAll", async (req, res, next) => {
  try {
    const dcodeInfo = req.userData;

    // console.log(">>>> GET TENANTS >>>> " + dcodeInfo.leveltenant);
    /*
    var query =
      "SELECT * from public.system_param where category = 'USER_MANAGEMENT' order by id";
    const resp = await pool.query(query, []);
    */

    const resp = await pool
      .select()
      .table("system_param")
      .where({ category: "USER_MANAGEMENT" })
      .orderBy("id");

    var jsonbody = resp;

    res.status(200).json({ status: 200, data: jsonbody });
  } catch (err) {
    next(err);

    res.status(500).json({ status: 500, data: "Error get data" });
  }
});

router.get("/getById/:id", async (req, res, next) => {
  try {
    let id = req.params.id;
    const dcodeInfo = req.userData;

    // console.log(">>>> GET TENANTS >>>> " + dcodeInfo.leveltenant);
    /*
    var query = "SELECT * from public.system_param where id = $1";
    const resp = await pool.query(query, [id]);
    */
    const resp = await pool.select().table("system_param").where({ id: id });

    var jsonbody = resp[0];

    res.status(200).json({ status: 200, data: jsonbody });
  } catch (err) {
    next(err);

    res.status(500).json({ status: 500, data: "Error get data" });
  }
});

router.post("/update", async (req, res, next) => {
  try {
    let { id, value, description } = req.body;
    var dcodeInfo = req.userData;
    // var toknid = req.tokenID;
    var apps = dcodeInfo.apps[0];

    if (apps.withapproval > 0) {
      const insertApprovalTmp = await pool("trx_approvaltmp")
        .returning([
          "id",
          "idapp",
          "idtenant",
          "cdmenu",
          "jsondata",
          "status",
          "typedata",
        ])
        .insert({
          idapp: apps.idapp,
          idtenant: dcodeInfo.idtenant,
          cdmenu: "SYSPRM",
          jsondata: JSON.stringify(req.body),
          status: 0,
          typedata: 5,
        });
      if (insertApprovalTmp.length > 0) {
        var userObj = insertApprovalTmp[0];
        let idreturn = userObj.id;
        const resp = await pool("system_param")
          .returning(["id", "param_name", "updated_at"])
          .where("id", id)
          .update({
            updated_at: pool.fn.now(),
            active: 7,
            idapproval: idreturn,
          });
        if (resp.length > 0) {
          var userObj = resp[0];
          res.status(200).json({ status: 200, data: userObj });
        }
      }
    } else {
      await pool("system_param").where({ id: id }).update({
        param_value: value,
        description: description,
      });
      res.status(200).json({ status: 200, data: "Update Success" });
    }
  } catch (err) {
    console.log(err);
    next(err);

    res.status(500).json({ status: 500, data: "Error Update" });
  }
});

router.post("/updatebyadminactive", async (req, res, next) => {
  var dcodeInfo = req.userData;
  var toknid = req.tokenID;
  var apps = dcodeInfo.apps[0];
  // var levetid = parseInt(dcodeInfo.leveltenant) + 1;
  var levetid = parseInt(dcodeInfo.leveltenant);
  if (levetid == 0) {
    levetid = levetid + 1;
  }
  try {
    const { id, oldactive, isactive, idapproval } = req.body;
    let now = new Date();
    now.setHours(now.getHours() + 7);
    switch (oldactive) {
      case 3:
        // code block
        break;
      case (5, 7):
        console.log(">>>>>>>>>>>>>>>>>>>>>>>.JSON SSSSSSSSSSSSSSSSSSSSS");
        let respAppTmp = await pool
          .select("jsondata")
          .from("trx_approvaltmp")
          .where({ id: idapproval, status: 0 });
        if (respAppTmp.length > 0) {
          let jsdata = respAppTmp[0];
          let jdata = JSON.parse(jsdata.jsondata);
          // console.log(jdata);
          await pool("system_param").where({ id: id }).update({
            param_value: jdata.value,
            description: jdata.description,
            active: 1,
          });
          res.status(200).json({ status: 200, data: "Update Success" });
        }
        break;
      case 9:
        console.log(">>>>>>>>>>>>>>>>>>>>>>>.JSON SSSSSSSSSSSSSSSSSSSSS");
        let respDel;
        break;
      default:
      // code block
    }
  } catch (err) {
    console.log(err);
    next(err);

    res.status(500).json({ status: 500, data: "Error update User" });
  }
});

module.exports = router;
