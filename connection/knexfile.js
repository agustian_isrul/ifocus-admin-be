// Update with your config settings.

module.exports = {
  development: {
    client: "mysql",
    connection: {
      host: "localhost",
      port: 3306,
      user: "dexa_master",
      password: "dexaGroupw/MII2021",
      database: "kktadmorm",
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: "pgmigra",
    },
  },
};
