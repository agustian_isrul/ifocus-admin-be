exports.seed = function(knex) {
    // Deletes ALL existing entries
    return knex('mst_application')
        .del()
        .then(function() {
            // Inserts seed entries
            return knex('mst_application')
            .insert([
                {
                    appname:'default',
                    applabel:'Default',
                    description:'Krakatoa Administer',
                    apptype:0,
                    routelink:'/home'
                },
                {
                    appname:'komi',
                    applabel:'KOMI',
                    description:'This for DEMO',
                    apptype:1,
                    routelink:'http://localhost:4201/#'
                }
            ])
        })
}