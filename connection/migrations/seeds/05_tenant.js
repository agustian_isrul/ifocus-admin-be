const encPass = require("../../../routes/services/utils/uuidGenerator");
const moment = require("moment");
exports.seed = async function (knex) {
  // Deletes ALL existing entries

  const code = await encPass.getRandomWithCrypto(2);
  const tenantResult = await knex("cdid_tenant")
    .returning(["id", "tnname"])
    .insert([
      {
        tnname: "PT Bank BRI agro",
        tnstatus: 1,
        tntype: 1,
        cdidowner: 1,
        cdtenant: code.toUpperCase(),
      },
    ]);
  if (tenantResult.length > 0) {
    let idobj = tenantResult[0].id;
    const idapp = 2;
    const biocorel = await knex("mst_biodata_corell")
      .returning(["id", "bioname"])
      .insert([
        {
          bioname: "PT Bank BRI agro",
          bioidcorel: 2,
          biocorelobjid: idobj,
        },
      ]);
    if (biocorel.length > 0) {
      const insertmodulebytenant = await knex("mst_moduleby_tenant")
        .returning(["id", "modulename"])
        .insert([
          {
            modulename: "Users Management",
            created_byid: 0,
            status: 1,
            idapplication: idapp,
            idowner: 1,
            idtenant: idobj,
            modulecode: "BIFAST001",
          },
          {
            modulename: "Group & ACL Komi",
            created_byid: 0,
            status: 1,
            idapplication: idapp,
            idowner: 1,
            idtenant: idobj,
            modulecode: "BIFAST002",
          },
          {
            modulename: "Settings",
            created_byid: 0,
            status: 1,
            idapplication: idapp,
            idowner: 1,
            idtenant: idobj,
            modulecode: "BIFAST003",
          },
          {
            modulename: "Master",
            created_byid: 0,
            status: 1,
            idapplication: idapp,
            idowner: 1,
            idtenant: idobj,
            modulecode: "BIFAST004",
          },
          {
            modulename: "Rekonsiliasi",
            created_byid: 0,
            status: 1,
            idapplication: idapp,
            idowner: 1,
            idtenant: idobj,
            modulecode: "BIFAST005",
          },
          {
            modulename: "Monitoring",
            created_byid: 0,
            status: 1,
            idapplication: idapp,
            idowner: 1,
            idtenant: idobj,
            modulecode: "BIFAST006",
          },
          {
            modulename: "Reports",
            created_byid: 0,
            status: 1,
            idapplication: idapp,
            idowner: 1,
            idtenant: idobj,
            modulecode: "BIFAST007",
          },
        ]);
      console.log(">>>>>>>>>>> INPUT MByTenant " + insertmodulebytenant[0]);
      if (insertmodulebytenant.length > 0) {
        let expDate = moment().add(1, "years").format("YYYY-MM-DD");
        const insertLiscense = await knex("cdid_liscenses")
          .returning(["id", "expiredate"])
          .insert([
            {
              expiredate: expDate,
              cdid_tenant: idobj,
              id_application: 1,
              paidstatus: 1,
              active: 1,
            },
            {
              expiredate: expDate,
              cdid_tenant: idobj,
              id_application: 2,
              paidstatus: 1,
              active: 1,
            },
          ]);
        console.log(">>>>>>>>>>> License " + insertLiscense[0]);
        if (insertLiscense.length > 0) {
          const insertSysParam = await knex("system_param")
            .returning(["id", "param_name"])
            .insert([
              {
                param_name: "LOGIN_ATTEMPT",
                param_value: "3",
                category: "USER_MANAGEMENT",
                description: "Unknown",
                idtenant: 1,
                idapp: 2,
                typeparam: 0,
              },
              {
                param_name: "LOCK_DURATION",
                param_value: "30",
                category: "USER_MANAGEMENT",
                description: "Unknown",
                idtenant: 1,
                idapp: 2,
                typeparam: 0,
              },
              {
                param_name: "PASSWORD_MIN_LENGTH",
                param_value: "10",
                category: "USER_MANAGEMENT",
                description: "Unknown",
                idtenant: 1,
                idapp: 2,
                typeparam: 0,
              },
              {
                param_name: "PASSWORD_MAX_LENGTH",
                param_value: "30",
                category: "USER_MANAGEMENT",
                description: "Unknown",
                idtenant: 1,
                idapp: 2,
                typeparam: 0,
              },
              {
                param_name: "PASSWORD_UPPERCASE",
                param_value: "1",
                category: "USER_MANAGEMENT",
                description: "Unknown",
                idtenant: 1,
                idapp: 2,
                typeparam: 0,
              },
              {
                param_name: "PASSWORD_LOWERCASE",
                param_value: "1",
                category: "USER_MANAGEMENT",
                description: "Unknown",
                idtenant: 1,
                idapp: 2,
                typeparam: 0,
              },
              {
                param_name: "PASSWORD_CHARACTER",
                param_value: "3",
                category: "[-!$%^&*()_+|~=`{}[]:\";''<>?,.@#/]",
                description: "Unknown",
                idtenant: 1,
                idapp: 2,
                typeparam: 0,
              },
              {
                param_name: "PASSWORD_HISTORY",
                param_value: "5",
                category: "USER_MANAGEMENT",
                description: "Unknown",
                idtenant: 1,
                idapp: 2,
                typeparam: 0,
              },
              {
                param_name: "SESSION_IDLE",
                param_value: "30",
                category: "USER_MANAGEMENT",
                description: "Unknown",
                idtenant: 1,
                idapp: 2,
                typeparam: 0,
              },
              {
                param_name: "INACTIVE_USER_RANGE",
                param_value: "60",
                category: "USER_MANAGEMENT",
                description: "Unknown",
                idtenant: 1,
                idapp: 2,
                typeparam: 0,
              },
              {
                param_name: "INCATIVE_USER_SCHEDULLER_NAME",
                param_value: "inactiveUser",
                category: "USER_MANAGEMENT",
                description: "Unknown",
                idtenant: 1,
                idapp: 2,
                typeparam: 0,
              },
              {
                param_name: "INACTIVE_USER_CRON_SHCEDULLER",
                param_value: "* * * * *",
                category: "USER_MANAGEMENT",
                description: "Unknown",
                idtenant: 1,
                idapp: 2,
                typeparam: 0,
              },
              {
                param_name: "EMAIL_HOST",
                param_value: "smtp.gmail.com",
                category: "NOTIFIKASI_EMAIL",
                description: "Unknown",
                idtenant: 1,
                idapp: 2,
                typeparam: 1,
              },
              {
                param_name: "EMAIL_HOST",
                param_value: "smtp.gmail.com",
                category: "NOTIFIKASI_EMAIL",
                description: "Unknown",
                idtenant: 1,
                idapp: 2,
                typeparam: 1,
              },
              {
                param_name: "EMAIL_PASS",
                param_value: "wirosableng12",
                category: "NOTIFIKASI_EMAIL",
                description: "Unknown",
                idtenant: 1,
                idapp: 2,
                typeparam: 1,
              },
              {
                param_name: "EMAIL_SUBJECT",
                param_value: "Reset your password",
                category: "NOTIFIKASI_EMAIL",
                description: "Unknown",
                idtenant: 1,
                idapp: 2,
                typeparam: 1,
              },
              {
                param_name: "EMAIL_EXP",
                param_value: "1000",
                category: "NOTIFIKASI_EMAIL",
                description: "Unknown",
                idtenant: 1,
                idapp: 2,
                typeparam: 1,
              },
              {
                param_name: "EMAIL_PORT",
                param_value: "587",
                category: "NOTIFIKASI_EMAIL",
                description: "Unknown",
                idtenant: 1,
                idapp: 2,
                typeparam: 1,
              },
              {
                param_name: "EMAIL_TEMPLATE",
                param_value: "forgot",
                category: "NOTIFIKASI_EMAIL",
                description: "Unknown",
                idtenant: 1,
                idapp: 2,
                typeparam: 1,
              },
              {
                param_name: "EMAIL_FROM",
                param_value: "KOMI",
                category: "NOTIFIKASI_EMAIL",
                description: "Unknown",
                idtenant: 1,
                idapp: 2,
                typeparam: 1,
              },
              {
                param_name: "EMAIL_USER",
                param_value: "bersatukronggahan@gmail.com",
                category: "NOTIFIKASI_EMAIL",
                description: "Unknown",
                idtenant: 1,
                idapp: 2,
                typeparam: 1,
              },
              {
                param_name: "EMAIL_HTTP",
                param_value: "http://182.169.41.153:3013",
                category: "NOTIFIKASI_EMAIL",
                description: "Unknown",
                idtenant: 1,
                idapp: 2,
                typeparam: 1,
              },
              {
                param_name: "EMAIL_SENDERNAME",
                param_value: "Info Mailer",
                category: "NOTIFIKASI_EMAIL",
                description: "Unknown",
                idtenant: 1,
                idapp: 2,
                typeparam: 1,
              },
              {
                param_name: "EMAIL_SECURE",
                param_value: "false",
                category: "NOTIFIKASI_EMAIL",
                description: "Unknown",
                idtenant: 1,
                idapp: 2,
                typeparam: 1,
              },
            ]);
          if (insertSysParam.length > 0) {
            return await knex("mst_bussines_param").insert([
              {
                category: "GENDER_ID",
                label: "Laki-laki",
                valuestr: "01",
                valueint: 1,
              },
              {
                category: "GENDER_ID",
                label: "Perempuan",
                valuestr: "02",
                valueint: 2,
              },
              {
                category: "LOG_EVENT",
                label: "Laki-laki",
                valuestr: "01",
                valueint: 1,
              },
              {
                category: "LOG_EVENT",
                label: "Login",
                valuestr: "01",
                valueint: 1,
              },
              {
                category: "LOG_EVENT",
                label: "Logout",
                valuestr: "02",
                valueint: 2,
              },
              {
                category: "LOG_EVENT",
                label: "Query",
                valuestr: "03",
                valueint: 3,
              },
              {
                category: "LOG_EVENT",
                label: "View",
                valuestr: "04",
                valueint: 4,
              },
              {
                category: "LOG_EVENT",
                label: "Create",
                valuestr: "05",
                valueint: 5,
              },
              {
                category: "LOG_EVENT",
                label: "Update",
                valuestr: "06",
                valueint: 6,
              },
              {
                category: "LOG_EVENT",
                label: "Delete",
                valuestr: "07",
                valueint: 7,
              },
              {
                category: "NOTIFY_EMAIL_TYPE",
                label: "INFO",
                valuestr: "info",
                valueint: 1,
              },
              {
                category: "NOTIFY_EMAIL_TYPE",
                label: "SUCCESS",
                valuestr: "02",
                valueint: 2,
              },
              {
                category: "NOTIFY_EMAIL_TYPE",
                label: "WARNING",
                valuestr: "03",
                valueint: 3,
              },
              {
                category: "NOTIFY_EMAIL_TYPE",
                label: "ERROR",
                valuestr: "04",
                valueint: 4,
              },
              {
                category: "NOTIFY_EMAIL_TYPE",
                label: "DEBUG",
                valuestr: "05",
                valueint: 5,
              },
              {
                category: "MRT_STAT_ID",
                label: "Belum kawin",
                valuestr: "01",
                valueint: 1,
              },

              {
                category: "MRT_STAT_ID",
                label: "Kawin",
                valuestr: "02",
                valueint: 2,
              },
              {
                category: "MRT_STAT_ID",
                label: "Tidak kawin",
                valuestr: "03",
                valueint: 3,
              },

              {
                category: "MRT_STAT_ID",
                label: "Cerai",
                valuestr: "04",
                valueint: 4,
              },

              {
                category: "KOMI_PRX_STS",
                label: "Registration",
                valuestr: "01",
                valueint: 1,
              },
              {
                category: "KOMI_PRX_STS",
                label: "Deregistration",
                valuestr: "02",
                valueint: 2,
              },
              {
                category: "KOMI_PRX_STS",
                label: "Update",
                valuestr: "03",
                valueint: 3,
              },
              {
                category: "KOMI_PRX_STS",
                label: "Suspend",
                valuestr: "04",
                valueint: 4,
              },
              {
                category: "KOMI_PRX_STS",
                label: "Activition",
                valuestr: "05",
                valueint: 5,
              },
              {
                category: "KOMI_PRX_STS",
                label: "Porting",
                valuestr: "06",
                valueint: 6,
              },
              {
                category: "KOMI_PRX_STS",
                label: "Resolution",
                valuestr: "07",
                valueint: 7,
              },
              {
                category: "ID_TYPE",
                label: "E-KTP",
                valuestr: "01",
                valueint: 1,
              },
              {
                category: "ID_TYPE",
                label: "KTP Biasa",
                valuestr: "02",
                valueint: 2,
              },
              {
                category: "ID_TYPE",
                label: "PASPORT",
                valuestr: "03",
                valueint: 3,
              },
              {
                category: "KOMI_PRX_TYP",
                label: "Proxy 1",
                valuestr: "01",
                valueint: 1,
              },
              {
                category: "KOMI_PRX_TYP",
                label: "Proxy 2",
                valuestr: "02",
                valueint: 2,
              },
              {
                category: "KOMI_LMT_STS",
                label: "Active",
                valuestr: "01",
                valueint: 1,
              },
              {
                category: "KOMI_LMT_STS",
                label: "InActive",
                valuestr: "02",
                valueint: 2,
              },
            ]);
          }
        }
      }
    }

    // return true;

    // return await knex('mst_biodata_corell').insert([
    //     {
    //         bioname:'PT Bank BRI agro',
    //         bioidcorel:2,
    //         biocorelobjid:idobj
    //     }
    // ]);
  }
};
