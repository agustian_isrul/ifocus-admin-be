const encPass = require("../../../routes/services/utils/password/encryptionPass");
exports.seed = async function(knex) {
    // Deletes ALL existing entries
    const userResult = await knex('cdid_tenant_user').returning(['id']).insert([
        {
            fullname:'System Administrator',
            userid:'sysadmin',
            pwd:await encPass.encryptPass('manage'),
            creator_stat:1,
            creator_byid: 0,
            idtenant:1,
            leveltenant:0,
            active:1
        }
    ]);
    if(userResult.length > 0) {
        let idobj = userResult[0].id;
        return await knex('mst_biodata_corell').returning(['id']).insert([
            {
                bioname:'System Administrator',
                bioidcorel:3,
                biocorelobjid:idobj
            }
        ]);
    }
}