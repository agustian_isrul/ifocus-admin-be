exports.seed = async function(knex) {
    return await knex('mst_menu')
        .del()
        .then(function() {
            return knex('mst_menu')
            .insert([
                {idmodule:1,name:"bifusers",title:"Users",routelink:"/mgm/user/userslist",defaultid:1, created_byid:0, description:"menu bifast"},
                {idmodule:1,name:"bifgroup",title:"Group & ACL",routelink:"/mgm/acl/grouplist",defaultid:1, created_byid:0, description:"menu bifast"},
                {idmodule:1,name:"bifsysparam",title:"System Parameter",routelink:"/mgm/system/sysparam",defaultid:1, created_byid:0, description:"menu bifast"},
                {idmodule:3,name:"bifbicadmin",title:"Bank Participant",routelink:"/mgm/settings/bicadminlist",defaultid:1, created_byid:0, description:"menu bifast"},
                {idmodule:3,name:"bifcostmgm",title:"Transaction Cost",routelink:"/mgm/settings/trxcostlist",defaultid:1, created_byid:0, description:"menu bifast"},
                {idmodule:3,name:"bifproxymtn",title:"Proxy Management",routelink:"/mgm/settings/proxyaliaslist",defaultid:1, created_byid:0, description:"menu bifast"},
                {idmodule:3,name:"bifparamset",title:"System Parameter",routelink:"/mgm/settings/sysparamslist",defaultid:1, created_byid:0, description:"menu bifast"},
                {idmodule:3,name:"bifbranch",title:"Branch",routelink:"/mgm/settings/branchlist",defaultid:1, created_byid:0, description:"menu bifast"},
                {idmodule:3,name:"bifprefund",title:"Prefund Management",routelink:"/mgm/settings/prefunddashboard",defaultid:1, created_byid:0, description:"menu bifast"},
                {idmodule:3,name:"bifnetman",title:"Network Management",routelink:"/mgm/settings/netmgm",defaultid:1, created_byid:0, description:"menu bifast"},
                {idmodule:3,name:"bifsmtpsett",title:"SMTP Config",routelink:"/mgm/settings/smtpconfig",defaultid:1, created_byid:0, description:"menu bifast"},
                {idmodule:4,name:"bifchtype",title:"Channel Type",routelink:"/mgm/master/channeltypelist",defaultid:1, created_byid:0, description:"menu bifast"},
                {idmodule:4,name:"bifprxtype",title:"Proxy Type",routelink:"/mgm/master/proxytypelist",defaultid:1, created_byid:0, description:"menu bifast"},
                {idmodule:4,name:"bifcusttype",title:"Customer Type",routelink:"/mgm/master/customertype",defaultid:1, created_byid:0, description:"menu bifast"},
                {idmodule:4,name:"bifmapacctype",title:"Account Type",routelink:"/mgm/master/accounttype",defaultid:1, created_byid:0, description:"menu bifast"},
                {idmodule:4,name:"bifmapidtype",title:"Identity Type",routelink:"/mgm/master/idtype",defaultid:1, created_byid:0, description:"menu bifast"},
                {idmodule:4,name:"bifmapresdtype",title:"Resident Type",routelink:"/mgm/master/resident",defaultid:1, created_byid:0, description:"menu bifast"},
                {idmodule:4,name:"bifmaplimit",title:"Limit",routelink:"/mgm/master/limitlist",defaultid:1, created_byid:0, description:"menu bifast"},
                {idmodule:4,name:"bifmapnotify",title:"Admin Notification",routelink:"/mgm/master/adminnotificationlist",defaultid:1, created_byid:0, description:"menu bifast"},
                {idmodule:6,name:"bifsyslog",title:"System Log",routelink:"/mgm/monitor/systemevent",defaultid:1, created_byid:0, description:"menu bifast"},
                {idmodule:6,name:"biftransmonitor",title:"Transaction",routelink:"/mgm/monitor/transmonitoringlist",defaultid:1, created_byid:0, description:"menu bifast"},
                {idmodule:6,name:"biflogevent",title:"Log Event",routelink:"/mgm/monitor/eventlog",defaultid:1, created_byid:0, description:"menu bifast"},
                {idmodule:7,name:"bifreport1",title:"Transaction Report",routelink:"/mgm/report/transaction",defaultid:1, created_byid:0, description:"menu bifast"}
            ])
        })
}