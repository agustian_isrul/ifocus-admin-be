const express = require("express"),
  app = express(),
  port = process.env.port || 3000;
const cors = require("cors");
var authRoute = require("./routes/services/serv_authentication");
// var adminNotifRoute = require("./routes/services/serv_adminnotif");
const integrasiRoute = require("./routes/services/serv_integrasi");
const passwordRoute = require("./routes/services/serv_password");
const bussinesparamRoute = require("./routes/services/serv_bussinesparam");
const sysparamRoute = require("./routes/services/serv_sysparam");
const smtpsettingRoute = require("./routes/services/serv_smtp_setting");
const adminnotifRoute = require("./routes/services/serv_adminnotif");
var profileRoute = require("./routes/services/serv_profile");
const loggingError = require("./middleware/loggingError");
var groupRoute = require("./routes/services/serv_group");
var organizationRoute = require("./routes/services/serv_organizations");
var userManagerRoute = require("./routes/services/serv_usermanagement");
var modulesRoute = require("./routes/services/serv_modules");
var applicationRoute = require("./routes/services/serv_application");
var bussinesParamRoute = require("./routes/services/serv_bussinesparam");
var applicationRoute = require("./routes/services/serv_application");
var modulesRoute = require("./routes/services/serv_modules");
var logRoute = require("./routes/services/serv_log");
var integratApiRoute = require("./routes/services/serv_outapi");
var tenantsRoute = require("./routes/services/serv_tenant");
var path = require("path");

// const session = require("express-session");
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
//-==================== PATH SETTING =================
//app.use(logger('dev'));
app.use(express.static(path.join(__dirname, "public")));
app.use("/api/log", logRoute);
app.use("/adm/auth", authRoute);
app.use("/adm/bussinesparam", bussinesparamRoute);
app.use("/adm/organization", organizationRoute);
app.use("/adm/umanager", userManagerRoute);
app.use("/adm/tenants", tenantsRoute);
app.use("/adm/sysparam", sysparamRoute);
app.use("/adm/smtpset", smtpsettingRoute);
app.use("/adm/adminnotif", adminnotifRoute);
app.use("/adm/group", groupRoute);
app.use("/adm/accmod", modulesRoute);
app.use("/adm/application", applicationRoute);
app.use("/integrate/bp", bussinesParamRoute);
app.use("/adm/send", integrasiRoute);
app.use("/adm/send", passwordRoute);
app.use("/adm/profile", profileRoute);
app.use("/adm/send", integrasiRoute);
app.use("/integrate/api", integratApiRoute);
app.use("/adm/apps", applicationRoute);
app.use("/adm/accmod", modulesRoute);

app.use(loggingError);
//TODO ######## Ini server listen
var server = app.listen(port, function () {
  let host = server.address().address;
  let portname = server.address().port;
  console.log("Example server is running in http://%s:%s", host, portname);
});
