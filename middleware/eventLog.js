const pool = require("../connection/db");

module.exports = async (req, res, next) => {
  if (req.url != "who") {
    await insertIntoLogEvent(req);
  }
  if (!req.url.includes("signout")) {
    try {
      const oldWrite = res.write;
      const oldEnd = res.end;

      const chunks = [];

      res.write = (...restArgs) => {
        chunks.push(Buffer.from(restArgs[0]));
        oldWrite.apply(res, restArgs);
      };

      res.end = async (...restArgs) => {
        if (restArgs[0]) {
          chunks.push(Buffer.from(restArgs[0]));
        }
        const body = Buffer.concat(chunks).toString("utf8");
        if (req.url.includes("sign")) {
          req.body.secret = "**************";
        }

        if (req.url != "who") {
          await pool("event_log")
            .returning(["*"])
            .insert({
              request_ip:
                req.headers["x-forwarded-for"] || req.connection.remoteAddress,
              request_url: req.originalUrl,
              request_method: req.method,
              request_service: req.url,
              request_body: JSON.stringify(req.body),
              request_header: JSON.stringify(req.headers),
              request_date: req.requestDate,
              response_data: JSON.stringify(body) || {},
              response_date: new Date(),
              request_token: req.tokenDate,
              app_id: req.userData ? req.userData.apps[0].idapp : null,
            });
        }
        //console.log(res, restArgs);

        oldEnd.apply(res, restArgs);
      };
    } catch (err) {
      console.log(err);
      next();
    }
  }
  next();
};

Object.defineProperty(String.prototype, "capitalize", {
  value: function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
  },
  enumerable: false,
});

const getMenuFromPath = (routes) => {
  let menu = {
    umanager: "User Management",
    group: "Group & ACL",
    smtpset: "SMTP Setting",
    sysparam: "Security Parameter",
  };
  let route = routes + "";
  route = route.split("/");
  console.log(route);
  let result = menu[route[2]] ? menu[route[2]] : route[2];

  result =
    result == "Log"
      ? route[3] == "getInBoundlog"
        ? "Inbound Log"
        : route[3] == "getOutBoundlog"
        ? "Outbond Log"
        : "System Log"
      : result;

  return result;
};

const getEventValue = (fullPath, menu, req) => {
  let eventVal =
    fullPath.includes("getAll") ||
    fullPath.includes("get") ||
    fullPath.includes("retrive") ||
    fullPath.includes("smtpsettings")
      ? `${menu} View  data`
      : fullPath.includes("get") && req.params
      ? `${menu} View Data With ${req.params}`
      : fullPath.includes("insert") || fullPath.includes("add")
      ? `${menu} Insert New Data `
      : fullPath.includes("update") ||
        fullPath.includes("edit") ||
        fullPath.includes("change")
      ? `${menu} Update Data `
      : fullPath.includes("delete")
      ? `${menu} Delete Data`
      : "";
  return eventVal;
};

const getDescription = (fullPath, routes, menu) => {
  let description =
    fullPath.includes("getAll") ||
    fullPath.includes("get") ||
    fullPath.includes("retrive") ||
    fullPath.includes("smtpsettings")
      ? `View  ${menu} data `
      : fullPath.includes("get") && req.params
      ? `${menu} View Data With ${req.params}`
      : fullPath.includes("insert") || fullPath.includes("add")
      ? `Insert ${menu} New Data `
      : fullPath.includes("update") ||
        fullPath.includes("update") ||
        fullPath.includes("edit") ||
        fullPath.includes("change")
      ? `Update ${menu} Data `
      : fullPath.includes("delete")
      ? `Delete ${menu} Data`
      : "";
  return description;
};

const getAction = (fullPath) => {
  let cdaction =
    fullPath.includes("getAll") || fullPath.includes("retrive")
      ? 4
      : fullPath.includes("get")
      ? 4
      : fullPath.includes("insert")
      ? 5
      : fullPath.includes("update")
      ? 6
      : fullPath.includes("delete")
      ? 7
      : 4;
  return cdaction;
};

const insertIntoLogEvent = async (req) => {
  try {
    //fileLog.info(`New request with token ${token} `);
    console.log("loggin");
    let fullPath = req.baseUrl + req.path;
    let routes = req.baseUrl.split("/");
    let menu = await getMenuFromPath(fullPath);
    console.log(menu);
    let eventVal = await getEventValue(fullPath, menu, req);
    console.log(eventVal);
    let idapp = req.userData.apps[0].idapp;
    let description = await getDescription(fullPath, routes, menu);
    console.log(description);
    let cdaction = await getAction(fullPath);
    let logData = {
      created_by: req.userData.id ? req.userData.id : 0,
      value: eventVal ? eventVal : "",
      idapp: idapp ? idapp : 0,
      idmenu: 0,
      description: description ? description : "",
      cdaction: cdaction ? cdaction : 0,
      refevent: req.tokenID ? req.tokenID : 0,
      valuejson: req.body ? JSON.stringify(req.body) : null,
    };
    await pool("trx_eventlog").insert(logData);
  } catch (err) {
    console.log(err);
  }
};

// const insertIntoLogEvent = async (req) => {
//   try {
//     let fullPath = req.baseUrl + req.path;
//     let routes = req.baseUrl.split("/");

//     let eventVal =
//       fullPath.includes("getAll") || fullPath.includes("retrive")
//         ? `${routes[2]} View all data `
//         : fullPath.includes("get") || fullPath.includes("byid")
//         ? `${routes[2]} View data with filter`
//         : fullPath.includes("insert") || fullPath.includes("add")
//         ? `${routes[2]} Insert new data `
//         : fullPath.includes("update") ||
//           fullPath.includes("edit") ||
//           fullPath.includes("change")
//         ? `${routes[2]} Update data `
//         : fullPath.includes("delete")
//         ? `${routes[2]} Delete data`
//         : "";
//     console.log(req.userData);
//     let idapp = req.userData.apps[0].idapp;

//     let description =
//       fullPath.includes("getAll") || fullPath.includes("retrive")
//         ? `${routes[2]} View all ${routes[2]} data `
//         : fullPath.includes("get") || fullPath.includes("byid")
//         ? `${routes[2]} View ${routes[2]} data with filter`
//         : fullPath.includes("insert") || fullPath.includes("add")
//         ? `${routes[2]} Insert ${routes[2]} new data `
//         : fullPath.includes("update") ||
//           fullPath.includes("edit") ||
//           fullPath.includes("change")
//         ? `${routes[2]} Update ${routes[2]} data `
//         : fullPath.includes("delete")
//         ? `${routes[2]} Delete ${routes[2]} data`
//         : "";
//     let cdaction =
//       fullPath.includes("getAll") || fullPath.includes("retrive")
//         ? 4
//         : fullPath.includes("get") || fullPath.includes("byid")
//         ? 4
//         : fullPath.includes("insert") || fullPath.includes("add")
//         ? 5
//         : fullPath.includes("update") ||
//           fullPath.includes("edit") ||
//           fullPath.includes("change")
//         ? 6
//         : fullPath.includes("delete")
//         ? 7
//         : 4;
//     console.log("EVENTTTTTTTTTTTTTT");
//     console.log(eventVal, idapp, 0);

//     await pool("trx_eventlog").insert({
//       created_by: req.userData.id ? req.userData.id : 0,
//       value: eventVal ? eventVal : 0,
//       idapp: idapp ? idapp : 0,
//       idmenu: 0,
//       description: description ? description : 0,
//       cdaction: cdaction ? cdaction : 0,
//       refevent: req.tokenID ? req.tokenID : 0,
//       valuejson: req.body ? JSON.stringify(req.body) : null,
//     });
//     // fileLog.info(`Saving event to eventlog `);
//   } catch (err) {
//     console.log(err);
//     //next();
//   }
// };
