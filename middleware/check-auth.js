const HttpError = require("../routes/models/http-error");
const jwttools = require("../routes/services/utils/encryptdecryptjwt");
const pool = require("../connection/db");
// const sequelize = require("../connection/db");
const logs = require("../routes/services/utils/logger");
const sysParam = require("../routes/services/utils/user_management/getSysParam");
const checkSession = require("../routes/services/utils/checkSession");
let sessionParam;

const getParam = async () => {
  sessionParam = await sysParam.getUMParam();
};

const fileLog = logs.logging();
module.exports = async (req, res, next) => {
  try {
    const token = req.headers.authorization;
    // Authorization: 'Bearer TOKEN'
    // console.log(token);
    await getParam();
    if (!token) {
      console.log(">>>>>>>>>>>>>>>>>>> " + req.url);
      // fileLog.info(`New request with url ${req.url} `);
      let urlString = req.url;
      switch (urlString) {
        case "/signviakkt":
          // req.userData
          req.requestDate = new Date();
          next();
          break;
        case "/signviaadmin":
          // req.userData
          req.requestDate = new Date();
          next();
          break;
        case "/initialtoken":
          // req.userData
          req.requestDate = new Date();
          next();
          break;
        default:
          req.isexpired = true;
          // res.status(500).json({ status: 500, data: "Authentication failed! "+req.url });
          next();
          break;
      }
    } else {
      // fileLog.info(`New request with token ${token} `);
      TokenArray = token.split(" ");
      // console.log(TokenArray[1]);
      let dcodeInfo = jwttools.decryptdata(TokenArray[1]);
      console.log(">>>>>>>>> DECODED >>>>>>>>> " + JSON.stringify(dcodeInfo));

      const checklLogEvent = await pool("trx_sessionlog")
        .where({
          id: dcodeInfo,
          status: 1,
        })
        .orderBy("created_at", "desc")
        .select("*");
      if (checklLogEvent.length > 0) {
        let checkIdle = await checkSession.checkIdle(
          checklLogEvent[0],
          sessionParam
        );
        if (checkIdle == false) {
          req.isexpired = true;
          next();
        }
        req.tokenID = dcodeInfo;
        req.userData = JSON.parse(checklLogEvent[0].userinfo);
        req.isexpired = false;
        req.requestDate = new Date();
        next();
      } else {
        req.isexpired = true;
        next();
      }

      // let dcodeInfo = jwttools.decryptdata(TokenArray[1]);
    }
  } catch (err) {
    console.log(err);
    fileLog.error(`Authentication to ${req.url} failed `);
    fileLog.error(`Error detail ${err} `);
    const error = new HttpError("Authentication failed!", 403);
    return next(error);
  }
};
